//
//  OptionsCollectionViewCell.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/5/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit

class OptionsCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var viewContainer: BaseUIView!
    
    
    @IBOutlet weak var lblOptions: BaseUILabel!
    
    @IBOutlet weak var imgOptions: UIImageView!
    
    var animated = false
    
    func configCell(data : (imgName : String, title : String)) {
        
        imgOptions.tintColor = UIColor.darkGray
        lblOptions.text = data.title
        imgOptions.image = UIImage.init(named: data.imgName)
        viewContainer.fillThemeColor = "white"
        imgOptions.tintColor = UIColor.darkGray
        lblOptions.fontColorTheme = "darkGray"
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
