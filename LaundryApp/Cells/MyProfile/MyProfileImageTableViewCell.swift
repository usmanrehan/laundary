//
//  MyProfileImageTableViewCell.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/2/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit
import Kingfisher

class MyProfileImageTableViewCell: UITableViewCell {

    @IBOutlet weak var imgProfile: BaseUIImageView!
    @IBOutlet weak var btnUploadImg: UIButton!
    var isAnimated = false
    var uploadImageAction : (()->Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configCell(imgUrl : String, type : TblType , imgUpload : UIImage? = nil) {
        if type == .profile {
            
            if !imgUrl.isEmpty {
              //  DispatchQueue.main.async {
                    let imgString = DefaultConfig.shared.baseUrlImage + imgUrl + "/120/120"
                    let url = URL(string: imgString)
                   // self.imgProfile.kf.indicatorType = .activity
                  //  self.imgProfile.kf.setImage(with: url)
                
             self.imgProfile.kf.indicatorType = .activity
            imgProfile.kf.setImage(with: url, placeholder: UIImage.init(named: "dummyAvatar"), options: nil, progressBlock: { (x, y) in
               // print(x,y)

            }, completionHandler: { (img, error,type, url) in
                if img != nil {
                self.imgProfile.image = img
                }
            })
               // }
            }else{
                 imgProfile.image = UIImage.init(named: "dummyAvatar")
            }
            btnUploadImg.isHidden = true
        }else{
            
            btnUploadImg.isHidden = false
            
            if (imgUpload != nil) {
                imgProfile.image = imgUpload
            }
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    @IBAction func uploadImage(_ sender: Any) {
        uploadImageAction!()
    }
    
}
