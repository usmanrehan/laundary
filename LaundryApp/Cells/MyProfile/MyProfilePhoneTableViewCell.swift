//
//  MyProfilePhoneTableViewCell.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/5/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit

class MyProfilePhoneTableViewCell: UITableViewCell {

    @IBOutlet weak var txtCode: BaseUITextField!
    @IBOutlet weak var txtNumber: BaseUITextField!
    var isAnimated = false
    var codeAction : (() -> Void )?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configCell(data: (title : String, desc: String, imgName : String) , type : TblType, code : String) {
        
        
        let phone = CurrentUser.data?.phone?.components(separatedBy: "-")
        
        txtNumber.keyboardType = .phonePad
        txtCode.placeholder = "code"
        txtCode.text = code.isEmpty ? phone?[0] ?? "" : code
        
        txtCode.leftImage = UIImage.init(named: data.imgName)
        txtNumber.placeholder = data.title
        txtNumber.text = phone!.count > 1 ? phone![1] : ""//data.desc
        txtNumber.leftImage = nil
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    @IBAction func didTapOnCode(_ sender: Any) {
        codeAction!()
    }
    
}
