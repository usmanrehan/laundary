//
//  MyProfileTableViewCell.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/2/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit

class MyProfileTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTitle: BaseUILabel!
    @IBOutlet weak var lblDetail: BaseUILabel!
    var isAnimated = false
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        lblTitle.tag = -1
        lblDetail.tag = -1
    }
    
    func configCell(data: (title : String, desc: String, imgName : String), row : Int){
        
        lblTitle.text = data.title
        lblDetail.text = data.desc
        if (row == 4) {
            let arrPhoneNumber = data.desc.components(separatedBy: "-")
            var phoneNumer = arrPhoneNumber[1]
            phoneNumer.insert(separator: "-", every: 3, totalInsertCount: 2)
            print(phoneNumer)   // "11:23:12:45:1\n"
            lblDetail.text = arrPhoneNumber[0] + "-" + phoneNumer
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

