//
//  EditProfileTableViewCell.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/2/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit

class EditProfileTableViewCell: UITableViewCell {

    @IBOutlet weak var txtData: BaseUITextField!
    var dataHolder : AddAddressModel!
    var isAnimated = false
    
    @IBOutlet weak var txtDataProportionalWidthConstraint: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        txtData.delegate = self
        txtData.tag = -1
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configCell(data: (title : String, desc: String, imgName : String) , type : TblType){
        
        txtData.placeholder = data.title
         txtData.text = data.desc
        txtData.leftImage = UIImage.init(named: data.imgName)
        
        if type == .edit && (data.imgName == "email" || data.imgName == "address") {
            
            txtData.isUserInteractionEnabled = false
          //--ww  txtData.alpha = 0.7
        }else {
            txtData.isUserInteractionEnabled = true
          //--ww  txtData.alpha = 1.0
            
        }
        
}
    
    func configCellDataAddress(data: AddAddressModel, indexNo : Int) {
        
        txtData.removeTarget(self, action: #selector(txtChanged), for: .editingChanged)
         txtData.addTarget(self, action: #selector(txtChanged), for: .editingChanged)
        dataHolder = data
         txtDataProportionalWidthConstraint.constant = DesignUtility.getValueFromRatio(70)
         txtData.placeholder = "\(data.placeholders.0)"
         txtData.text = data.values.0 // " \(data.values.0)"
         txtData.leftImage = nil
        txtData.isUserInteractionEnabled = true
        
        if indexNo == 0 {
            txtData.rightImage = UIImage.init(named: "address")
            txtData.rightImageRightPadding = 10
            txtData.isUserInteractionEnabled = false
            
        }
      
    }
    
    
    @objc func txtChanged()  {
        if let typedText = txtData.text {
            print(typedText)
            dataHolder.values.0 = typedText
        }
    }
    
}
extension EditProfileTableViewCell :UITextFieldDelegate {
    
    
    
}
