//
//  NotificationTableViewCell.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/8/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {

    
    @IBOutlet weak var lblNotification: BaseUILabel!
    @IBOutlet weak var lblTime: BaseUILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configCell(noti : String , time : String , didRead : Int)  {
        lblNotification.text = noti
        lblTime.text = time.toDateString("dd MMM, yyyy")
        if didRead == 0 {
           self.contentView.backgroundColor = UIColor.init(hexString: "#F2DCDB")
        }else{
               self.contentView.backgroundColor = .white
        }
    }
    
}
