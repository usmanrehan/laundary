//
//  AddAddressTableViewCell.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/8/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit

class AddAddressTableViewCell: UITableViewCell {
    

    @IBOutlet weak var txtFirst: BaseUITextField!
    @IBOutlet weak var txtSecond: BaseUITextField!
    var dataHolder : AddAddressModel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        txtFirst.delegate = self
        txtSecond.delegate = self
        txtFirst.tag = -1
        txtSecond.tag = -1
        
        txtFirst.addTarget(self, action: #selector(txtFirstChanged), for: .editingChanged)
        txtSecond.addTarget(self, action: #selector(txtSecondChanged), for: .editingChanged)
        
    }

    func configCell(data : AddAddressModel) {
        
        dataHolder = data
        txtFirst.placeholder = "\(data.placeholders.0)"
        txtSecond.placeholder = "\(data.placeholders.1)"
        
        txtFirst.text = data.values.0 //" \(data.values.0)"
        txtSecond.text = data.values.1 // " \(data.values.1)"
        
        if data.placeholders.0 == GlobalStatic.getLocalizedString("city") {
            txtFirst.isUserInteractionEnabled = false
            txtSecond.isUserInteractionEnabled = false
        }else {
            txtFirst.isUserInteractionEnabled = true
            txtSecond.isUserInteractionEnabled = true
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @objc func txtFirstChanged()  {
        if let typedText = txtFirst.text {
          print(typedText)
            dataHolder.values.0 = typedText
        }
    }
    
    @objc func txtSecondChanged()  {
        if let typedText = txtSecond.text {
            print(typedText)
            dataHolder.values.1 = typedText
        }
    }
    
}
extension AddAddressTableViewCell : UITextFieldDelegate {
}

