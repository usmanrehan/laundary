//
//  ScheduleTableViewCell.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/8/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit

class ScheduleTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTitle: BaseUILabel!
    @IBOutlet weak var imgOptions: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configCell(data : (title: String, imgName: String )){
      
        lblTitle.text = data.title
        imgOptions.image = UIImage.init(named: data.imgName)?.flipIfNeeded()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
