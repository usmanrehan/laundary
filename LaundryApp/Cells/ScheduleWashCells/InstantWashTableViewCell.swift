//
//  InstantWashTableViewCell.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/8/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit
class InstantWashTableViewCell: UITableViewCell {
    
    @IBOutlet weak var switchControll: UISwitch!
    var switchAction : ((_ state : Bool) -> Void)?
    @IBOutlet weak var lblInstantWash: BaseUILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
  
       //--ww switchControll.isOn =  CartData.SD.isInstance
        if switchControll.isOn{
            switchControll.thumbTintColor = Constants.selectedColor
            
        }else{
            switchControll.thumbTintColor = .lightGray
        }
        lblInstantWash.text = GlobalStatic.getLocalizedString("instant_order")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configCell(isOn : Bool) {
        
         switchControll.isOn =  isOn
        if switchControll.isOn{
            switchControll.thumbTintColor = Constants.selectedColor
            
        }else{
            switchControll.thumbTintColor = .lightGray
        }
    }
    
    @IBAction func didChangeSwitch(_ sender: Any) {
        
        if switchControll.isOn{
            switchControll.thumbTintColor = Constants.selectedColor
            
        }else{
            switchControll.thumbTintColor = .lightGray
        }
        switchAction!(switchControll.isOn)
    }
    
    
    @IBAction func didTapOnInfo(_ sender: Any) {
        
        let alert = CustomAlert(title: GlobalStatic.getLocalizedString("instant_order_info"), image: UIImage(named: "textlogo")!)
        alert.show(animated: true)
    }
    
}
