//
//  ContactTableViewCell.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/2/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit
import MOLH


class ContactTableViewCell: UITableViewCell {

    
    @IBOutlet weak var txtContact: BaseUITextField!
    
    var lblPhone = MarqueeLabel()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        txtContact.tag = -1
        // Initialization code
        if MOLHLanguage.currentAppleLanguage() == "en" {
            txtContact.textAlignment = .left
        }else{
          txtContact.textAlignment = .right
        }
     //--ww  _ = Timer.scheduledTimer(timeInterval: 0.3, target: self, selector: #selector(self.marquee), userInfo: nil, repeats: true)
        
        
        let xLbl = (self.txtContact.leftView?.frame.width)!
        let frame = CGRect(x: xLbl , y:0 , width: self.txtContact.frame.width - (xLbl / 2) , height: self.txtContact.frame.height )
        lblPhone.frame = frame
         self.txtContact.addSubview(lblPhone)
    }
    
    func configCell(data : (imgName : String , title : String)) {
        
       
        txtContact.leftImage = UIImage.init(named: data.imgName)
        
        if data.imgName == "contact" {
           lblPhone.text = "\(data.title)  "
           lblPhone.textColor = UIColor.darkGray
            lblPhone.font = txtContact.font
           txtContact.text  = ""
           lblPhone.isHidden = false
          lblPhone.restartLabel()
            
        }else{
           txtContact.text = data.title
            lblPhone.isHidden = true
          
        }
        
       
    }

    
    func setUpMarqueLabel(){
        lblPhone.frame = self.txtContact.frame
        lblPhone.text = "some very big text here jfhfdakjf fdkfkfkalf kfkdfkf akfkafakfh afkfakfj akfjkafjkdfj akjfka jf"
        lblPhone.isHidden = false
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension ContactTableViewCell {
    @objc func marquee(){
        
//        let str = txtContact.text!
//        let indexFirst = str.index(str.startIndex, offsetBy: 0)
//        let indexSecond = str.index(str.startIndex, offsetBy: 1)
//        txtContact.text = String(str.suffix(from: indexSecond)) + String(str[indexFirst])
        
        if txtContact.tag != 404{
            return
        }
    
            let str = self.txtContact.text!
            let indexFirst = str.index(str.startIndex, offsetBy: 0)
            let indexSecond = str.index(str.startIndex, offsetBy: 1)
            self.txtContact.text = String(str.suffix(from: indexSecond)) + String(str[indexFirst])
    
        
    }
}
