//
//  ContactHeaderTableViewCell.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/2/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit

class ContactHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var lblText: BaseUILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblText.text = GlobalStatic.getLocalizedString("contact_tagline")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
