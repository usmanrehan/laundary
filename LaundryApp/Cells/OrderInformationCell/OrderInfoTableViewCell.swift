//
//  OrderInfoTableViewCell.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/7/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit

class OrderInfoTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTitle: BaseUILabel!
    @IBOutlet weak var imgArrow: UIImageView!
    
    var isAnimated = false
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
         imgArrow.image = UIImage.init(named: "right_arrow")?.flipIfNeeded()
    }

    
    func configCell(data : String)  {
    
        lblTitle.text = data
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
