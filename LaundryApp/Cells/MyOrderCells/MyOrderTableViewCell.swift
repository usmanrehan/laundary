//
//  MyOrderTableViewCell.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/9/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit

class MyOrderTableViewCell: UITableViewCell {


    
    let pickupColor = UIColor(red: 198/255, green: 0/255, blue: 37/255, alpha: 1)
    let deliveryColor = UIColor(red: 0/255, green: 175/255, blue: 48/255, alpha: 1)
    
    
    @IBOutlet weak var lblOrderId: BaseUILabel!
    @IBOutlet weak var lblPickupDateTime: BaseUILabel!
    @IBOutlet weak var lblDeliveryDateTime: BaseUILabel!
    @IBOutlet weak var lblPrice: BaseUILabel!
    
    @IBOutlet weak var lblProcess: BaseUILabel!
    @IBOutlet weak var lblTextPickupDateTime: BaseUILabel!
    @IBOutlet weak var lblTextDeliveryDateTime: BaseUILabel!
    @IBOutlet weak var lblTextPrice: BaseUILabel!
   
    
    var dataArray = [
        (GlobalStatic.getLocalizedString("order_placed"), UIColor.lightGray),
        (GlobalStatic.getLocalizedString("order_pending"),UIColor(red: 204/255, green: 204/255, blue: 0/255, alpha: 1)),
        (GlobalStatic.getLocalizedString("pickup_process"),Constants.selectedColor),
        (GlobalStatic.getLocalizedString("delivery_process"),UIColor(red: 0/255, green: 175/255, blue: 48/255, alpha: 1)),
        (GlobalStatic.getLocalizedString("order_complete"), UIColor(red: 0/255, green: 175/255, blue: 48/255, alpha: 1)),
        (GlobalStatic.getLocalizedString("order_failed"),Constants.selectedColor),
        (GlobalStatic.getLocalizedString("order_denied"),Constants.selectedColor),
        (GlobalStatic.getLocalizedString("laundry_process"), UIColor(red: 0/255, green: 175/255, blue: 48/255, alpha: 1)),
        (GlobalStatic.getLocalizedString("order_canceled"),Constants.selectedColor)
    ]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        lblTextPickupDateTime.text = GlobalStatic.getLocalizedString("pickup_date_time")
        lblTextDeliveryDateTime.text = GlobalStatic.getLocalizedString("delivery_date_time")
        lblTextPrice.text = GlobalStatic.getLocalizedString("price")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configCell(data : OLData){
       
       // 12 Feb 16 9:30 AM - 10:30 AM
        
         //Delivery Process
        let pickupParts = data.slot!.components(separatedBy: "-")
        if pickupParts.count > 1 {
            let pickupJoined  = " \((pickupParts[0] ).to12HourTime()) - \((pickupParts[1] ).to12HourTime())"
            
             lblPickupDateTime.text = data.pickupDate!.toDateString() + " " + pickupJoined
        }else{
            lblPickupDateTime.text = data.pickupDate!.toDateString() + " " + data.slot!
        }
       
        
        
        let deliveryParts = data.deliverySlot!.components(separatedBy: "-")
        if deliveryParts.count > 1 {
             let deliveryJoined  = " \((deliveryParts[0] ).to12HourTime()) - \((deliveryParts[1] ).to12HourTime())"
             lblDeliveryDateTime.text = data.deliveryDate!.toDateString() + " " + deliveryJoined
        }else{
            lblDeliveryDateTime.text = data.deliveryDate!.toDateString() + " " + data.deliverySlot!
        }
       
        
        
        
        lblOrderId.text = "Order ID - #\(data.id)"
       
       
        
        lblPrice.text = "AED \(data.totalAmount)"
        
       // if data.orderStatus < 5{
            lblProcess.text = dataArray[data.orderStatus].0
            lblProcess.backgroundColor = dataArray[data.orderStatus].1
      //  }
//        } else if data.orderStatus == 7 {
//            lblProcess.text = "Cancelled"
//            lblProcess.backgroundColor = UIColor.darkGray
//
//        }else{
//
//            lblProcess.text = GlobalStatic.getLocalizedString("delivery_process")
//            lblProcess.backgroundColor = deliveryColor
//
//        }
        
        
       print( "data.orderStatus" , data.orderStatus)
        
    }
    
}
