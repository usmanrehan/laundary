//
//  OrderSummaryTableViewCell.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/9/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit

class OrderSummaryTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var lblTitle: BaseUILabel!
    
    @IBOutlet weak var lblDetail: BaseUILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    func configCell(data : (String, String)) {
        lblTitle.text = data.0
        lblDetail.text = data.1
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
