//
//  OrderTopTableViewCell.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/9/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit

class OrderTopTableViewCell: UITableViewCell {

    @IBOutlet weak var viewTopHeightConstraint: NSLayoutConstraint!
    
   
    @IBOutlet weak var lblOrderId: BaseUILabel!
    @IBOutlet weak var lblPtickupTime: BaseUILabel!
    @IBOutlet weak var lblDeliveryTime: BaseUILabel!
    @IBOutlet weak var lblPickup: BaseUILabel!
    @IBOutlet weak var lblDelivery: BaseUILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblPickup.text = GlobalStatic.getLocalizedString("pickup")
        lblDelivery.text = GlobalStatic.getLocalizedString("delivery")
        
    }

    func configCell (data : SOOrder) {
        lblOrderId.text = "ORDER ID - #\(data.id ?? 0)"
        let pickupParts = data.slot!.components(separatedBy: "-")
        let pickupJoined  = " \((pickupParts[0] ).to12HourTime()) - \((pickupParts[1] ).to12HourTime())"
        
        
        let deliveryParts = data.deliverySlot!.components(separatedBy: "-")
        let deliveryJoined  = " \((deliveryParts[0] ).to12HourTime()) - \((deliveryParts[1] ).to12HourTime())"
        
        lblPtickupTime.text = data.pickupDate!.toFormatedDateString() + " " + pickupJoined
        lblDeliveryTime.text = data.deliveryDate!.toFormatedDateString() + " " + deliveryJoined
      
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
