//
//  OrderFooterTableViewCell.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/9/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit

class OrderFooterTableViewCell: UITableViewCell {

    @IBOutlet weak var lblSubTotal: BaseUILabel!
    @IBOutlet weak var lblPromoCode: BaseUILabel!
    @IBOutlet weak var lblTotal: BaseUILabel!
    
    
    @IBOutlet weak var lblSubTotalText: BaseUILabel!
    @IBOutlet weak var lblPromoCodeText: BaseUILabel!
    @IBOutlet weak var lblPaymentText: BaseUILabel!
    
    
    @IBOutlet weak var lblTextInstantOrder: BaseUILabel!
    @IBOutlet weak var lblAmountInstantOrder: BaseUILabel!
    @IBOutlet weak var lblPromoTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblInstantTopConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblSubTotalText.text = GlobalStatic.getLocalizedString("sub_total")
        lblPaymentText.text = GlobalStatic.getLocalizedString("payment")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func configCell (data : SOOrder) {
    
        
        if data.redeemdAmount! > 0 {
              lblPromoCodeText.text = GlobalStatic.getLocalizedString("promo_code_text")
             lblPromoTopConstraint.constant = DesignUtility.getValueFromRatio(15)
             lblPromoCode.text =  "- \(data.redeemdAmount ?? 0) AED" //- 20 %"
        }else{
              lblPromoCodeText.text = ""
              lblPromoTopConstraint.constant = 0
             lblPromoCode.text = ""
        }
        
        if data.deliveryAmount! > 0 {
             lblTextInstantOrder.text = GlobalStatic.getLocalizedString("instant_surcharge")
            lblInstantTopConstraint.constant = DesignUtility.getValueFromRatio(15)
            lblAmountInstantOrder.text = "+ \(data.deliveryAmount ?? 0) AED"
        }else{
             lblTextInstantOrder.text = ""
            lblInstantTopConstraint.constant = 0
            lblAmountInstantOrder.text = ""
        }
        
        lblSubTotal.text = "\(data.amount!) AED"//"100 AED"
        lblTotal.text = "\(data.totalAmount!) AED" //"80 AED"
        
        self.layoutIfNeeded()
        self.setNeedsDisplay()
    }
    
}
