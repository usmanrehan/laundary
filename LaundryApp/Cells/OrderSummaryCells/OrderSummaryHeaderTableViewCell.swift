//
//  OrderSummaryHeaderTableViewCell.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/8/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit

class OrderSummaryHeaderTableViewCell: UITableViewCell {
    
    var historyAction : (() -> Void)?
    var continueLaundryAction : (() -> Void)?

    @IBOutlet weak var lblOrderConfirmed: BaseUILabel!
    @IBOutlet weak var lblOrderPlaced: BaseUILabel!
    @IBOutlet weak var btnOrderHistory: CustomButton!
    @IBOutlet weak var btnContinueLaundry: BaseUIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblOrderConfirmed.text = GlobalStatic.getLocalizedString("order_confirmed_text")
        lblOrderPlaced.text = GlobalStatic.getLocalizedString("order_placed_text")
        btnOrderHistory.setTitle(GlobalStatic.getLocalizedString("order_history"), for: .normal)
        btnContinueLaundry.setTitle(GlobalStatic.getLocalizedString("continue_laundry"), for: .normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func orderHistoryTapped(_ sender: Any) {
        historyAction!()
    }
    
    
    @IBAction func continueLaundryTapped(_ sender: Any) {
        continueLaundryAction!()
    }
    
    
    
}
