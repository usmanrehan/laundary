//
//  WalkthroughCollectionViewCell.swift
//  LaundryApp
//
//  Created by CMS Testing on 2/27/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit

class WalkthroughCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imgBg: UIImageView!
    
    @IBOutlet weak var lblTitle: BaseUILabel!
    
    
    @IBOutlet weak var lblDescription: BaseUILabel!
    
    
    @IBOutlet weak var btnNext: CustomButton!
    
    //MARK:- Variables
    var btnNextAction: (() -> Void)?
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
      //  btnNext.addTarget(self, action: #selector(self.didTapOnNextButton), for: .touchUpInside)
        
    }
    
    //Mark: Function to call on tap of footer view
    @objc func didTapOnNextButton(){
        btnNextAction!()
    }
    
    func configCell(data : WalkthroughModel){
        
        imgBg.image = UIImage.init(named: data.imgName)
        lblTitle.text = data.title
        lblDescription.text = data.description
        
    }
}
