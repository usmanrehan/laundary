//
//  ServicesTableViewCell.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/7/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit
import MOLH
import Kingfisher

protocol ServicesTableViewCellProtocol : class {
     func didTapOnSlider()->Void
}

class ServicesTableViewCell: UITableViewCell {

    @IBOutlet weak var imgCategory: BaseUIImageView!
    @IBOutlet weak var lblTitle: BaseUILabel!
    @IBOutlet weak var lblPrice: BaseUILabel!
    @IBOutlet weak var viewStepper: BaseUIView!
    @IBOutlet weak var btnMinus: BaseUIButton!
    @IBOutlet weak var txtStepper: BaseUITextField!
    @IBOutlet weak var btnPlus: BaseUIButton!
    weak var serviceDelegate : ServicesTableViewCellProtocol?
    var stepValue = 0
    var dataItem : SLItem!
    var sectionId = 0
    var isAnimated = false
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        txtStepper.isUserInteractionEnabled = false
        
        if MOLHLanguage.currentAppleLanguage() == "en" {
         btnMinus.borderSides = .right
            btnPlus.borderSides = .left
            
        }else{
            btnMinus.borderSides = .left
            btnPlus.borderSides = .right
        }
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configCell(data : SLItem , section : Int){
        
        txtStepper.textAlignment = .center
        dataItem = data
        sectionId = section
        
        if !(data.image?.isEmpty)! {
            let imgString = DefaultConfig.shared.baseUrl + "resize-service/" + data.image! + "/65/65"
            
           // imgCategory.imageFromServerURL(urlString: imgString)
        //    let url = URL(string: imgString)
          imgCategory.kf.indicatorType = .activity
       //   imgCategory.kf.setImage(with: url)
            
//            imgCategory.kf.setImage(with: url, placeholder: nil, options: nil, progressBlock: { (x, y) in
//                print(x, y)
//            }, completionHandler: { (img, error, type, url) in
//
//                 print(img, error, type, url)
//            })
            
            
            
            let modifier = AnyModifier { request in
                var r = request
                // replace "Access-Token" with the field name you need, it's just an example
                r.setValue("en", forHTTPHeaderField: "Accept-Language")
                return r
            }
            
            let url = URL(string: imgString)
            
          
            
            imgCategory.kf.setImage(with: url, options: [.requestModifier(modifier)]) { (image, error, type, url) in
                if error == nil && image != nil {
                    // here the downloaded image is cached, now you need to set it to the imageView
                    DispatchQueue.main.async {
                        self.imgCategory.image = image
                    }
                } else {
                    // handle the failure
                    print(error)
                }
            }
        }
        lblTitle.text = data.title!
        lblPrice.text =  "AED \(data.amount!)"
        
        if let item =  CartData.cartDict[data.id] {
         txtStepper.text = "\(item.quantity)"
         stepValue = item.quantity
        }else{
           txtStepper.text = "0"
             stepValue = 0
        }
        
    }
    
    @IBAction func didTapOnMinus(_ sender: Any) {
        
        if stepValue != 0 {
            stepValue -= 1
            txtStepper.text = String(stepValue)
            
            if var item =  CartData.cartDict[dataItem.id] {
                item.quantity = stepValue
                item.amount = Float(stepValue) * Float(dataItem.amount!)!
                CartData.cartDict[dataItem.id] = item
                //--ww CartData.totalAmount += item.amount
                if stepValue == 0 {
                    CartData.cartDict.removeValue(forKey: dataItem.id)
                }
              
            }

             CartData.totalAmount -= Float(dataItem.amount!) ?? 0.0
            CartData.totalQuantity -= 1
             serviceDelegate?.didTapOnSlider()
        }
    }
    
    
    @IBAction func didTapOnPlus(_ sender: Any) {
        stepValue += 1
        txtStepper.text = String(stepValue)
        if var item =  CartData.cartDict[dataItem.id] {
            item.quantity = stepValue
            item.amount = Float(stepValue) * Float(dataItem.amount!)!
            CartData.cartDict[dataItem.id] = item
          //  CartData.totalAmount += item.amount
        }else{
            
            CartData.cartDict[dataItem.id] = (quantity : stepValue, amount : Float(stepValue) * Float(dataItem.amount!)! , item : dataItem.title! , section : sectionId)
          
        }
        CartData.totalAmount += Float(dataItem.amount!) ?? 0.0
        CartData.totalQuantity += 1
          serviceDelegate?.didTapOnSlider()
    }
    
}

extension UIImageView {
    public func imageFromServerURL(urlString: String) {
        
        URLSession.shared.dataTask(with: NSURL(string: "http://laundry.stagingic.com/api/resize-service/service/gents/undershirt.png/65/65")! as URL, completionHandler: { (data, response, error) -> Void in
            
            
             print(response)
            if error != nil {
                
                print(error)
                return
            }
            DispatchQueue.main.async(execute: { () -> Void in
                 print(data)
                let image = UIImage(data: data!)
                self.image = image
                print(image)
            })
            
        }).resume()
    }}
