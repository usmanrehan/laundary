//
//  ServiceHeaderTableViewCell.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/7/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit

class ServiceHeaderTableViewCell: UITableViewCell {

    
    @IBOutlet weak var optionsView: OptionsView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configOptionsView() {
        
        optionsView.dataArray = [
            (imgName: "dryClean" , title : "Dry Clean"),
            (imgName: "iron" , title : "Steam Ironing"),
            (imgName: "wash-iron" , title : "Wash & Iron"),
            
        ]
        
        optionsView.delegateOptions = self
        optionsView.colViewOptions.reloadData()
        
        optionsView.colViewOptions.selectItem(at:IndexPath.init(item: 0, section: 0), animated: false, scrollPosition: .centeredHorizontally)
    }
    
}
extension ServiceHeaderTableViewCell : OptionsViewProtocol {
    
    func didSelectItemAtIndex(index: Int) {
        print(optionsView.dataArray[index])
    }
}
