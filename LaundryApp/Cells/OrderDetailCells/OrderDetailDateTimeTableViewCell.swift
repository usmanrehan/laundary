//
//  OrderDetailDateTimeTableViewCell.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/9/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit

class OrderDetailDateTimeTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblDateTime: BaseUILabel!
    @IBOutlet weak var lblAddress: BaseUILabel!
    @IBOutlet weak var lblTextDateTime: BaseUILabel!
    @IBOutlet weak var lblTextAddress: BaseUILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblTextDateTime.text = GlobalStatic.getLocalizedString("date_time")
        lblTextAddress.text = GlobalStatic.getLocalizedString("address")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configCell(data : ODOrder , sec : Int){
        
     
        
        
     
        
        var date = ""
        var time = ""
        var address = ""
        if sec == 1 {
            date = data.pickupDate!.toDateString()
            time = data.slot!
            address = data.pickupLocation!
        }else{
            date = data.deliveryDate!.toDateString()
            time = data.deliverySlot!
             address = data.dropLocation!
            }
        
        
        let parts = time.components(separatedBy: "-")
        if parts.count > 1 {
            let joined  = " \((parts[0] ).to12HourTime()) - \((parts[1] ).to12HourTime())"
            lblDateTime.text = date + " & " + joined
        }else{
        
            lblDateTime.text = date + " & " + time
        }
       
        lblAddress.text = address
    }
    
}
