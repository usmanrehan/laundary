//
//  OrderDetailFooterTableViewCell.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/9/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit

class OrderDetailFooterTableViewCell: UITableViewCell {

    
    @IBOutlet weak var lblTotalItems: BaseUILabel!
    @IBOutlet weak var lblSubTotal: BaseUILabel!
    @IBOutlet weak var lblPromoCode: BaseUILabel!
    @IBOutlet weak var lblPayment: BaseUILabel!
    
    
    
    @IBOutlet weak var lblTextTotalItems: BaseUILabel!
    @IBOutlet weak var lblTextSubTotal: BaseUILabel!
    @IBOutlet weak var lblTextPromoCode: BaseUILabel!
    @IBOutlet weak var lblTextPayment: BaseUILabel!
    
    
    @IBOutlet weak var lblTextInstantOrder: BaseUILabel!
    @IBOutlet weak var lblAmountInstantOrder: BaseUILabel!
    @IBOutlet weak var lblPromoTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblInstantOrderTopConstraint: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
         lblTextTotalItems.text = GlobalStatic.getLocalizedString("total_item")
         lblTextSubTotal.text = GlobalStatic.getLocalizedString("sub_total")
       //--ww   lblTextPromoCode.text = GlobalStatic.getLocalizedString("promo_code_text")
         lblTextPayment.text = GlobalStatic.getLocalizedString("payment")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func configCell(data : ODOrder){
        var quantity = 0
        for i in data.orderDetail! {
            quantity +=  i.quantity ?? 0
        }
        
        
        if data.redeemdAmount! > 0 {
            lblTextPromoCode.text = GlobalStatic.getLocalizedString("promo_code_text")
            lblPromoTopConstraint.constant = DesignUtility.getValueFromRatio(15)
            lblPromoCode.text =  "- \(data.redeemdAmount ?? 0) AED" //- 20 %"
        }else{
            lblTextPromoCode.text = ""
            lblPromoTopConstraint.constant = 0
            lblPromoCode.text = ""
        }
        
        if data.deliveryAmount! > 0 {
            lblTextInstantOrder.text = GlobalStatic.getLocalizedString("instant_surcharge")
            lblInstantOrderTopConstraint.constant = DesignUtility.getValueFromRatio(15)
            lblAmountInstantOrder.text = "+ \(data.deliveryAmount ?? 0) AED"
        }else{
            lblTextInstantOrder.text = ""
            lblInstantOrderTopConstraint.constant = 0
            lblAmountInstantOrder.text = ""
        }
        
        lblTotalItems.text = "\(quantity)"
        lblSubTotal.text = "\(String(describing: data.amount!)) AED"
         lblPayment.text =  "\(String(describing: data.totalAmount!)) AED"
        
        self.layoutIfNeeded()
        self.setNeedsDisplay()
    }
}
