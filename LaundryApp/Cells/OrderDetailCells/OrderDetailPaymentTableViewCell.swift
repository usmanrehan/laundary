//
//  OrderDetailPaymentTableViewCell.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/9/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit

class OrderDetailPaymentTableViewCell: UITableViewCell {

    
    @IBOutlet weak var lblPaymentType: BaseUILabel!
    @IBOutlet weak var txtCardNo: BaseUITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        txtCardNo.isHidden = true
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
