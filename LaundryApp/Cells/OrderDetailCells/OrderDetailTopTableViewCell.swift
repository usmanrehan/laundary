//
//  OrderDetailTopTableViewCell.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/9/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit

class OrderDetailTopTableViewCell: UITableViewCell {
    
    var trackingAction : (()->Void)?
    @IBOutlet weak var lblName: BaseUILabel!
    @IBOutlet weak var lblNumber: BaseUILabel!
    
    @IBOutlet weak var btnOrderTracking: CustomButton!
    @IBOutlet weak var lblFullName: BaseUILabel!
    @IBOutlet weak var lblMobileNo: BaseUILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblFullName.text = GlobalStatic.getLocalizedString("full_name")
        lblMobileNo.text = GlobalStatic.getLocalizedString("mobile_no")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configCell(data : ODOrderuser , orderStatus : Int){
        lblName.text = data.name!
        
        print(data.phone!)
        lblNumber.text = data.phone!
        
        let arrPhoneNumber = data.phone!.components(separatedBy: "-")
        var phoneNumer = arrPhoneNumber[1]
        phoneNumer.insert(separator: "-", every: 3, totalInsertCount: 2)
        print(phoneNumer)   // "11:23:12:45:1\n"
        lblNumber.text = arrPhoneNumber[0] + "-" + phoneNumer
        
        if orderStatus > 4 && orderStatus != 7 {
            
            if btnOrderTracking != nil {
            btnOrderTracking.isUserInteractionEnabled = false
            btnOrderTracking.alpha = 0.0
            btnOrderTracking.removeFromSuperview()
            }
           //--ww  btnOrderTracking .setTitle("Cancelled", for: .normal)
            
        }else{
           //
            btnOrderTracking.isUserInteractionEnabled = true
            btnOrderTracking.alpha = 1
            btnOrderTracking .setTitle(GlobalStatic.getLocalizedString("order_status_tracking"), for: .normal)
        }
    }
    
    @IBAction func didTapOnTracker(_ sender: Any) {
        trackingAction!()
    }
    
}
