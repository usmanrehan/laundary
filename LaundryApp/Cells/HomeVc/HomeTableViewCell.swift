//
//  HomeTableViewCell.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/1/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit

class HomeTableViewCell: UITableViewCell {

    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblTitle: BaseUILabel!
    
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var viewContainerLeadingConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var viewContainerTraillingConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var imgRightArrow: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.selectionStyle = .none
    }

    func configCell(data : (image : String , title : String) , index : Int , isAnimated : Bool) {
        
      
        imgIcon.image = UIImage.init(named: data.image)?.flipIfNeeded()
        imgRightArrow.image = UIImage.init(named: "right_arrow")?.flipIfNeeded()
        lblTitle.text = data.title
        
        
        
//        if !isAnimated {
//        viewContainerLeadingConstraint.constant = -DeviceUtility.getSize().width
//        viewContainerTraillingConstraint.constant = DeviceUtility.getSize().width
//        let delay  = Float(index) * 0.25 //280
//
//
//            perform(#selector(animate), with: nil, afterDelay: TimeInterval(delay))
//
////        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(delay)) {
////
////           self.animate()
////        }
//        }else{
//            self.viewContainerLeadingConstraint.constant = 0
//            self.viewContainerTraillingConstraint.constant = 0
//             self.layoutIfNeeded()
//        }
    }
    
    @objc func animate() {
        
        
        UIView.animate(withDuration: 0.5) {
            self.viewContainerLeadingConstraint.constant = 0
            self.viewContainerTraillingConstraint.constant = 0
            self.layoutIfNeeded()
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
