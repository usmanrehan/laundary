//
//  CartViewContainerTableViewCell.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/7/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//
//

import UIKit

enum CartType {
    case fromService , fromDetail
}

class CartViewContainerTableViewCell: UITableViewCell {

    @IBOutlet weak var viewCOntainer: UIView!
    @IBOutlet weak var imgCategory: UIImageView!
    @IBOutlet weak var lblTitle: BaseUILabel!
    @IBOutlet weak var viewLineBottom: UIView!
    @IBOutlet weak var tblListing: UITableView!
    
    @IBOutlet weak var lblQuantity: BaseUILabel!
    @IBOutlet weak var lblPrice: BaseUILabel!
    
    
    @IBOutlet weak var tblViewHeightConstraint: NSLayoutConstraint!
    var dataAray = [(item :String , cost : String)]()
    var dataArr = [(quantity : Int, amount: Float , item : SLItem , section :Int)]()
    var orderDetailArr  = [ODOrderDetail]()
    let cartCellIdentifier =  "CartViewTableViewCell"
    var animated = false
    
    
    var imgArray = [(GlobalStatic.getLocalizedString("dry_clean") ,"wash-fold-cart" ),
                    (GlobalStatic.getLocalizedString("steam_ironning"), "iron-cart") ,
                    (GlobalStatic.getLocalizedString("wash_iron"), "wash-iron-cart")]
    
    var comeHere : CartType = .fromService
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        //--ww setupTable()
        lblQuantity.numberOfLines = 0
        lblPrice.numberOfLines = 0
    }
    
    func configCell(data : MyCartModel) {
        
        imgCategory.image = UIImage.init(named: data.imgName)
        lblTitle.text = data.title
         dataAray = data.dArray
        // tblListing.reloadData()
    }
    
    
    func configCellFromServerData(data : [(quantity : Int, amount: Float , item : String , section :Int)] , indexNo : Int) {
        
        imgCategory.image = UIImage.init(named: imgArray[indexNo].1)
        lblTitle.text = imgArray[indexNo].0
      //--ww  dataArr = data
        
        var quantity  = ""
        var price = ""
        for detail in data {
            quantity = quantity + "\(detail.quantity) x \(detail.item)\n"
            price = price + "AED \(detail.amount)\n"
        }
        lblQuantity.text = quantity
        lblPrice.text = price
      //  tblListing.reloadData()
    }
    
    
    
    func configCellFromOrderDetail(data : [ODOrderDetail], indexNo : Int , from : CartType) {
        
          //--wwtblViewHeightConstraint.isActive = false
        imgCategory.image = UIImage.init(named: imgArray[indexNo].1)
        lblTitle.text = imgArray[indexNo].0
        orderDetailArr = data
        comeHere = from
        
        var quantity  = ""
        var price = ""
        for detail in data {
            quantity = quantity + "\(detail.quantity!) x \(detail.itemDetail?.title ?? "")" + "\n"
            price = price + "AED \(detail.itemDetail?.amount ?? "")\n"
        }
        lblQuantity.text = quantity
        lblPrice.text = price
         //--ww print("orderDetailArr" , orderDetailArr)
       //   tblListing.reloadData()
      //--ww  tblViewHeightConstraint.constant = tblListing.contentSize.height
        
      
    }
    
  

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        
    }
    
  /*  func setupTable() {
        
       // tblListing.backgroundColor = .red
        tblListing.delegate = self
        tblListing.dataSource = self
        tblListing.estimatedRowHeight = DesignUtility.getValueFromRatio(26)
        tblListing.rowHeight = DesignUtility.getValueFromRatio(25)
        tblListing.register(UINib.init(nibName: cartCellIdentifier, bundle: nil), forCellReuseIdentifier: cartCellIdentifier)
        
       
    }
    
 override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
        
        tblListing.reloadData()
         // if the table view is the last UI element, you might need to adjust the height
     let ht =  tblListing.frame.origin.y + tblListing.contentSize.height + DesignUtility.getValueFromRatio(20)
        let size = CGSize(width: targetSize.width,
                          height:ht )
        
       print("tblListing.frame" , tblListing.frame )
     //print("targetSize.height" , targetSize.height )
        return size
        
    } */
}
/*
extension CartViewContainerTableViewCell : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
  
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
           print("tblListing.frame numberOfRowsInSection" , tblListing.frame )
        if comeHere == .fromService{
        return  dataArr.count
        }else{
            
            return orderDetailArr.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
        let cell  = tableView.dequeueReusableCell(withIdentifier: cartCellIdentifier) as! CartViewTableViewCell
        cell.selectionStyle = .none
         if comeHere == .fromService{
       cell.configCellData(data: dataArr[indexPath.row])
         }else{
            cell.configCellDataOrderDetail(data: orderDetailArr[indexPath.row])
             cell.setNeedsLayout()
            cell.layoutIfNeeded()
        }
        return cell
        
    }
    
    
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return DesignUtility.getValueFromRatio(25)
//    }
    
    
}

extension CartViewContainerTableViewCell : UITableViewDelegate {
    
    
}
*/
