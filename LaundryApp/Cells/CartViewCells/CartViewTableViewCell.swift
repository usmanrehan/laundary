//
//  CartViewTableViewCell.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/7/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit

class CartViewTableViewCell: UITableViewCell {

    @IBOutlet weak var lblCost: BaseUILabel!
    @IBOutlet weak var lblTItle: BaseUILabel!
    
 
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configData(data : (item :String , cost : String) )  {
        lblTItle.text = data.item
        lblCost.text = data.cost
        
    }
    func configCellData(data:(quantity : Int, amount: Int , item : SLItem , section :Int))  {
        
        lblTItle.text = data.item.title
        lblCost.text = "AED \(data.amount)"
    }
    
    func configCellDataOrderDetail(data : ODOrderDetail)  {
        
       // let amount = Int(data.amount!)
       // let quantity = Int(data.quantity!)
        
        lblTItle.text = data.itemDetail?.title!
        lblCost.text = "AED \(data.amount!)" //--ww amount! * quantity!
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
