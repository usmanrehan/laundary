//
//  MyAddressTableViewCell.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/8/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit

class MyAddressTableViewCell: UITableViewCell {

    
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var imgRadio: UIImageView!
    @IBOutlet weak var lblTitle: BaseUILabel!
    @IBOutlet weak var lblAddress: BaseUILabel!
    
    @IBOutlet weak var imgEdit: UIImageView!
    
    var animated = false
    var actionEdit : ((_ addModel : GAData?) -> Void)?
    var dataHolder : GAData!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imgEdit.image = #imageLiteral(resourceName: "edit_pencil").flipIfNeeded()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
       

    }
    
    func configCell (data : GAData , type : AddressType) {
        
        
        if type == .pickup {
            lblTitle.text = GlobalStatic.getLocalizedString("from_pickup")
        }else{
           lblTitle.text =  GlobalStatic.getLocalizedString("to_delivery")
        }
        dataHolder = data
        lblAddress.text = GlobalStatic.getFormatedAddress(add: data.location ?? "")
        if CurrentUser.selectedAddressID == data.id {
        imgRadio.image = UIImage.init(named: "radio_btn_sel")
         
        }else{
          imgRadio.image = UIImage.init(named: "radio_btn_blank")
           
        }
    }
    
    @IBAction func didTapOnEdit(_ sender: Any) {
        actionEdit!(dataHolder)
    }
}
