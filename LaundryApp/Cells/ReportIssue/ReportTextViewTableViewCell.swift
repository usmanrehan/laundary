//
//  ReportTextViewTableViewCell.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/3/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit


class ReportTextViewTableViewCell: UITableViewCell {

    @IBOutlet weak var txtViewProportionalWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var txtIssues: BaseUITextView!
    
    @IBOutlet weak var lblCounter: BaseUILabel!
    
    @IBOutlet weak var txtViewTopConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        txtIssues.delegate = self
        lblCounter.text = "0 / \(Constants.maximumLengthTextView)"
        txtIssues.placeholderTxt = GlobalStatic.getLocalizedString("enter_issue")
    }

    func configCellProvideInstruction() {
        txtIssues.placeholderTxt = GlobalStatic.getLocalizedString("other_instruction")
        txtViewProportionalWidthConstraint.constant = DesignUtility.getValueFromRatio(70)
        txtViewTopConstraint.constant = DesignUtility.getValueFromRatio(30)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
extension ReportTextViewTableViewCell : UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        // We need to figure out how many characters would be in the string after the change happens
        let startingLength = txtIssues.text?.count ?? 0
        let lengthToAdd = text.count
        let lengthToReplace = range.length
        let newLength = startingLength + lengthToAdd - lengthToReplace
     //  return newLength <= Constants.maximumLengthTextView
        if newLength <= Constants.maximumLengthTextView{
            lblCounter.text = "\(newLength) / \(Constants.maximumLengthTextView)"
            return true
        }
        return  false //newLength <= characterCountLimit
    }
}
