//
//  ReportEmailTableViewCell.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/3/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit
import MOLH

class ReportEmailTableViewCell: UITableViewCell {

    @IBOutlet weak var txtEmail: BaseUITextField!
    @IBOutlet weak var lblDetail: BaseUILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
      if MOLHLanguage.currentAppleLanguage() == "en" {
        lblDetail.textAlignment = .left
      }else{
         lblDetail.textAlignment = .right
     }
        lblDetail.text = GlobalStatic.getLocalizedString("provide_valid_email")
        lblDetail.tag = 0
        txtEmail.text = CurrentUser.data?.email ?? "example@mail.com"
        txtEmail.isUserInteractionEnabled = false
        if MOLHLanguage.currentAppleLanguage() == "en" {
            txtEmail.textAlignment = .left
        }else{
            txtEmail.textAlignment = .right
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
