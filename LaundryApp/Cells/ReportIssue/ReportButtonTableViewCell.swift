//
//  ReportButtonTableViewCell.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/3/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit

class ReportButtonTableViewCell: UITableViewCell {

    @IBOutlet weak var btnProportionalWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnReport: CustomButton!
    var reportBtnAction : (() -> Void)?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        btnReport.setTitle(GlobalStatic.getLocalizedString("report"), for: .normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configCell(data : String) {
        btnReport .setTitle(data, for: .normal)
        btnProportionalWidthConstraint.constant = DesignUtility.getValueFromRatio(75)
    }
    
    @IBAction func btnReportDidTap(_ sender: Any) {
        
        reportBtnAction!()
    }
}
