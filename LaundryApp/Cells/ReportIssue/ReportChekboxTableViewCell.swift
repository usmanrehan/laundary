//
//  ReportChekboxTableViewCell.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/3/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit
import MOLH


class ReportChekboxTableViewCell: UITableViewCell {

    @IBOutlet weak var btnCheckBox: CheckBoxButton!
    
    var isChecked = false
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        btnCheckBox.isUserInteractionEnabled = false
         if MOLHLanguage.currentAppleLanguage() == "en" {
        btnCheckBox.tag = 0
         }else{
               btnCheckBox.tag = -1
        }
    }

    func configCell(data : String)  {
      //  btnCheckBox.fillThemeColor = "red"
        btnCheckBox.setTitle(data, for: .normal)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func btnCheckboxTapped(_ sender: Any) {
        
        if isChecked == false {
            
            isChecked = true
            
            btnCheckBox .setImage(UIImage.init(named: "checkbox-sel"), for: .normal)
        }else{
            isChecked = false
            btnCheckBox .setImage(UIImage.init(named: "checkbox"), for: .normal)
        }
        
    }
}
