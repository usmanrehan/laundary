//
//	GAData.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

class GAData : Codable {

	let address : String?
	let createdAt : String?
	let deletedAt : String?
	let id : Int?
	let latitude : Double?
	let location : String?
	let longitude : Double?
	let status : String?
	let type : String?
	let updatedAt : String?
	let userId : Int?


	enum CodingKeys: String, CodingKey {
		case address = "address"
		case createdAt = "created_at"
		case deletedAt = "deleted_at"
		case id = "id"
		case latitude = "latitude"
		case location = "location"
		case longitude = "longitude"
		case status = "status"
		case type = "type"
		case updatedAt = "updated_at"
		case userId = "user_id"
	}
	required init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		address = try values.decodeIfPresent(String.self, forKey: .address) ?? String()
		createdAt = try values.decodeIfPresent(String.self, forKey: .createdAt) ?? String()
		deletedAt = try values.decodeIfPresent(String.self, forKey: .deletedAt) ?? String()
		id = try values.decodeIfPresent(Int.self, forKey: .id) ?? Int()
		latitude = try values.decodeIfPresent(Double.self, forKey: .latitude) ?? Double()
		location = try values.decodeIfPresent(String.self, forKey: .location) ?? String()
		longitude = try values.decodeIfPresent(Double.self, forKey: .longitude) ?? Double()
        
		//--WW status = try values.decodeIfPresent(String.self, forKey: .status) ?? String()
        if let value = try? values.decode(Int.self, forKey: .status) {
            status = String(value)
        } else {
            status = try? values.decode(String.self, forKey: .status)
            
        }
        
    
        
		type = try values.decodeIfPresent(String.self, forKey: .type) ?? String()
        
        
        
        
		updatedAt = try values.decodeIfPresent(String.self, forKey: .updatedAt) ?? String()
		userId = try values.decodeIfPresent(Int.self, forKey: .userId) ?? Int()
	}


}
