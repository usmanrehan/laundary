//
//	GAResult.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

class GAResult : Codable {

	//--ww let address : GAAddres?
    let data : [GAData]?


	enum CodingKeys: String, CodingKey {
	//	case address
        case data = "data"
	}
	required init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		//address = try values.decodeIfPresent(GAAddres.self, forKey: .address)  //?? GAAddres()
        data = try values.decodeIfPresent([GAData].self, forKey: .data) ?? [GAData]()
	}


}
