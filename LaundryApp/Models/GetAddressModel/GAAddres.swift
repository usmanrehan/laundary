//
//	GAAddres.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

class GAAddres : Codable {

	let data : [GAData]?
	let pages : Int?


	enum CodingKeys: String, CodingKey {
		case data = "data"
		case pages = "pages"
	}
	required init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		data = try values.decodeIfPresent([GAData].self, forKey: .data) ?? [GAData]()
		pages = try values.decodeIfPresent(Int.self, forKey: .pages) ?? Int()
	}


}