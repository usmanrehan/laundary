//
//	GNNotification.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

class GNNotification : Codable {

	let actionType : String?
	let createdAt : String?
	let id : Int?
	let message : String?
	let refId : Int?
    var isRead : Int?

	enum CodingKeys: String, CodingKey {
		case actionType = "action_type"
		case createdAt = "created_at"
		case id = "id"
		case message = "message"
		case refId = "ref_id"
        case isRead = "is_read"
	}
	required init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		actionType = try values.decodeIfPresent(String.self, forKey: .actionType) ?? String()
		createdAt = try values.decodeIfPresent(String.self, forKey: .createdAt) ?? String()
		id = try values.decodeIfPresent(Int.self, forKey: .id) ?? Int()
		message = try values.decodeIfPresent(String.self, forKey: .message) ?? String()
		refId = try values.decodeIfPresent(Int.self, forKey: .refId) ?? Int()
        isRead = try values.decodeIfPresent(Int.self, forKey: .isRead) ?? Int()
	}


}
