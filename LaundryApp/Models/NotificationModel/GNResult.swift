//
//	GNResult.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

class GNResult : Codable {

	let notification : [GNNotification]?
    let unreadCount : Int?

	enum CodingKeys: String, CodingKey {
		case notification = "notification"
        case unreadCount = "UnreadCount"
	}
	required init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		notification = try values.decodeIfPresent([GNNotification].self, forKey: .notification) ?? [GNNotification]()
         unreadCount = try values.decodeIfPresent(Int.self, forKey: .unreadCount) ?? Int()
	}
}
