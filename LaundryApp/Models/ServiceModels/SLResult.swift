//
//	SLResult.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import Realm
import RealmSwift

class SLResult :Object, Decodable {

	var services = List<SLService>()


	enum CodingKeys: String, CodingKey {
		case services = "services"
	}
	convenience required init(from decoder: Decoder) throws {
        self.init()
		let values = try decoder.container(keyedBy: CodingKeys.self)
		let servicesArray = try values.decodeIfPresent([SLService].self, forKey: .services) ?? [SLService]()
        services.append(objectsIn: servicesArray)
	}

    required init() {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }

}
