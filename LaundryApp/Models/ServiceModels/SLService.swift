//
//	SLService.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import Realm
import RealmSwift

class SLService : Object, Decodable {

	@objc dynamic var createdAt : String?
	@objc dynamic var id : Int = 0
   
    var serviceType =  List<SLServiceType>()
	@objc dynamic var status : String?
	@objc dynamic var title : String?
	@objc dynamic var updatedAt : String?


	enum CodingKeys: String, CodingKey {
		case createdAt = "created_at"
		case id = "id"
		case serviceType = "service_type"
		case status = "status"
		case title = "title"
		case updatedAt = "updated_at"
	}
	convenience required init(from decoder: Decoder) throws {
        self.init()
		let values = try decoder.container(keyedBy: CodingKeys.self)
		createdAt = try values.decodeIfPresent(String.self, forKey: .createdAt) ?? String()
		id = try values.decodeIfPresent(Int.self, forKey: .id) ?? Int()
		let serviceTypeArray = try values.decodeIfPresent([SLServiceType].self, forKey: .serviceType) ?? [SLServiceType]()
        serviceType.append(objectsIn: serviceTypeArray)
        
		//--WW status = try values.decodeIfPresent(String.self, forKey: .status) ?? String()
        
        if let value = try? values.decode(Int.self, forKey: .status) {
            status = String(value)
        } else {
            status = try? values.decode(String.self, forKey: .status)
            
        }
        
        
		title = try values.decodeIfPresent(String.self, forKey: .title) ?? String()
		updatedAt = try values.decodeIfPresent(String.self, forKey: .updatedAt) ?? String()
	}

    required init() {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    
}
