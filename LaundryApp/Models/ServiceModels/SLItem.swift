//
//	SLItem.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import Realm
import RealmSwift

class SLItem : Object, Codable {

	@objc dynamic var amount : String?
	@objc dynamic var createdAt : String?
	@objc dynamic var id : Int = 0
	@objc dynamic var image : String?
	@objc dynamic var status : String?
	@objc dynamic var title : String?
	@objc dynamic var typeId : String?
	@objc dynamic var updatedAt : String?


	enum CodingKeys: String, CodingKey {
		case amount = "amount"
		case createdAt = "created_at"
		case id = "id"
		case image = "image"
		case status = "status"
		case title = "title"
		case typeId = "type_id"
		case updatedAt = "updated_at"
	}
	convenience required init(from decoder: Decoder) throws {
         self.init()
		let values = try decoder.container(keyedBy: CodingKeys.self)
		//--WW amount = try values.decodeIfPresent(String.self, forKey: .amount) ?? String()
        
        if let value = try? values.decode(Int.self, forKey: .amount) {
            amount = String(value)
        } else {
            amount = try? values.decode(String.self, forKey: .amount)
        }
        
        
		createdAt = try values.decodeIfPresent(String.self, forKey: .createdAt) ?? String()
		id = try values.decodeIfPresent(Int.self, forKey: .id) ?? Int()
		image = try values.decodeIfPresent(String.self, forKey: .image) ?? String()
        
	//--WW	status = try values.decodeIfPresent(String.self, forKey: .status) ?? String()
        
        if let value = try? values.decode(Int.self, forKey: .status) {
            status = String(value)
        } else {
            status = try? values.decode(String.self, forKey: .status)
        }
        
        
		title = try values.decodeIfPresent(String.self, forKey: .title) ?? String()
        
		//--WW typeId = try values.decodeIfPresent(String.self, forKey: .typeId) ?? String()
        
        
        if let value = try? values.decode(Int.self, forKey: .typeId) {
            typeId = String(value)
        } else {
            typeId = try? values.decode(String.self, forKey: .typeId)
            
        }
		updatedAt = try values.decodeIfPresent(String.self, forKey: .updatedAt) ?? String()
	}

    required init() {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }

}
