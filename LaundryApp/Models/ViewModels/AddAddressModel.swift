//
//  AddAddressModel.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/13/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit

class AddAddressModel {
    var placeholders : (String, String)
    var values : (String, String)
    
    init(_placeholders : (String,String) , _values : (String, String)) {
        self.placeholders = _placeholders
        self.values = _values
    }
    
  func set(_placeholders : (String,String) , _values : (String, String)) {
        self.placeholders = _placeholders
        self.values = _values
    }
}
