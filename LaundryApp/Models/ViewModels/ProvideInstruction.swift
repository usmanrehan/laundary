//
//  ProvideInstruction.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/14/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import Foundation

struct ProvideInstructionModel : Codable {
    
    var atMyDoor  =  false
    var callBeforeDelivery = false
    var callBeforePickup  = false
    var isFold  = false
    var saveConfiguration = false
    var otherInstruction = ""
    
}
