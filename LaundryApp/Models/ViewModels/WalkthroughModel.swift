//
//  WalkthroughModel.swift
//  LaundryApp
//
//  Created by Waqas Ali on 2/27/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit

struct WalkthroughModel {
    
    var imgName : String
    var title : String
    var description : String
}
