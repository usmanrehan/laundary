//
//  MyCartModel.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/7/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit

struct MyCartModel {
    var title : String
    var imgName : String
    var dArray : [(item :String , cost : String)]
}
