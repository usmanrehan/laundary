//
//	AAAddres.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

class AAAddres : Codable {

	let createdAt : String?
	let id : Int?
	let latitude : String?
	let location : String?
	let longitude : String?
	let type : String?
	let updatedAt : String?
	let userId : String?


	enum CodingKeys: String, CodingKey {
		case createdAt = "created_at"
		case id = "id"
		case latitude = "latitude"
		case location = "location"
		case longitude = "longitude"
		case type = "type"
		case updatedAt = "updated_at"
		case userId = "user_id"
	}
	required init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		createdAt = try values.decodeIfPresent(String.self, forKey: .createdAt) ?? String()
		id = try values.decodeIfPresent(Int.self, forKey: .id) ?? Int()
		latitude = try values.decodeIfPresent(String.self, forKey: .latitude) ?? String()
		location = try values.decodeIfPresent(String.self, forKey: .location) ?? String()
		longitude = try values.decodeIfPresent(String.self, forKey: .longitude) ?? String()
		type = try values.decodeIfPresent(String.self, forKey: .type) ?? String()
		updatedAt = try values.decodeIfPresent(String.self, forKey: .updatedAt) ?? String()
	//--WW 	userId = try values.decodeIfPresent(String.self, forKey: .userId) ?? String()
        
        if let value = try? values.decode(Int.self, forKey: .userId) {
            userId = String(value)
        } else {
            userId = try? values.decode(String.self, forKey: .userId)
        }
        
	}


}
