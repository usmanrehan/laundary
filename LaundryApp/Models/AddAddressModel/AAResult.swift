//
//	AAResult.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

class AAResult : Codable {

	let address : GAData?


	enum CodingKeys: String, CodingKey {
		case address
	}
	required init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		address = try values.decodeIfPresent(GAData.self, forKey: .address)  //?? AAAddres()
	}


}
