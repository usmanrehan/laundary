//
//	TSResult.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

class TSResult : Codable {

	let availableTimeSlot : [TSAvailableTimeSlot]?


	enum CodingKeys: String, CodingKey {
		case availableTimeSlot = "availableTimeSlot"
	}
	required init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		availableTimeSlot = try values.decodeIfPresent([TSAvailableTimeSlot].self, forKey: .availableTimeSlot) ?? [TSAvailableTimeSlot]()
	}


}