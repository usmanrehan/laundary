//
//	TSAvailableTimeSlot.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

class TSAvailableTimeSlot : Codable {

	let deliveryPerSlot : String?
	let id : Int?
	let instanceDeliveryPerSlot : String?
	let instancePerSlot : String?
	let instanceSurcharge : Float?
	let orderPerSlot : String?
	let slotTime : String?


	enum CodingKeys: String, CodingKey {
		case deliveryPerSlot = "delivery_per_slot"
		case id = "id"
		case instanceDeliveryPerSlot = "instance_delivery_per_slot"
		case instancePerSlot = "instance_per_slot"
		case instanceSurcharge = "instance_surcharge"
		case orderPerSlot = "order_per_slot"
		case slotTime = "slot_time"
	}
	required init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
        
	//--WW 	deliveryPerSlot = try values.decodeIfPresent(String.self, forKey: .deliveryPerSlot) ?? String()
        
        if let value = try? values.decode(Int.self, forKey: .deliveryPerSlot) {
            deliveryPerSlot = String(value)
        } else {
            deliveryPerSlot = try? values.decode(String.self, forKey: .deliveryPerSlot)
            
        }
        
        
		id = try values.decodeIfPresent(Int.self, forKey: .id) ?? Int()
        
        
		//--WWinstanceDeliveryPerSlot = try values.decodeIfPresent(String.self, forKey: .instanceDeliveryPerSlot) ?? String()
        if let value = try? values.decode(Int.self, forKey: .instanceDeliveryPerSlot) {
            instanceDeliveryPerSlot = String(value)
        } else {
            instanceDeliveryPerSlot = try? values.decode(String.self, forKey: .instanceDeliveryPerSlot)
            
        }
        
   
        
		//--WW instancePerSlot = try values.decodeIfPresent(String.self, forKey: .instancePerSlot) ?? String()
        
        if let value = try? values.decode(Int.self, forKey: .instancePerSlot) {
            instancePerSlot = String(value)
        } else {
            instancePerSlot = try? values.decode(String.self, forKey: .instancePerSlot)
            
        }
        
        
		instanceSurcharge = try values.decodeIfPresent(Float.self, forKey: .instanceSurcharge) ?? Float()
        
        
	//--WW 	orderPerSlot = try values.decodeIfPresent(String.self, forKey: .orderPerSlot) ?? String()
        
        if let value = try? values.decode(Int.self, forKey: .orderPerSlot) {
            orderPerSlot = String(value)
        } else {
            orderPerSlot = try? values.decode(String.self, forKey: .orderPerSlot)
            
        }
        
		slotTime = try values.decodeIfPresent(String.self, forKey: .slotTime) ?? String()
	}


}
