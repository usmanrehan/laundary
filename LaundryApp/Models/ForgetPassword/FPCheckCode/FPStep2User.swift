//
//	FPStep2User.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

class FPStep2User : Codable {

	let code : String?
	let email : String?


	enum CodingKeys: String, CodingKey {
		case code = "code"
		case email = "email"
	}
	required init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		code = try values.decodeIfPresent(String.self, forKey: .code) ?? String()
		email = try values.decodeIfPresent(String.self, forKey: .email) ?? String()
	}


}