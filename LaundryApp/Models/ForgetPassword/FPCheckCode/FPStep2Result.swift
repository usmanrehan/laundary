//
//	FPStep2Result.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

class FPStep2Result : Codable {

	let user : FPStep2User?


	enum CodingKeys: String, CodingKey {
		case user
	}
	required init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		user = try values.decodeIfPresent(FPStep2User.self, forKey: .user)  //?? FPStep2User()
	}


}