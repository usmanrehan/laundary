//
//	OrderInstruction.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

class OrderInstruction : Codable {

	let atMyDoor : Int?
	let callMeBeforeDelivery : Int?
	let callMeBeforePickup : Int?
	let createdAt : String?
	let deletedAt : String?
	let id : Int?
	let isFold : Int?
	let orderId : String?
	let other : String?
	let status : String?
	let updatedAt : String?
	let userId : String?


	enum CodingKeys: String, CodingKey {
		case atMyDoor = "at_my_door"
		case callMeBeforeDelivery = "call_me_before_delivery"
		case callMeBeforePickup = "call_me_before_pickup"
		case createdAt = "created_at"
		case deletedAt = "deleted_at"
		case id = "id"
		case isFold = "is_fold"
		case orderId = "order_id"
		case other = "other"
		case status = "status"
		case updatedAt = "updated_at"
		case userId = "user_id"
	}
	required init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		atMyDoor = try values.decodeIfPresent(Int.self, forKey: .atMyDoor) ?? Int()
		callMeBeforeDelivery = try values.decodeIfPresent(Int.self, forKey: .callMeBeforeDelivery) ?? Int()
		callMeBeforePickup = try values.decodeIfPresent(Int.self, forKey: .callMeBeforePickup) ?? Int()
		createdAt = try values.decodeIfPresent(String.self, forKey: .createdAt) ?? String()
		deletedAt = try values.decodeIfPresent(String.self, forKey: .deletedAt) ?? String()
		id = try values.decodeIfPresent(Int.self, forKey: .id) ?? Int()
		isFold = try values.decodeIfPresent(Int.self, forKey: .isFold) ?? Int()
		orderId = try values.decodeIfPresent(String.self, forKey: .orderId) ?? String()
		other = try values.decodeIfPresent(String.self, forKey: .other) ?? String()
		//--WW status = try values.decodeIfPresent(String.self, forKey: .status) ?? String()
        
        if let value = try? values.decode(Int.self, forKey: .status) {
            status = String(value)
        } else {
            status = try? values.decode(String.self, forKey: .status)
        }
        
        
		updatedAt = try values.decodeIfPresent(String.self, forKey: .updatedAt) ?? String()
		//--WW userId = try values.decodeIfPresent(String.self, forKey: .userId) ?? String()
        
        
        if let value = try? values.decode(Int.self, forKey: .userId) {
            userId = String(value)
        } else {
            userId = try? values.decode(String.self, forKey: .userId)
        }
	}


}
