//
//	ODOrderInstruction.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

class ODOrderInstruction : Codable {

	let atMyDoor : String?
	let callMeBeforeDelivery : String?
	let callMeBeforePickup : String?
	let createdAt : String?
	let deletedAt : String?
	let id : Int?
	let isFold : String?
	let orderId : String?
	let other : String?
	let status : String?
	let updatedAt : String?
	let userId : String?


	enum CodingKeys: String, CodingKey {
		case atMyDoor = "at_my_door"
		case callMeBeforeDelivery = "call_me_before_delivery"
		case callMeBeforePickup = "call_me_before_pickup"
		case createdAt = "created_at"
		case deletedAt = "deleted_at"
		case id = "id"
		case isFold = "is_fold"
		case orderId = "order_id"
		case other = "other"
		case status = "status"
		case updatedAt = "updated_at"
		case userId = "user_id"
	}
	required init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		//--WW atMyDoor = try values.decodeIfPresent(String.self, forKey: .atMyDoor) ?? String()
        
        if let value = try? values.decode(Int.self, forKey: .atMyDoor) {
            atMyDoor = String(value)
        } else {
            atMyDoor = try? values.decode(String.self, forKey: .atMyDoor)
        }
        
        
        
	//--WW 	callMeBeforeDelivery = try values.decodeIfPresent(String.self, forKey: .callMeBeforeDelivery) ?? String()
        
        if let value = try? values.decode(Int.self, forKey: .callMeBeforeDelivery) {
            callMeBeforeDelivery = String(value)
        } else {
            callMeBeforeDelivery = try? values.decode(String.self, forKey: .callMeBeforeDelivery)
        }
        
	//--WW 	callMeBeforePickup = try values.decodeIfPresent(String.self, forKey: .callMeBeforePickup) ?? String()
        
        if let value = try? values.decode(Int.self, forKey: .callMeBeforePickup) {
            callMeBeforePickup = String(value)
        } else {
            callMeBeforePickup = try? values.decode(String.self, forKey: .callMeBeforePickup)
        }
        
        
        
      //--WW   isFold = try values.decodeIfPresent(String.self, forKey: .isFold) ?? String()
        
        if let value = try? values.decode(Int.self, forKey: .isFold) {
            isFold = String(value)
        } else {
            isFold = try? values.decode(String.self, forKey: .isFold)
        }
        
        
		createdAt = try values.decodeIfPresent(String.self, forKey: .createdAt) ?? String()
        
		deletedAt = try values.decodeIfPresent(String.self, forKey: .deletedAt) ?? String()
		id = try values.decodeIfPresent(Int.self, forKey: .id) ?? Int()
		
        
        
	//--WW 	orderId = try values.decodeIfPresent(String.self, forKey: .orderId) ?? String()
        
        if let value = try? values.decode(Int.self, forKey: .orderId) {
            orderId = String(value)
        } else {
            orderId = try? values.decode(String.self, forKey: .orderId)
            
        }
        
        
		other = try values.decodeIfPresent(String.self, forKey: .other) ?? String()
		//--WW status = try values.decodeIfPresent(String.self, forKey: .status) ?? String()
        if let value = try? values.decode(Int.self, forKey: .status) {
            status = String(value)
        } else {
            status = try? values.decode(String.self, forKey: .status)
            
        }
        
		updatedAt = try values.decodeIfPresent(String.self, forKey: .updatedAt) ?? String()
	//--WW 	userId = try values.decodeIfPresent(String.self, forKey: .userId) ?? String()
        
        if let value = try? values.decode(Int.self, forKey: .userId) {
            userId = String(value)
        } else {
            userId = try? values.decode(String.self, forKey: .userId)
            
        }
        
	}


}
