//
//	ODItemDetail.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

class ODItemDetail : Codable {

	let amount : String?
	let createdAt : String?
	let id : Int?
	let image : String?
	let parenttype : ODParenttype?
	let status : String?
	let title : String?
	let typeId : String?
	let updatedAt : String?


	enum CodingKeys: String, CodingKey {
		case amount = "amount"
		case createdAt = "created_at"
		case id = "id"
		case image = "image"
		case parenttype = "parenttype"
		case status = "status"
		case title = "title"
		case typeId = "type_id"
		case updatedAt = "updated_at"
	}
	required init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		//--WW amount = try values.decodeIfPresent(String.self, forKey: .amount) ?? String()
        
        if let value = try? values.decode(Int.self, forKey: .amount) {
            amount = String(value)
        } else {
            amount = try? values.decode(String.self, forKey: .amount)
            
        }
        
		createdAt = try values.decodeIfPresent(String.self, forKey: .createdAt) ?? String()
		id = try values.decodeIfPresent(Int.self, forKey: .id) ?? Int()
		image = try values.decodeIfPresent(String.self, forKey: .image) ?? String()
		parenttype = try values.decodeIfPresent(ODParenttype.self, forKey: .parenttype)  //?? ODParenttype()
        
		//--WW status = try values.decodeIfPresent(String.self, forKey: .status) ?? String()
        if let value = try? values.decode(Int.self, forKey: .status) {
            status = String(value)
        } else {
            status = try? values.decode(String.self, forKey: .status)
            
        }
        
        
		title = try values.decodeIfPresent(String.self, forKey: .title) ?? String()
        
		//--WW typeId = try values.decodeIfPresent(String.self, forKey: .typeId) ?? String()
        
        if let value = try? values.decode(Int.self, forKey: .typeId) {
            typeId = String(value)
        } else {
            typeId = try? values.decode(String.self, forKey: .typeId)
            
        }
        
		updatedAt = try values.decodeIfPresent(String.self, forKey: .updatedAt) ?? String()
	}


}
