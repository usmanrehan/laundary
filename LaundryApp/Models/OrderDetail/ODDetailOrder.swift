//
//	ODDetailOrder.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

class ODDetailOrder : Codable {

	let code : Int?
	let message : String?
	let result : ODResult?
	let userBlocked : Int?
	let pages : Int?


	enum CodingKeys: String, CodingKey {
		case code = "Code"
		case message = "Message"
		case result = "Result"
		case userBlocked = "UserBlocked"
		case pages = "pages"
	}
	required init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		code = try values.decodeIfPresent(Int.self, forKey: .code) ?? Int()
		message = try values.decodeIfPresent(String.self, forKey: .message) ?? String()
		result = try values.decodeIfPresent(ODResult.self, forKey: .result)  //?? ODResult()
		userBlocked = try values.decodeIfPresent(Int.self, forKey: .userBlocked) ?? Int()
		pages = try values.decodeIfPresent(Int.self, forKey: .pages) ?? Int()
	}


}
