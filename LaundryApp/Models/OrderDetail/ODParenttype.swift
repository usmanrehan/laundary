//
//	ODParenttype.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

class ODParenttype : Codable {

	let createdAt : String?
	let id : Int?
	let parent : ODParent?
	let serviceId : String?
	let status : String?
	let title : String?
	let updatedAt : String?


	enum CodingKeys: String, CodingKey {
		case createdAt = "created_at"
		case id = "id"
		case parent = "parent"
		case serviceId = "service_id"
		case status = "status"
		case title = "title"
		case updatedAt = "updated_at"
	}
	required init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		createdAt = try values.decodeIfPresent(String.self, forKey: .createdAt) ?? String()
		id = try values.decodeIfPresent(Int.self, forKey: .id) ?? Int()
		parent = try values.decodeIfPresent(ODParent.self, forKey: .parent)  //?? ODParent()
		//--WW serviceId = try values.decodeIfPresent(String.self, forKey: .serviceId) ?? String()
        
        
        if let value = try? values.decode(Int.self, forKey: .serviceId) {
            serviceId = String(value)
        } else {
            serviceId = try? values.decode(String.self, forKey: .serviceId)
            
        }
        
        
		//--WW status = try values.decodeIfPresent(String.self, forKey: .status) ?? String()
        
        if let value = try? values.decode(Int.self, forKey: .status) {
            status = String(value)
        } else {
            status = try? values.decode(String.self, forKey: .status)
        }
		title = try values.decodeIfPresent(String.self, forKey: .title) ?? String()
		updatedAt = try values.decodeIfPresent(String.self, forKey: .updatedAt) ?? String()
	}


}
