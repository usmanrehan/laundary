//
//	ODOrderDetail.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

class ODOrderDetail : Codable {

	let amount : Float?
	let createdAt : String?
	let deletedAt : String?
	let id : Int?
	let itemDetail : ODItemDetail?
	let itemId : Int?
	let orderId : Int?
	let quantity : Int?
	let status : String?
	let updatedAt : String?
	let userId : Int?


	enum CodingKeys: String, CodingKey {
		case amount = "amount"
		case createdAt = "created_at"
		case deletedAt = "deleted_at"
		case id = "id"
		case itemDetail = "item_detail"
		case itemId = "item_id"
		case orderId = "order_id"
		case quantity = "quantity"
		case status = "status"
		case updatedAt = "updated_at"
		case userId = "user_id"
	}
	required init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		amount = try values.decodeIfPresent(Float.self, forKey: .amount) ?? Float()
		createdAt = try values.decodeIfPresent(String.self, forKey: .createdAt) ?? String()
		deletedAt = try values.decodeIfPresent(String.self, forKey: .deletedAt) ?? String()
		id = try values.decodeIfPresent(Int.self, forKey: .id) ?? Int()
		itemDetail = try values.decodeIfPresent(ODItemDetail.self, forKey: .itemDetail)  //?? ODItemDetail()
		itemId = try values.decodeIfPresent(Int.self, forKey: .itemId) ?? Int()
		orderId = try values.decodeIfPresent(Int.self, forKey: .orderId) ?? Int()
		quantity = try values.decodeIfPresent(Int.self, forKey: .quantity) ?? Int()
		//--WW status = try values.decodeIfPresent(String.self, forKey: .status) ?? String()
        if let value = try? values.decode(Int.self, forKey: .status) {
            status = String(value)
        } else {
            status = try? values.decode(String.self, forKey: .status)
            
        }
        
		updatedAt = try values.decodeIfPresent(String.self, forKey: .updatedAt) ?? String()
		userId = try values.decodeIfPresent(Int.self, forKey: .userId) ?? Int()
	}


}
