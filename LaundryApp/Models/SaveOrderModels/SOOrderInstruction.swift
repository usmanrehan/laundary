//
//	SOOrderInstruction.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

class SOOrderInstruction : Codable {

	let atMyDoor : Int?
	let callMeBeforeDelivery : Int?
	let callMeBeforePickup : Int?
	let createdAt : String?
	let id : Int?
	let isFold : Int?
	let orderId : Int?
	let other : String?
	let updatedAt : String?
	let userId : Int?


	enum CodingKeys: String, CodingKey {
		case atMyDoor = "at_my_door"
		case callMeBeforeDelivery = "call_me_before_delivery"
		case callMeBeforePickup = "call_me_before_pickup"
		case createdAt = "created_at"
		case id = "id"
		case isFold = "is_fold"
		case orderId = "order_id"
		case other = "other"
		case updatedAt = "updated_at"
		case userId = "user_id"
	}
	required init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		
	
	
		createdAt = try values.decodeIfPresent(String.self, forKey: .createdAt) ?? String()
		id = try values.decodeIfPresent(Int.self, forKey: .id) ?? Int()
		//--ww isFold = try values.decodeIfPresent(Int.self, forKey: .isFold) ?? Int()
        
        if let value = try? values.decode(Bool.self, forKey: .isFold) {
            isFold = (value == true) ? 1 : 0
        } else {
            isFold = try? values.decode(Int.self, forKey: .isFold)
        }
     
     //--ww    atMyDoor = try values.decodeIfPresent(Int.self, forKey: .atMyDoor) ?? Int()
        if let value = try? values.decode(Bool.self, forKey: .atMyDoor) {
            atMyDoor = (value == true) ? 1 : 0
        } else {
            atMyDoor = try? values.decode(Int.self, forKey: .atMyDoor)
        }
        
     //--ww   callMeBeforeDelivery = try values.decodeIfPresent(Int.self, forKey: .callMeBeforeDelivery) ?? Int()
        if let value = try? values.decode(Bool.self, forKey: .callMeBeforeDelivery) {
            callMeBeforeDelivery = (value == true) ? 1 : 0
        } else {
            callMeBeforeDelivery = try? values.decode(Int.self, forKey: .callMeBeforeDelivery)
        }
        
        //---ww    callMeBeforePickup = try values.decodeIfPresent(Int.self, forKey: .callMeBeforePickup) ?? Int()
        
        if let value = try? values.decode(Bool.self, forKey: .callMeBeforePickup) {
            callMeBeforePickup = (value == true) ? 1 : 0
        } else {
            callMeBeforePickup = try? values.decode(Int.self, forKey: .callMeBeforePickup)
        }
        
		orderId = try values.decodeIfPresent(Int.self, forKey: .orderId) ?? Int()
		other = try values.decodeIfPresent(String.self, forKey: .other) ?? String()
		updatedAt = try values.decodeIfPresent(String.self, forKey: .updatedAt) ?? String()
		userId = try values.decodeIfPresent(Int.self, forKey: .userId) ?? Int()
	}


}
