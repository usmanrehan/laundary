//
//	SOOrder.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

class SOOrder : Codable {

	let amount : Float?
	let createdAt : String?
	let deliveryAmount : Float?
	let deliveryDate : String?
	let deliverySlot : String?
	let dropLocation : String?
	let dropType : String?
    let dropupLatitude : Double?
	let dropupLongitude : Double?
	let id : Int?
	let orderInstructions : SOOrderInstruction?
	let paymentType : String?
	let pickType : String?
	let pickupDate : String?
	let pickupLatitude : Double?
	let pickupLocation : String?
	let pickupLongitude : Double?
	let slot : String?
	let totalAmount : Float?
	let updatedAt : String?
	let userId : Int?
    let redeemdCode : String?
    let redeemdAmount : Float?


	enum CodingKeys: String, CodingKey {
		case amount = "amount"
		case createdAt = "created_at"
		case deliveryAmount = "delivery_amount"
		case deliveryDate = "delivery_date"
		case deliverySlot = "delivery_slot"
		case dropLocation = "drop_location"
		case dropType = "drop_type"
		case dropupLatitude = "dropup_latitude"
		case dropupLongitude = "dropup_longitude"
		case id = "id"
		case orderInstructions = "order_instructions"
		case paymentType = "payment_type"
		case pickType = "pick_type"
		case pickupDate = "pickup_date"
		case pickupLatitude = "pickup_latitude"
		case pickupLocation = "pickup_location"
		case pickupLongitude = "pickup_longitude"
		case slot = "slot"
		case totalAmount = "total_amount"
		case updatedAt = "updated_at"
		case userId = "user_id"
        case redeemdCode = "redeemed_code"
        case redeemdAmount  = "redeemed_code_amount"
	}
	required init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		amount = try values.decodeIfPresent(Float.self, forKey: .amount) ?? Float()
		createdAt = try values.decodeIfPresent(String.self, forKey: .createdAt) ?? String()
		deliveryAmount = try values.decodeIfPresent(Float.self, forKey: .deliveryAmount) ?? Float()
		deliveryDate = try values.decodeIfPresent(String.self, forKey: .deliveryDate) ?? String()
		//--WW deliverySlot = try values.decodeIfPresent(String.self, forKey: .deliverySlot) ?? String()
        
        if let value = try? values.decode(Int.self, forKey: .deliverySlot) {
            deliverySlot = String(value)
        } else {
            deliverySlot = try? values.decode(String.self, forKey: .deliverySlot)
        }
        
        
		dropLocation = try values.decodeIfPresent(String.self, forKey: .dropLocation) ?? String()
		dropType = try values.decodeIfPresent(String.self, forKey: .dropType) ?? String()
		dropupLatitude = try values.decodeIfPresent(Double.self, forKey: .dropupLatitude) ?? Double()
		dropupLongitude = try values.decodeIfPresent(Double.self, forKey: .dropupLongitude) ?? Double()
		id = try values.decodeIfPresent(Int.self, forKey: .id) ?? Int()
		orderInstructions = try values.decodeIfPresent(SOOrderInstruction.self, forKey: .orderInstructions)  //?? SOOrderInstruction()
		paymentType = try values.decodeIfPresent(String.self, forKey: .paymentType) ?? String()
		pickType = try values.decodeIfPresent(String.self, forKey: .pickType) ?? String()
		pickupDate = try values.decodeIfPresent(String.self, forKey: .pickupDate) ?? String()
		pickupLatitude = try values.decodeIfPresent(Double.self, forKey: .pickupLatitude) ?? Double()
		pickupLocation = try values.decodeIfPresent(String.self, forKey: .pickupLocation) ?? String()
		pickupLongitude = try values.decodeIfPresent(Double.self, forKey: .pickupLongitude) ?? Double()
		//--WW slot = try values.decodeIfPresent(String.self, forKey: .slot) ?? String()
        
        if let value = try? values.decode(Int.self, forKey: .slot) {
            slot = String(value)
        } else {
            slot = try? values.decode(String.self, forKey: .slot)
        }
        
		totalAmount = try values.decodeIfPresent(Float.self, forKey: .totalAmount) ?? Float()
		updatedAt = try values.decodeIfPresent(String.self, forKey: .updatedAt) ?? String()
		userId = try values.decodeIfPresent(Int.self, forKey: .userId) ?? Int()
        
        redeemdCode = try values.decodeIfPresent(String.self, forKey: .redeemdCode) ?? String()
        redeemdAmount = try values.decodeIfPresent(Float.self, forKey: .redeemdAmount) ?? Float()
	}


}
