//
//	SOResult.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

class SOResult : Codable {

	let order : SOOrder?


	enum CodingKeys: String, CodingKey {
		case order
	}
	required init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		order = try values.decodeIfPresent(SOOrder.self, forKey: .order)  //?? SOOrder()
	}


}