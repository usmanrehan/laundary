//
//	CNCountNotification.swift
//
//	Create by CMS Testing on 12/4/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

class CNCountNotification : Codable {

	let code : Int?
	let message : String?
	let result : CNResult?
	let userBlocked : Int?
	let pages : Int?


	enum CodingKeys: String, CodingKey {
		case code = "Code"
		case message = "Message"
		case result = "Result"
		case userBlocked = "UserBlocked"
		case pages = "pages"
	}
	required init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		code = try values.decodeIfPresent(Int.self, forKey: .code) ?? Int()
		message = try values.decodeIfPresent(String.self, forKey: .message) ?? String()
		result = try values.decodeIfPresent(CNResult.self, forKey: .result)  //?? CNResult()
		userBlocked = try values.decodeIfPresent(Int.self, forKey: .userBlocked) ?? Int()
		pages = try values.decodeIfPresent(Int.self, forKey: .pages) ?? Int()
	}


}
