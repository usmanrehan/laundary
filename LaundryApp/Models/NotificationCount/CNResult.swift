//
//	CNResult.swift
//
//	Create by CMS Testing on 12/4/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

class CNResult : Codable {

	let count : Int?


	enum CodingKeys: String, CodingKey {
		case count = "count"
	}
	required init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		count = try values.decodeIfPresent(Int.self, forKey: .count) ?? Int()
	}


}