//
//	GResult.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

class GResult : Codable {

	let email : String?
	let phone : String?


	enum CodingKeys: String, CodingKey {
		case email = "email"
		case phone = "phone"
	}
	required init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		email = try values.decodeIfPresent(String.self, forKey: .email) ?? String()
		//--WW phone = try values.decodeIfPresent(Int.self, forKey: .phone) ?? Int()
        
        if let value = try? values.decode(Int.self, forKey: .phone) {
            phone = String(value)
        } else {
            phone = try? values.decode(String.self, forKey: .phone)
        }
        
	}


}
