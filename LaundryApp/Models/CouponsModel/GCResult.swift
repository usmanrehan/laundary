//
//	GCResult.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

class GCResult : Codable {

	let amount : Float?
	let code : String?
	let couponId : String?
	let createdAt : String?
	let deletedAt : String?
	let descriptionField : String?
	let endDate : String?
	let id : Int?
	let isMarkiting : String?
	let isRedeemed : String?
	let startDate : String?
	let status : String?
	let title : String?
	let type : String?
	let updatedAt : String?
	let userId : String?


	enum CodingKeys: String, CodingKey {
		case amount = "amount"
		case code = "code"
		case couponId = "coupon_id"
		case createdAt = "created_at"
		case deletedAt = "deleted_at"
		case descriptionField = "description"
		case endDate = "end_date"
		case id = "id"
		case isMarkiting = "is_markiting"
		case isRedeemed = "is_redeemed"
		case startDate = "start_date"
		case status = "status"
		case title = "title"
		case type = "type"
		case updatedAt = "updated_at"
		case userId = "user_id"
	}
	required init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		amount = try values.decodeIfPresent(Float.self, forKey: .amount) ?? Float()
	//--WW	code = try values.decodeIfPresent(String.self, forKey: .code) ?? String()
        
        if let value = try? values.decode(Int.self, forKey: .code) {
            code = String(value)
        } else {
            code = try? values.decode(String.self, forKey: .code)
            
        }
        
        
        
		//--WW couponId = try values.decodeIfPresent(String.self, forKey: .couponId) ?? String()
        
        if let value = try? values.decode(Int.self, forKey: .couponId) {
            couponId = String(value)
        } else {
            couponId = try? values.decode(String.self, forKey: .couponId)
            
        }
        
        
		createdAt = try values.decodeIfPresent(String.self, forKey: .createdAt) ?? String()
		deletedAt = try values.decodeIfPresent(String.self, forKey: .deletedAt) ?? String()
		descriptionField = try values.decodeIfPresent(String.self, forKey: .descriptionField) ?? String()
		endDate = try values.decodeIfPresent(String.self, forKey: .endDate) ?? String()
		id = try values.decodeIfPresent(Int.self, forKey: .id) ?? Int()
        
		//--WW isMarkiting = try values.decodeIfPresent(String.self, forKey: .isMarkiting) ?? String()
        
        if let value = try? values.decode(Int.self, forKey: .isMarkiting) {
            isMarkiting = String(value)
        } else {
            isMarkiting = try? values.decode(String.self, forKey: .isMarkiting)
            
        }
        
        
	//--WW 	isRedeemed = try values.decodeIfPresent(String.self, forKey: .isRedeemed) ?? String()
        
        if let value = try? values.decode(Int.self, forKey: .isRedeemed) {
            isRedeemed = String(value)
        } else {
            isRedeemed = try? values.decode(String.self, forKey: .isRedeemed)
            
        }
        
		startDate = try values.decodeIfPresent(String.self, forKey: .startDate) ?? String()
        
		//--WW status = try values.decodeIfPresent(String.self, forKey: .status) ?? String()
        
        if let value = try? values.decode(Int.self, forKey: .status) {
            status = String(value)
        } else {
            status = try? values.decode(String.self, forKey: .status)
            
        }
        
        
		title = try values.decodeIfPresent(String.self, forKey: .title) ?? String()
		type = try values.decodeIfPresent(String.self, forKey: .type) ?? String()
		updatedAt = try values.decodeIfPresent(String.self, forKey: .updatedAt) ?? String()
		//--ww userId = try values.decodeIfPresent(String.self, forKey: .userId) ?? String()
        
        
        if let value = try? values.decode(Int.self, forKey: .userId) {
            userId = String(value)
        } else {
            userId = try? values.decode(String.self, forKey: .userId)
            
        }
        
	}


}
