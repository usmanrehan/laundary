//
//	IOResult.swift
//
//	Create by CMS Testing on 30/3/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

class IOResult : Codable {

	let instanceText : String?


	enum CodingKeys: String, CodingKey {
		case instanceText = "instanceText"
	}
	required init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		instanceText = try values.decodeIfPresent(String.self, forKey: .instanceText) ?? String()
	}


}
