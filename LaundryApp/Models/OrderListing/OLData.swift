//
//	OLData.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import Realm
import RealmSwift

class OLData : Object, Codable {

	 @objc dynamic var amount : Float = 0.0
	 @objc dynamic var createdAt : String?
	 @objc dynamic var deletedAt : String?
	 @objc dynamic var deliveryAmount : Float = 0.0
	 @objc dynamic var deliveryDate : String?
	 @objc dynamic var deliverySlot : String?
	 @objc dynamic var dropLocation : String?
	 @objc dynamic var dropType : String?
	 @objc dynamic var dropupLatitude : Double = 0.0
	 @objc dynamic var dropupLongitude : Double = 0.0
	 @objc dynamic var id : Int = 0
	 @objc dynamic var orderStatus : Int = 0
	 @objc dynamic var paymentResponce : String?
	 @objc dynamic var paymentType : String?
	 @objc dynamic var pickType : String?
	 @objc dynamic var pickupDate : String?
	 @objc dynamic var pickupLatitude : Double = 0.0
	 @objc dynamic var pickupLocation : String?
	 @objc dynamic var pickupLongitude : Double = 0.0
	 @objc dynamic var slot : String?
	 @objc dynamic var status : String?
	 @objc dynamic var totalAmount : Float = 0.0
	 @objc dynamic var updatedAt : String?
	 @objc dynamic var userId : Int = 0


	enum CodingKeys: String, CodingKey {
		case amount = "amount"
		case createdAt = "created_at"
		case deletedAt = "deleted_at"
		case deliveryAmount = "delivery_amount"
		case deliveryDate = "delivery_date"
		case deliverySlot = "delivery_slot"
		case dropLocation = "drop_location"
		case dropType = "drop_type"
		case dropupLatitude = "dropup_latitude"
		case dropupLongitude = "dropup_longitude"
		case id = "id"
		case orderStatus = "order_status"
		case paymentResponce = "payment_responce"
		case paymentType = "payment_type"
		case pickType = "pick_type"
		case pickupDate = "pickup_date"
		case pickupLatitude = "pickup_latitude"
		case pickupLocation = "pickup_location"
		case pickupLongitude = "pickup_longitude"
		case slot = "slot"
		case status = "status"
		case totalAmount = "total_amount"
		case updatedAt = "updated_at"
		case userId = "user_id"
	}
	convenience required init(from decoder: Decoder) throws {
         self.init()
		let values = try decoder.container(keyedBy: CodingKeys.self)
		amount = try values.decodeIfPresent(Float.self, forKey: .amount) ?? Float()
		createdAt = try values.decodeIfPresent(String.self, forKey: .createdAt) ?? String()
		deletedAt = try values.decodeIfPresent(String.self, forKey: .deletedAt) ?? String()
		deliveryAmount = try values.decodeIfPresent(Float.self, forKey: .deliveryAmount) ?? Float()
		deliveryDate = try values.decodeIfPresent(String.self, forKey: .deliveryDate) ?? String()
        
		//--WW deliverySlot = try values.decodeIfPresent(String.self, forKey: .deliverySlot) ?? String()
        
        if let value = try? values.decode(Int.self, forKey: .deliverySlot) {
            deliverySlot = String(value)
        } else {
            deliverySlot = try? values.decode(String.self, forKey: .deliverySlot)
            
        }
        
        
		dropLocation = try values.decodeIfPresent(String.self, forKey: .dropLocation) ?? String()
        
	//--WW	dropType = try values.decodeIfPresent(String.self, forKey: .dropType) ?? String()
        if let value = try? values.decode(Int.self, forKey: .dropType) {
            dropType = String(value)
        } else {
            dropType = try? values.decode(String.self, forKey: .dropType)
            
        }
        
        
        
		dropupLatitude = try values.decodeIfPresent(Double.self, forKey: .dropupLatitude) ?? Double()
		dropupLongitude = try values.decodeIfPresent(Double.self, forKey: .dropupLongitude) ?? Double()
		id = try values.decodeIfPresent(Int.self, forKey: .id) ?? Int()
		orderStatus = try values.decodeIfPresent(Int.self, forKey: .orderStatus) ?? Int()
        
       
        
        
		paymentResponce = try values.decodeIfPresent(String.self, forKey: .paymentResponce) ?? String()
		paymentType = try values.decodeIfPresent(String.self, forKey: .paymentType) ?? String()
		pickType = try values.decodeIfPresent(String.self, forKey: .pickType) ?? String()
		pickupDate = try values.decodeIfPresent(String.self, forKey: .pickupDate) ?? String()
		pickupLatitude = try values.decodeIfPresent(Double.self, forKey: .pickupLatitude) ?? Double()
		pickupLocation = try values.decodeIfPresent(String.self, forKey: .pickupLocation) ?? String()
		pickupLongitude = try values.decodeIfPresent(Double.self, forKey: .pickupLongitude) ?? Double()
		slot = try values.decodeIfPresent(String.self, forKey: .slot) ?? String()
        
		//--WW status = try values.decodeIfPresent(String.self, forKey: .status) ?? String()
        if let value = try? values.decode(Int.self, forKey: .status) {
            status = String(value)
        } else {
            status = try? values.decode(String.self, forKey: .status)
            
        }
        
		totalAmount = try values.decodeIfPresent(Float.self, forKey: .totalAmount) ?? Float()
		updatedAt = try values.decodeIfPresent(String.self, forKey: .updatedAt) ?? String()
		userId = try values.decodeIfPresent(Int.self, forKey: .userId) ?? Int()
	}


    required init() {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    
}
