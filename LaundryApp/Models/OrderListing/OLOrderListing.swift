//
//	OLOrderListing.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import Realm
import RealmSwift

class OLOrderListing : Object , Decodable {

    @objc dynamic var code : Int = 0
    @objc dynamic var message : String?
    @objc dynamic var userBlocked : Int = 0
	@objc dynamic var pages : Int = 0
    @objc dynamic var result : OLResult?


	enum CodingKeys: String, CodingKey {
		case code = "Code"
		case message = "Message"
		case result = "Result"
		case userBlocked = "UserBlocked"
		case pages = "pages"
	}
    
	convenience required init(from decoder: Decoder) throws {
         self.init()
		let values = try decoder.container(keyedBy: CodingKeys.self)
		code = try values.decodeIfPresent(Int.self, forKey: .code) ?? Int()
		message = try values.decodeIfPresent(String.self, forKey: .message) ?? String()
		result = try values.decodeIfPresent(OLResult.self, forKey: .result)  //?? OLResult()
		userBlocked = try values.decodeIfPresent(Int.self, forKey: .userBlocked) ?? Int()
		pages = try values.decodeIfPresent(Int.self, forKey: .pages) ?? Int()
       
    }
    
    required init() {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    

}
