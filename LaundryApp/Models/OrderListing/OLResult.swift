//
//	OLResult.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import Realm
import RealmSwift

class OLResult : Object, Decodable {

     var data  = List<OLData>()
	@objc dynamic var pages : Int = 0


	enum CodingKeys: String, CodingKey {
		case data = "data"
		case pages = "pages"
	}
	convenience required init(from decoder: Decoder) throws {
         self.init()
		let values = try decoder.container(keyedBy: CodingKeys.self)
		let dataArray = try values.decodeIfPresent([OLData].self, forKey: .data) ?? [OLData]()
        data.append(objectsIn: dataArray)
		pages = try values.decodeIfPresent(Int.self, forKey: .pages) ?? Int()
	}
    
    
    required init() {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
  
    

}
