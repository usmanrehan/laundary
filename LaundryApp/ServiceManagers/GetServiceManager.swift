//
//  GetServiceManager.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/12/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import Foundation
import Alamofire
import RealmSwift
import MOLH

class GetServiceManager: AFManagerProtocol {
    
    let realm = try! Realm()
    var isSuccess = false
    var htmlString = ""
    var serviceData  : [SLService]?
    func api(_ param: AFParam, completion: @escaping () -> Void) {
        
    }
        
    func apiCall(_ param: AFParam, completion: @escaping () -> Void , isSpinnerNeeded : Bool) {
        
    //   let res = self.realm.objects(SLResult.self)
     //if res.count < 0 {
     //       print(res)
      //      self.isSuccess = true
      //      self.serviceData = Array(res[0].services)
      //      DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
       //          completion()
       //     })
           
     //  }else{
            
         self.isSuccess = false
        //Request
        AFNetwork.shared.apiRequest(param, isSpinnerNeeded: isSpinnerNeeded, success: { (response) in
            
            guard let data = response else { return }
            
            do {
                let decoder = JSONDecoder()
                let model = try decoder.decode(SLServices.self, from: data)
                
                //check success case from server
                if model.code == ServiceCodes.successCode {
                    self.isSuccess = true
                    self.serviceData = Array(model.result!.services)
                   
//                  try! self.realm.write({
//                    self.realm.add(model.result!)
//                    })
                    
                }else{
                    Alert.showMsg(msg: model.message ?? "Server not responding")
                }
                
            } catch let err {
                
                print("Err", err)
            }
            
            completion()
        }) { (error) in
            
            completion()
        }
        
   // }
  }
}

extension GetServiceManager {
    // parameters : [String : AnyObject]
    func params() -> AFParam {
        
        let headers: [String : String] = [:]
        
        let param = AFParam(endpoint: "get-services", params:[:], headers: headers, method: .post, parameterEncoding: JSONEncoding.default, images: [])
        
        return param
    }
}

