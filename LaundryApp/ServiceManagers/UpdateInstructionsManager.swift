//
//  UpdateInstructionsManager.swift
//  LaundryApp
//
//  Created by Waqas Ali on 4/16/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import Foundation
import Alamofire

class UpdateInstructionsManager: AFManagerProtocol {
    
    
    var isSuccess = false
    
    
    func api(_ param: AFParam, completion: @escaping () -> Void) {
        
        
        self.isSuccess = false
        
        //Request
        AFNetwork.shared.apiRequest(param, isSpinnerNeeded: false, success: { (response) in
            
            guard let data = response else { return }
            
            do {
                let decoder = JSONDecoder()
                let model = try decoder.decode(LoginModel.self, from: data)
                
                //check success case from server
                if model.code == ServiceCodes.successCode {
                    self.isSuccess = true
                     print(response)
                    CurrentUser.data =  model.result?.user
                    UserDefaults.standard.set(try? PropertyListEncoder().encode(CurrentUser.data), forKey:Login.userData)
                    
                    
                }else{
                }
                
            } catch let err {
                
                print("Err", err)
            }
            
            completion()
        }) { (error) in
            
            completion()
        }
    }
}

extension UpdateInstructionsManager {
    // parameters : [String : AnyObject]
    func params(parameters : [String : AnyObject]) -> AFParam {
        
        let headers: [String : String] = ["Authorization": "Bearer \(CurrentUser.token)"]
        
        let param = AFParam(endpoint: "update-instruction", params: parameters, headers: headers, method: .post, parameterEncoding: JSONEncoding.default, images: [])
        
        return param
    }
    
    
  
}
