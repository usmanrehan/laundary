//
//  InstantOrderManager.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/30/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import Foundation
import Alamofire

class InstantOrderManager: AFManagerProtocol {
    
    
    var isSuccess = false
    var instanceText = ""
    
    func api(_ param: AFParam, completion: @escaping () -> Void) {
        
        
        self.isSuccess = false
        
        //Request
        AFNetwork.shared.apiRequest(param, isSpinnerNeeded: false, success: { (response) in
            
            guard let data = response else { return }
            
            do {
                let decoder = JSONDecoder()
                let model = try decoder.decode(IOInstantOrder.self, from: data)
                
                //check success case from server
                if model.code == ServiceCodes.successCode {
                    self.isSuccess = true
                  self.instanceText = model.result?.instanceText ?? ""
                    
                    
                }else{
                  //--ww  Alert.showMsg(msg: model.message ?? "Server not responding")
                }
                
            } catch let err {
                
                print("Err", err)
            }
            
            completion()
        }) { (error) in
            
            completion()
        }
    }
}

extension InstantOrderManager {
    // parameters : [String : AnyObject]
    func params() -> AFParam {
        
        let headers: [String : String] = ["Authorization": "Bearer \(CurrentUser.token)"]
        
        let param = AFParam(endpoint: "instance-order-text", params: [:], headers: headers, method: .post, parameterEncoding: JSONEncoding.default, images: [])
        
        return param
    }
}
