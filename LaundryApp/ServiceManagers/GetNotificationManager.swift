//
//  GetNotificationManager.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/22/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import Foundation
import Alamofire



class GetNotificationManager: AFManagerProtocol {
    
    var isSuccess = false
    var message : String?
    var notificationData : [GNNotification] = []
    var totalPages = 0
    var unreadCount = 0
    
    
    func api(_ param: AFParam, completion: @escaping () -> Void) { }
    
  func apiCall(_ param: AFParam, showSpinner : Bool, completion: @escaping () -> Void) {
      self.isSuccess = false
        self.message = ""
        //Request
        AFNetwork.shared.apiRequest(param, isSpinnerNeeded: showSpinner, success: { (response) in
            
            guard let data = response else { return }
            
            do {
                let decoder = JSONDecoder()
                let model = try decoder.decode(GNGetNotification.self, from: data)
                
                //check success case from server
                if model.code == ServiceCodes.successCode {
                    self.isSuccess = true
                    self.message = model.message ?? "Success"
                    self.notificationData =   self.notificationData + (model.result?.notification)!
                    self.totalPages = model.pages ?? 0
                
                }else{
                    Alert.showMsg(msg: model.message ?? "Server not responding")
                }
                
            } catch let err {
                
                print("Err", err)
            }
            
            completion()
        }) { (error) in
            
            completion()
        }
    }
    
    
    func apiCallGetCount(_ param: AFParam, showSpinner : Bool, completion: @escaping () -> Void) {
        self.isSuccess = false
        self.message = ""
        self.unreadCount = 0
        //Request
        AFNetwork.shared.apiRequest(param, isSpinnerNeeded: showSpinner, success: { (response) in
            
            guard let data = response else { return }
            
            do {
                let decoder = JSONDecoder()
                let model = try decoder.decode(CNCountNotification.self, from: data)
                
                //check success case from server
                if model.code == ServiceCodes.successCode {
                    self.isSuccess = true
                    self.message = model.message ?? "Success"
                    self.unreadCount = model.result?.count ?? 0
                }else{
                    Alert.showMsg(msg: model.message ?? "Server not responding")
                }
                
            } catch let err {
                
                print("Err", err)
            }
            
            completion()
        }) { (error) in
            
            completion()
        }
    }
    
    
    
    
    func apiReadNotification(_ param: AFParam, showSpinner : Bool, completion: @escaping () -> Void) {
      
        //Request
        AFNetwork.shared.apiRequest(param, isSpinnerNeeded: showSpinner, success: { (response) in
            
            guard let data = response else { return }
            
            do {
                let decoder = JSONDecoder()
                let model = try decoder.decode(GNGetNotification.self, from: data)
                
                //check success case from server
                if model.code == ServiceCodes.successCode {
                    self.isSuccess = true
                    self.message = model.message ?? "Success"
                   
                }else{
                    Alert.showMsg(msg: model.message ?? "Server not responding")
                }
                
            } catch let err {
                
                print("Err", err)
            }
            
            completion()
        }) { (error) in
            
            completion()
        }
    }
}


extension GetNotificationManager {
    
    func params(offset: Int , limit : Int) -> AFParam {
        
        let headers: [String : String] = ["Authorization": "Bearer \(CurrentUser.token)"]
        
        let param = AFParam(endpoint: "get-notifications", params: ["user_id" :  CurrentUser.data?.id , "offset" : offset , "limit" : limit] as [String : AnyObject] , headers: headers, method: .post, parameterEncoding:JSONEncoding.default, images: [])
        
        return param
    }
    
    
    
    
    func paramsCountNotification() -> AFParam {
        
        let headers: [String : String] = ["Authorization": "Bearer \(CurrentUser.token)"]
        
        let param = AFParam(endpoint: "count-notifications", params: ["user_id" :  CurrentUser.data?.id] as [String : AnyObject] , headers: headers, method: .post, parameterEncoding:JSONEncoding.default, images: [])
        
        return param
    }
    func paramsDelete(notificationID: Int) -> AFParam {
        
        let headers: [String : String] = ["Authorization": "Bearer \(CurrentUser.token)"]
        let param = AFParam(endpoint: "delete-notification", params: ["user_id" :  CurrentUser.data?.id , "notification_id" : notificationID] as [String : AnyObject] , headers: headers, method: .post, parameterEncoding:JSONEncoding.default, images: [])
        
        return param
    }
    
    
    
    
    func paramsReadNotification(notificationID: Int) -> AFParam {
        
        let headers: [String : String] = ["Authorization": "Bearer \(CurrentUser.token)"]
        let param = AFParam(endpoint: "read-notification", params: ["user_id" :  CurrentUser.data?.id , "notification_id" : notificationID] as [String : AnyObject] , headers: headers, method: .post, parameterEncoding:JSONEncoding.default, images: [])
        
        return param
    }
    
}
