//
//  FeedbackManager.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/6/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import Foundation
import Alamofire

class FeedbackManager: AFManagerProtocol {
    var isSuccess = false
    var message = ""
    func api(_ param: AFParam, completion: @escaping () -> Void) {
        
        
        self.isSuccess = false
        
        //Request
        AFNetwork.shared.apiRequest(param, isSpinnerNeeded: true, success: { (response) in
            
            guard let data = response else { return }
            
            do {
                let decoder = JSONDecoder()
                let model = try decoder.decode(CMSTerms.self, from: data)
                
                //check success case from server
                if model.code == ServiceCodes.successCode {
                    self.isSuccess = true
                    self.message =  model.message ?? "Feedback submitted successfully" 
                    
                 
                    
                }else{
                    Alert.showMsg(msg: model.message ?? "Server not responding")
                }
                
            } catch let err {
                
                print("Err", err)
            }
            
            completion()
        }) { (error) in
            
            completion()
        }
    }
}

extension FeedbackManager {
    // parameters : [String : AnyObject]
    func params(text : String) -> AFParam {
        
       let headers: [String : String] = ["Authorization": "Bearer \(CurrentUser.token)"]
        let parameters = [
            "user_id": CurrentUser.data!.id,
            "feedback" : text
            ] as [String : AnyObject]
        
        let param = AFParam(endpoint: "feedback", params:parameters , headers: headers, method: .post, parameterEncoding: JSONEncoding.default, images: [])
        
        return param
    }
}

