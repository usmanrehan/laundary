//
//  PromoCodeManager.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/22/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import Foundation
import Alamofire



class PromoCodeManager: AFManagerProtocol {
    
    var isSuccess = false
    var message : String?
    var couponData : GCResult?
    
    
    func api(_ param: AFParam, completion: @escaping () -> Void) {
        
        //set default value
        
        self.isSuccess = false
        self.message = ""
        //Request
        AFNetwork.shared.apiRequest(param, isSpinnerNeeded: true, success: { (response) in
            
            guard let data = response else { return }
            
            do {
                let decoder = JSONDecoder()
                let model = try decoder.decode(GCPromoCode.self, from: data)
                
                //check success case from server
                if model.code == ServiceCodes.successCode {
                    self.isSuccess = true
                    self.message = model.message ?? "Success"
                    self.couponData = model.result
                    
                }else{
                    Alert.showMsg(msg: model.message ?? "Server not responding")
                }
                
            } catch let err {
                
                print("Err", err)
            //--  Alert.showMsg(msg:"Invalid coupon or not assign to this user")
               
            }
            
            completion()
        }) { (error) in
            
            completion()
        }
    }
}


extension PromoCodeManager {
    
    func params(code : String) -> AFParam {
        
        let headers: [String : String] = ["Authorization": "Bearer \(CurrentUser.token)"]
        //--ww CurrentUser.data?.id
        let param = AFParam(endpoint: "get-coupons-detail", params: ["user_id" : CurrentUser.data?.id as AnyObject, "code" : code as AnyObject] , headers: headers, method: .post, parameterEncoding:JSONEncoding.default, images: [])
        
        return param
    }
    
}
