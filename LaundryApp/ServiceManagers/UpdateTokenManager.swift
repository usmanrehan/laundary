//
//  UpdateTokenManager.swift
//  LaundryApp
//
//  Created by Waqas Ali on 4/5/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import Foundation
import Alamofire

class UpdateTokenManager: AFManagerProtocol {
    
    
    var isSuccess = false

    
    func api(_ param: AFParam, completion: @escaping () -> Void) {
        
        
        self.isSuccess = false
        
        //Request
        AFNetwork.shared.apiRequest(param, isSpinnerNeeded: false, success: { (response) in
            
            guard let data = response else { return }
            
            do {
                let decoder = JSONDecoder()
                let model = try decoder.decode(IOInstantOrder.self, from: data)
                
                //check success case from server
                if model.code == ServiceCodes.successCode {
                    self.isSuccess = true
                print(response)
                    
                    
                }else{
                }
                
            } catch let err {
                
                print("Err", err)
            }
            
            completion()
        }) { (error) in
            
            completion()
        }
    }
}

extension UpdateTokenManager {
    // parameters : [String : AnyObject]
    func params() -> AFParam {
        
        let headers: [String : String] = ["Authorization": "Bearer \(CurrentUser.token)"]
        
        let param = AFParam(endpoint: "device-token", params: ["user_id": CurrentUser.data?.id ?? 0 , "device_type" : "ios" , "device_token" : CurrentUser.deviceToken ] as [String : AnyObject], headers: headers, method: .post, parameterEncoding: JSONEncoding.default, images: [])
        
        return param
    }
    
    
    func updateToken(completed : @escaping () -> Void )  {
        let params = self.params()
        
        if CurrentUser.data?.id == nil {
            return
        }
        
        if let id = CurrentUser.data?.id , id == 0{
            return
        }
        self.api(params) {
            print("called on background")
            if self.isSuccess == true {
                completed()
            }
        }
    }
}
