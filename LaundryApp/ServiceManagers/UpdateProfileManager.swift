//
//  UpdateProfileManager.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/5/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import Foundation
import Alamofire

class UpdateProfileManager: AFManagerProtocol {
    
     var isSuccess = false
     var message = ""
    
    func api(_ param: AFParam, completion: @escaping () -> Void) {
        
      AFNetwork.shared.apiRequestUpload(param, isSpinnerNeeded: true, success: { (res) in
            
          guard let data = res else { return }
            
            if  data["Code"] as! Int == ServiceCodes.successCode {
                 self.isSuccess = true
                self.message = data["Message"] as! String
                
                let result = data["Result"] as! Dictionary<String, Any>
                let user = result["user"] as! Dictionary<String, Any>
                
                CurrentUser.data?.name = user["name"] as? String
                CurrentUser.data?.address = user["address"] as? String
                CurrentUser.data?.image = user["image"] as? String
                CurrentUser.data?.phone = user["phone"] as? String
                CurrentUser.data?.notificationStatus = user["notification_status"] as? String
                
                
                  UserDefaults.standard.set(try? PropertyListEncoder().encode(CurrentUser.data), forKey:Login.userData)
            }else{
                Alert.showMsg(msg: data["Message"] as? String ?? "Server not responding")
            }
            
             completion()
       }) { (err) in
            print(err)
        
         completion()
        }
    }
    
    
    func apiNotification(_ param: AFParam, completion: @escaping () -> Void) {
        
        //set default value
        
        self.isSuccess = false
        self.message = ""
        //Request
        AFNetwork.shared.apiRequest(param, isSpinnerNeeded: true, success: { (response) in
            
            guard let res = response else { return }
            
            do {
                
                let data = try JSONSerialization.jsonObject(with: res, options: []) as! [String: Any]
                
                if  data["Code"] as! Int == ServiceCodes.successCode {
                    self.isSuccess = true
                    self.message = data["Message"] as! String
                    
                    let result = data["Result"] as! Dictionary<String, Any>
                    let user = result["user"] as! Dictionary<String, Any>
                    
                    CurrentUser.data?.name = user["name"] as? String
                    CurrentUser.data?.address = user["address"] as? String
                    CurrentUser.data?.image = user["image"] as? String
                    CurrentUser.data?.phone = user["phone"] as? String
                    CurrentUser.data?.notificationStatus = user["notification_status"] as? String
                    
                    
                    UserDefaults.standard.set(try? PropertyListEncoder().encode(CurrentUser.data), forKey:Login.userData)
                }else{
                    Alert.showMsg(msg: data["Message"] as? String ?? "Server not responding")
                }
                
                 completion()
                
            } catch let err {
                
                print("Err", err)
            }
            
            completion()
        }) { (error) in
            
            completion()
        }
    }
    
   
    
   
    
  
    
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
}

extension UpdateProfileManager {
   
    func params( parameters : [String : AnyObject] , imageUpload : UIImage? = nil) -> AFParam {
        
        let headers: [String : String] = ["Authorization": "Bearer \(CurrentUser.token)"]
        
        let param = AFParam(endpoint: "update-profile", params: parameters, headers: headers, method: .post, parameterEncoding: JSONEncoding.default, images: [imageUpload!])
        
        return param
    }
    
    func paramsNotificationToggle(status : Int) -> AFParam {
        
        let headers: [String : String] = ["Authorization": "Bearer \(CurrentUser.token)"]
        
        let param = AFParam(endpoint: "update-profile", params: ["user_id" :  CurrentUser.data!.id  , "notification_status" : status] as [String : AnyObject], headers: headers, method: .post, parameterEncoding: JSONEncoding.default, images: [])
        
        return param
    }
}


