//
//  OrderListingManager.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/15/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import Foundation
import Alamofire
import RealmSwift

class OrderListingManager {}

class ActiveOrderManager: AFManagerProtocol {
    
    let realm = try! Realm()  //RealmManager()
    var isSuccess = false
    var message : String?
    var activeOrders : [OLData] = []
    var totalPages = 0
 
   
    
    func api(_ param: AFParam, completion: @escaping () -> Void) { }
    
    func apiCall(_ param: AFParam, showSpinner : Bool, completion: @escaping () -> Void) {
        //set default value
        
        self.isSuccess = false
        self.message = ""
        //Request
        AFNetwork.shared.apiRequest(param, isSpinnerNeeded: showSpinner, success: { (response) in
            
            guard let data = response else { return }
            
            do {
                let decoder = JSONDecoder()
                let model = try decoder.decode(OLOrderListing.self, from: data)
                
                //check success case from server
                if model.code == ServiceCodes.successCode {
                    
//                    if showSpinner == false{
//                        self.activeOrders = []
//                    }
                    
                    self.isSuccess = true
                    self.message = model.message ?? "Success"
                    self.activeOrders =  self.activeOrders + (model.result?.data)!
                    self.totalPages = model.pages 
                    
//                    let res = self.realm.objects(OLOrderListing.self)
//
//                 if res.count > 0 {
//                        print(res)
//                    }else{
//                        try! self.realm.write({
//                            self.realm.add(model)
//                        })
//                    }
                }else{
                    Alert.showMsg(msg: model.message ?? "Server not responding")
                }
                
            } catch let err {
                
                print("Err", err)
            }
            
            completion()
        }) { (error) in
            
            completion()
        }
    }
    
}


extension ActiveOrderManager {
    
    func params(status : String , offset : Int , limit : Int) -> AFParam {
        
        let headers: [String : String] = ["Authorization": "Bearer \(CurrentUser.token)"]
       
    
        let param = AFParam(endpoint: "my-orders", params:["status": status as AnyObject, "user_id" :  CurrentUser.data?.id  as AnyObject , "offset" : offset as AnyObject , "limit" : limit as AnyObject], headers: headers, method: .post, parameterEncoding: JSONEncoding.default, images: [])
        
        return param
    }
    
}


class CompletedOrderManager: AFManagerProtocol {
    
    var isSuccess = false
    var message : String?
    var completedOrders : [OLData] = []
    var totalPages = 0
    
    func api(_ param: AFParam, completion: @escaping () -> Void) { }
        
    func apiCall(_ param: AFParam, showSpinner : Bool, completion: @escaping () -> Void) {
        
        self.isSuccess = false
        self.message = ""
        //Request
        AFNetwork.shared.apiRequest(param, isSpinnerNeeded: showSpinner, success: { (response) in
            
            guard let data = response else { return }
            
            do {
                let decoder = JSONDecoder()
                let model = try decoder.decode(OLOrderListing.self, from: data)
                
                //check success case from server
                if model.code == ServiceCodes.successCode {
                    self.isSuccess = true
                    self.message = model.message ?? "Success"
                    self.completedOrders = self.completedOrders + model.result!.data
                    self.totalPages = model.pages 
                    
                    
                }else{
                    Alert.showMsg(msg: model.message ?? "Server not responding")
                }
                
            } catch let err {
                
                print("Err", err)
            }
            
            completion()
        }) { (error) in
            
            completion()
        }
    }
    
}


extension CompletedOrderManager {
    
    func params(status : String) -> AFParam {
        
        let headers: [String : String] = ["Authorization": "Bearer \(CurrentUser.token)"]
        
        let param = AFParam(endpoint: "my-orders", params:["status": status as AnyObject, "user_id" : CurrentUser.data?.id as AnyObject,  "offset" : 0 as AnyObject , "limit" : 6 as AnyObject], headers: headers, method: .post, parameterEncoding: JSONEncoding.default, images: [])
        
        return param
    }
    
}
