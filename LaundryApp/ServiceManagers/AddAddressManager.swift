//
//  AddAddressManager.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/14/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import Foundation
import Alamofire



class AddAddressManager: AFManagerProtocol {
    
    var isSuccess = false
    var message : String?
    var addressObject : GAData?
    func api(_ param: AFParam, completion: @escaping () -> Void) {
        
        //set default value
        
        self.isSuccess = false
        self.message = ""
        //Request
        AFNetwork.shared.apiRequest(param, isSpinnerNeeded: true, success: { (response) in
            
            guard let data = response else { return }
            
            do {
                let decoder = JSONDecoder()
                let model = try decoder.decode(AAAddAddress.self, from: data)
                
                //check success case from server
                if model.code == ServiceCodes.successCode {
                    self.isSuccess = true
                    self.message = model.message ?? "Success"
                    self.addressObject = model.result?.address
                }else{
                    Alert.showMsg(msg: model.message ?? "Server not responding")
                }
                
            } catch let err {
                
                print("Err", err)
              //   Alert.showMsg(msg: model.message ?? "Server not responding")
            }
            
            completion()
        }) { (error) in
            
            completion()
        }
    }
}


extension AddAddressManager {
    
    func paramsAdd(parametes : [String : AnyObject]) -> AFParam {
        
        let headers: [String : String] = ["Authorization": "Bearer \(CurrentUser.token)"]
        
        let param = AFParam(endpoint: "add-address", params: parametes , headers: headers, method: .post, parameterEncoding: JSONEncoding.default, images: [])
        
        return param
    }
}

class UpdateAddressManager: AFManagerProtocol {
    
    var isSuccess = false
    var message : String?
    var addressObject : GAData?
    
    func api(_ param: AFParam, completion: @escaping () -> Void) {
        
        //set default value
        
        self.isSuccess = false
        self.message = ""
        //Request
        AFNetwork.shared.apiRequest(param, isSpinnerNeeded: true, success: { (response) in
            
            guard let data = response else { return }
            
            do {
                let decoder = JSONDecoder()
                let model = try decoder.decode(UAUpdateAddress.self, from: data)
                
                //check success case from server
                if model.code == ServiceCodes.successCode {
                    self.isSuccess = true
                    self.message = model.message ?? "Success"
                    self.addressObject = model.result?.address
                }else{
                    Alert.showMsg(msg: model.message ?? "Server not responding")
                }
                
            } catch let err {
                
                print("Err", err)
                //   Alert.showMsg(msg: model.message ?? "Server not responding")
            }
            
            completion()
        }) { (error) in
            
            completion()
        }
    }
}
extension UpdateAddressManager {
    
    func paramsUpdate(parametes : [String : AnyObject]) -> AFParam {
        
        let headers: [String : String] = ["Authorization": "Bearer \(CurrentUser.token)"]
        let param = AFParam(endpoint: "update-address", params: parametes , headers: headers, method: .post, parameterEncoding: JSONEncoding.default, images: [])
        
        return param
    }
    
}
