//
//  OrderCancelManager.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/26/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import Foundation
import Alamofire

class OrderCancelManager: AFManagerProtocol {
    
    var isSuccess = false
    var message : String?
    var orderData : ODOrder?

    
    
    func api(_ param: AFParam, completion: @escaping () -> Void) {
        
        //set default value
        
        self.isSuccess = false
        self.message = ""
        //Request
        AFNetwork.shared.apiRequest(param, isSpinnerNeeded: true, success: { (response) in
            
            guard let data = response else { return }
            
            do {
                let decoder = JSONDecoder()
                let model = try decoder.decode(ODDetailOrder.self, from: data)
                
                //check success case from server
                if model.code == ServiceCodes.successCode {
                    self.isSuccess = true
                    self.message = model.message ?? "Success"
                    self.orderData = model.result?.order
                    
                }else{
                    Alert.showMsg(msg: model.message ?? "Server not responding")
                }
                
            } catch let err {
                
                print("Err", err)
                //--  Alert.showMsg(msg:"Invalid coupon or not assign to this user")
                
            }
            
            completion()
        }) { (error) in
            
            completion()
        }
    }
}


extension OrderCancelManager {
    
    func paramsCancelOrder(orderId : Int , orderStatus : Int) -> AFParam {
      let headers: [String : String] = ["Authorization": "Bearer \(CurrentUser.token)"]
       let parameters = [
         "id": orderId,
        "user_id": CurrentUser.data?.id,
        "order_current_status": orderStatus,
        "order_status": 7
        ] as [String : AnyObject]
        let param = AFParam(endpoint: "cancel-order", params: parameters , headers: headers, method: .post, parameterEncoding:JSONEncoding.default, images: [])
        
        return param
    }
    
}
