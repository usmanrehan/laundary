//
//  TimeSlotManager.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/13/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import Foundation
import Alamofire



class TimeSlotManager: AFManagerProtocol {
    
    var isSuccess = false
    var message : String?
    var timeSlotData : [TSAvailableTimeSlot]?
    
    
    func api(_ param: AFParam, completion: @escaping () -> Void) {
        
        //set default value
        
        self.isSuccess = false
        self.message = ""
        //Request
        AFNetwork.shared.apiRequest(param, isSpinnerNeeded: true, success: { (response) in
            
            guard let data = response else { return }
            
            do {
                let decoder = JSONDecoder()
                let model = try decoder.decode(TSTimeSlot.self, from: data)
                
                //check success case from server
                if model.code == ServiceCodes.successCode {
                    self.isSuccess = true
                    self.message = model.message ?? "Success"
                    self.timeSlotData = model.result?.availableTimeSlot
                }else{
                    Alert.showMsg(msg: model.message ?? "Server not responding")
                }
                
            } catch let err {
                
                print("Err", err)
            }
            
            completion()
        }) { (error) in
            
            completion()
        }
    }
}


extension TimeSlotManager {
    
    func paramsTimeSlot(parametes : [String : AnyObject]) -> AFParam {
        
        let headers: [String : String] = ["Authorization": "Bearer \(CurrentUser.token)"]
        
        let param = AFParam(endpoint: "time-slot", params: parametes , headers: headers, method: .post, parameterEncoding:JSONEncoding.default, images: [])
        
        return param
    }
    
}



