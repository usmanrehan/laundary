//
//  ReportIssueManager.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/6/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import Foundation
import Alamofire

class ReportIssueManager: AFManagerProtocol {
    var isSuccess = false
    var message = ""
    func api(_ param: AFParam, completion: @escaping () -> Void) {
        
        
        self.isSuccess = false
        
        //Request
        AFNetwork.shared.apiRequest(param, isSpinnerNeeded: true, success: { (response) in
            
            guard let data = response else { return }
            
            do {
                let decoder = JSONDecoder()
                let model = try decoder.decode(CMSTerms.self, from: data)
                
                //check success case from server
                if model.code == ServiceCodes.successCode {
                    self.isSuccess = true
                    self.message = model.message ?? "You request has been submitted successfully" 
                    
                    
                    
                }else{
                    Alert.showMsg(msg: model.message ?? "Server not responding")
                }
                
            } catch let err {
                
                print("Err", err)
            }
            
            completion()
        }) { (error) in
            
            completion()
        }
    }
}

extension ReportIssueManager {
    // parameters : [String : AnyObject]
    func params(text : String, index : Int) -> AFParam {
        
        let missing = index == 1 ? 1 : 0
        let late = index == 2 ? 1 : 0
        let pay = index == 3 ? 1 : 0
        let something = index == 4 ? 1 : 0
        
        
        let headers: [String : String] = ["Authorization": "Bearer \(CurrentUser.token)"]
        let parameters = [
            "user_id": CurrentUser.data!.id,
            "title" : text,
            "missing_product": missing,
            "late_order":late,
            "payment_issue":pay,
            "something_else":something
            
            ] as [String : AnyObject]
        
        
        
        let param = AFParam(endpoint: "issues", params:parameters , headers: headers, method: .post, parameterEncoding: JSONEncoding.default, images: [])
        
        return param
    }
}

