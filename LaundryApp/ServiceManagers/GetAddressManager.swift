//
//  GetAddressManager.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/13/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import Foundation
import Alamofire

class GetAddressManager: AFManagerProtocol {
    
    
    var isSuccess = false
    var message = ""
    var addressData  : [GAData] = []
    var totalPages = 0
    func api(_ param: AFParam, completion: @escaping () -> Void) {
        
        
        self.isSuccess = false
        self.addressData = []
        //Request
        AFNetwork.shared.apiRequest(param, isSpinnerNeeded: true, success: { (response) in
            
            guard let data = response else { return }
            
            do {
                let decoder = JSONDecoder()
                let model = try decoder.decode(GAGetAddress.self, from: data)
                
                //check success case from server
                if model.code == ServiceCodes.successCode {
                    self.isSuccess = true
                    self.message = model.message ?? "Success"
                    if (model.result?.data?.count ?? 0) > 0{
                    self.addressData = (model.result?.data)!
                    self.totalPages = model.pages ?? 0
                    }
                }else{
                    Alert.showMsg(msg: model.message ?? "Server not responding")
                }
                
            } catch let err {
                
                print("Err", err)
            }
            
            completion()
        }) { (error) in
            
            completion()
        }
    }
    
    
    func apiCall(_ param: AFParam,  showSpinner : Bool, completion: @escaping () -> Void) {
        
        
        self.isSuccess = false
        self.addressData = []
        //Request
        AFNetwork.shared.apiRequest(param, isSpinnerNeeded: showSpinner, success: { (response) in
            
            guard let data = response else { return }
            
            do {
                let decoder = JSONDecoder()
                let model = try decoder.decode(GAGetAddress.self, from: data)
                
                //check success case from server
                if model.code == ServiceCodes.successCode {
                    self.isSuccess = true
                    self.message = model.message ?? "Success"
                    if (model.result?.data?.count ?? 0) > 0{
                        self.addressData = (model.result?.data)!
                        self.totalPages = model.pages ?? 0
                    }
                }else{
                    Alert.showMsg(msg: model.message ?? "Server not responding")
                }
                
            } catch let err {
                
                print("Err", err)
            }
            
            completion()
        }) { (error) in
            
            completion()
        }
    }
    
    
    func apiDelete(_ param: AFParam, completion: @escaping () -> Void) {
        
        
        self.isSuccess = false
        
        //Request
        AFNetwork.shared.apiRequest(param, isSpinnerNeeded: true, success: { (response) in
            
            guard let data = response else { return }
            
            do {
                let decoder = JSONDecoder()
                let model = try decoder.decode(GAGetAddress.self, from: data)
                
                //check success case from server
                if model.code == ServiceCodes.successCode {
                    self.isSuccess = true
                    self.message = model.message ?? "Success"
                 
                    
                }else{
                    Alert.showMsg(msg: model.message ?? "Server not responding")
                }
                
            } catch let err {
                
                print("Err", err)
            }
            
            completion()
        }) { (error) in
            
            completion()
        }
    }
}

extension GetAddressManager {
  //--ww   "user_id":3 , "offset" : 0 , "limit" : 10
    func params(offset : Int , limit : Int) -> AFParam {
        
        let headers: [String : String] = ["Authorization": "Bearer \(CurrentUser.token)"]
        let param = AFParam(endpoint: "get-address", params:["user_id": CurrentUser.data?.id as AnyObject , "offset" : offset  as AnyObject , "limit" : limit as AnyObject], headers: headers, method: .post, parameterEncoding: JSONEncoding.default, images: [])
        return param
    }
    
    
    func paramsDeleteAddress(addId : Int) -> AFParam {
        
        let headers: [String : String] = ["Authorization": "Bearer \(CurrentUser.token)"]
        let param = AFParam(endpoint: "delete-address", params:["user_id": CurrentUser.data?.id as AnyObject , "id" : addId  as AnyObject], headers: headers, method: .post, parameterEncoding: JSONEncoding.default, images: [])
        return param
    }
}

