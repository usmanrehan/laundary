//
//  ForgetPasswordManager.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/5/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import Foundation
import Alamofire

class ForgetPasswordManager{}

class FPGetCodeManager: ForgetPasswordManager , AFManagerProtocol {
    
    var isSuccess = false
    var message : String?
    var verificationCode  = 0
    
    
    func api(_ param: AFParam, completion: @escaping () -> Void) {
        
        //set default value
  
        self.isSuccess = false
        self.message = ""
        //Request
        AFNetwork.shared.apiRequest(param, isSpinnerNeeded: true, success: { (response) in
            
            guard let data = response else { return }
            
            do {
                let decoder = JSONDecoder()
                let model = try decoder.decode(FPGetCode.self, from: data)
                
                //check success case from server
                if model.code == ServiceCodes.successCode {
                    self.isSuccess = true
                    self.message = model.message ?? "We have sent you a verification code on your email" 
                    self.verificationCode = (model.result?.verificationCode)!
                  
                }else{
                    Alert.showMsg(msg: model.message ?? "Server not responding")
                }
                
            } catch let err {
                
                print("Err", err)
            }
            
            completion()
        }) { (error) in
            
            completion()
        }
    }
}


class FPCheckCodeManager: ForgetPasswordManager , AFManagerProtocol {
    
    var isSuccess = false
     var message : String?
    
    
    func api(_ param: AFParam, completion: @escaping () -> Void) {
        
        //set default value
        self.message = ""
        self.isSuccess = false
        
        //Request
        AFNetwork.shared.apiRequest(param, isSpinnerNeeded: true, success: { (response) in
            
            guard let data = response else { return }
            
            do {
                let decoder = JSONDecoder()
                let model = try decoder.decode(FPStep2CheckCode.self, from: data)
                //check success case from server
                if model.code == ServiceCodes.successCode {
                    self.isSuccess = true
                    self.message = model.message ?? "Success"
                   
                }else{
                    Alert.showMsg(msg: model.message ?? "Server not responding")
                }
                
            } catch let err {
                
                print("Err", err)
            }
            
            completion()
        }) { (error) in
            
            completion()
        }
    }
    
    
    func apiSV(_ param: AFParam, completion: @escaping () -> Void) {
        
        //set default value
        self.message = ""
        self.isSuccess = false
        
        //Request
        AFNetwork.shared.apiRequest(param, isSpinnerNeeded: true, success: { (response) in
            
            guard let data = response else { return }
            
            do {
                let decoder = JSONDecoder()
                let model = try decoder.decode(SVSignupVerify.self, from: data)
                //check success case from server
                
                if model.code == ServiceCodes.successCode {
                    self.isSuccess = true
                    self.message = model.message ?? "Success"
                    CurrentUser.data = model.result?.user
                    let userDefault = UserDefaults.standard
                    userDefault.set(true, forKey: Login.isLoggedIn)
                    userDefault.set(CurrentUser.data?.token, forKey: Login.token)
                    CurrentUser.token = CurrentUser.data?.token ?? ""
                    UserDefaults.standard.set(try? PropertyListEncoder().encode(CurrentUser.data), forKey:Login.userData)
                    
                }else{
                    Alert.showMsg(msg: model.message ?? "Server not responding")
                }
                
            } catch let err {
                
                print("Err", err)
            }
            
            completion()
        }) { (error) in
            
            completion()
        }
    }
}



class FPUpdatePasswordManager: ForgetPasswordManager , AFManagerProtocol {
    
    var isSuccess = false
    var message  = "Success"
    
    
    func api(_ param: AFParam, completion: @escaping () -> Void) {
        
        //set default value
      
        self.isSuccess = false
        self.message = ""
        //Request
        AFNetwork.shared.apiRequest(param, isSpinnerNeeded: true, success: { (response) in
            
            guard let data = response else { return }
            
            do {
                let decoder = JSONDecoder()
                let model = try decoder.decode(FPStep3UpdatePwd.self, from: data)
                
                //check success case from server
                if model.code == ServiceCodes.successCode {
                    self.isSuccess = true
                  self.message = model.message ?? "Success"
                }else{
                    Alert.showMsg(msg: model.message ?? "Server not responding")
                }
                
            } catch let err {
                
                print("Err", err)
            }
            
            completion()
        }) { (error) in
            
            completion()
        }
    }
}

extension ForgetPasswordManager {
    
    func paramsGetCode(emailID : String) -> AFParam {
        
        let headers: [String : String] = [:]
        
        let param = AFParam(endpoint: "email-password-code", params: ["email" :  emailID as AnyObject], headers: headers, method: .post, parameterEncoding: JSONEncoding.default, images: [])
        
        return param
    }
    
    
    func paramsResendCode(emailID : String) -> AFParam {
        
        let headers: [String : String] = [:]
        
        let param = AFParam(endpoint: "resend-code", params: ["email" :  emailID as AnyObject], headers: headers, method: .post, parameterEncoding: JSONEncoding.default, images: [])
        
        return param
    }
    
    
    func paramsResendCodeSignup(emailID : String) -> AFParam {
        
        let headers: [String : String] = [:]
        
        let param = AFParam(endpoint: "resend-reg-code", params: ["email" :  emailID as AnyObject], headers: headers, method: .post, parameterEncoding: JSONEncoding.default, images: [])
        
        return param
    }
    
    func paramsCheckCode(code : Int) -> AFParam {
        
        let headers: [String : String] = [:]
        
        let param = AFParam(endpoint: "verify-code", params: ["verification_code" : code as AnyObject], headers: headers, method: .post, parameterEncoding: JSONEncoding.default, images: [])
        
        return param
    }
    
    
    
    func paramsCheckCodeSignup(code : Int) -> AFParam {
        
        let headers: [String : String] = [:]
        
        let param = AFParam(endpoint: "verify-reg-code", params: ["verification_code" : code as AnyObject , "email" : FP.emailUser as AnyObject], headers: headers, method: .post, parameterEncoding: JSONEncoding.default, images: [])
        
        return param
    }
    
    func paramsUpdatePassword(pwd : String , confirmPwd : String) -> AFParam {
        
        let headers: [String : String] = [:]
        
        let parameters = [
            "email" : FP.emailUser,
            "password" : pwd,
            "password_confirmation" : confirmPwd
        ] as [String : AnyObject]
        
        let param = AFParam(endpoint: "update-password", params: parameters, headers: headers, method: .post, parameterEncoding: JSONEncoding.default, images: [])
        
        return param
    }
}


