//
//  OrderDetailManager.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/15/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import Foundation
import Alamofire

class OrderDetailManager: AFManagerProtocol {
    
    var isSuccess = false
    var message : String?
    var orderData : ODOrder?
    var mainArray = [[ODOrderDetail]]()
   
    func api(_ param: AFParam, completion: @escaping () -> Void) {
        
        //set default value
        
        self.isSuccess = false
        self.message = ""
        //Request
        AFNetwork.shared.apiRequest(param, isSpinnerNeeded: true, success: { (response) in
            
            guard let data = response else { return }
            
            do {
                let decoder = JSONDecoder()
                let model = try decoder.decode(ODDetailOrder.self, from: data)
                
                //check success case from server
                if model.code == ServiceCodes.successCode {
                    self.isSuccess = true
                    self.message = model.message ?? "Success"
                    self.orderData = model.result?.order
                    self.setDataInMainArray()
                    
                 
                }else{
                    Alert.showMsg(msg: model.message ?? "Server not responding")
                }
                
            } catch let err {
                
                print("Err", err)
            }
            
            completion()
        }) { (error) in
            
            completion()
        }
    }
    
    func setDataInMainArray() {
        if let data = self.orderData?.orderDetail {
        for i in 1...3 {
            let filtered = data.filter { $0.itemDetail?.parenttype?.parent?.id == i}
            let array = Array(filtered)
           mainArray.append(array)
        }
         print(mainArray)
        }
    }
}


extension OrderDetailManager {
    
    func params(orderID : Int) -> AFParam {
        
        let headers: [String : String] = ["Authorization": "Bearer \(CurrentUser.token)"]
        
        let param = AFParam(endpoint: "order-detail", params:["order_id": orderID as AnyObject], headers: headers, method: .post, parameterEncoding: JSONEncoding.default, images: [])
        
        return param
    }
    
}
