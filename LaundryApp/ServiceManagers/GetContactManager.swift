//
//  GetContactManager.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/26/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import Foundation
import Alamofire

class GetContactManager: AFManagerProtocol {
    
    
    var isSuccess = false
    var resultData : GResult?
    func api(_ param: AFParam, completion: @escaping () -> Void) {
        
        
        self.isSuccess = false
        
        //Request
        AFNetwork.shared.apiRequest(param, isSpinnerNeeded: true, success: { (response) in
            
            guard let data = response else { return }
            
            do {
                let decoder = JSONDecoder()
                let model = try decoder.decode(GCGetContacts.self, from: data)
                
                //check success case from server
                if model.code == ServiceCodes.successCode {
                    self.isSuccess = true
                    self.resultData = model.result
                 
                    
                }else{
                    Alert.showMsg(msg: model.message ?? "Server not responding")
                }
                
            } catch let err {
                
                print("Err", err)
            }
            
            completion()
        }) { (error) in
            
            completion()
        }
    }
}

extension GetContactManager {
    // parameters : [String : AnyObject]
    func params() -> AFParam {
        
       let headers: [String : String] = ["Authorization": "Bearer \(CurrentUser.token)"]
        
        let param = AFParam(endpoint: "get-contact", params: ["type": "terms" as AnyObject], headers: headers, method: .post, parameterEncoding: JSONEncoding.default, images: [])
        
        return param
    }
}
