//
//  HomeTopView.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/1/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit

class HomeTopView: UIView {

    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var lblFirst: BaseUILabel!
    @IBOutlet weak var lblSecond: BaseUILabel!
    
    @IBOutlet weak var lblTitle: BaseUILabel!
    
}
