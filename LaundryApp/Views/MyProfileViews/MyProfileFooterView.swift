//
//  MyProfileFooterView.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/2/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit

class MyProfileFooterView: UIView {

    @IBOutlet weak var btnUpdate: CustomButton!
    var updateAction : (()-> Void)?
    
    

    @IBAction func updateButtonTapped(_ sender: Any) {
        updateAction!()
    }
}
