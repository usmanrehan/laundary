//
//  NoRecordFoundView.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/16/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit

class NoRecordFoundView: UIView {

    @IBOutlet var contentView: UIView!
    
    
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var lblNoRecords: BaseUILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commomInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
   
        super.init(coder: aDecoder)
        commomInit()
    }
    
    
    
    func commomInit(){
        Bundle.main.loadNibNamed("NoRecordFoundView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth , .flexibleHeight]
       lblNoRecords.text = GlobalStatic.getLocalizedString("no_record")
    }

}
