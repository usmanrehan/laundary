//
//  CartView.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/7/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit

class CartView: UIView {

    @IBOutlet var contentView: UIView!
    
    
    @IBOutlet weak var imgCategory: UIImageView!
    @IBOutlet weak var lblTitle: BaseUILabel!
    @IBOutlet weak var tblListing: UITableView!
    
    let cartCellIdentifier =  "CartViewTableViewCell"
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commomInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        //--ww fatalError("init(coder:) has not been implemented")
        super.init(coder: aDecoder)
        commomInit()
    }
    
    
    
    func commomInit(){
        Bundle.main.loadNibNamed("CartView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth , .flexibleHeight]
        setupTable()
    }
    
    func setupTable() {
        tblListing.delegate = self
        tblListing.dataSource = self
        
        tblListing.register(UINib.init(nibName: cartCellIdentifier, bundle: nil), forCellReuseIdentifier: cartCellIdentifier)
        
        tblListing.reloadData()
        }
}
extension CartView : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: cartCellIdentifier) as! CartViewTableViewCell
        cell.selectionStyle = .none
        return cell
        
    }
    

    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return DesignUtility.getValueFromRatio(30)
    }
    

}

extension CartView : UITableViewDelegate {
    
    
}
