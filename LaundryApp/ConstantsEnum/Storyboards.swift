//
//  Storyboards.swift
//  LaundryApp
//
//  Created by Waqas Ali on 2/28/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import Foundation
import UIKit

public enum Storyboards : String {
    
    // As enum is extends from String then its case name is also its value
    case main = "Main"
    case walkthrough = "Walkthrough"
    case registeration = "Registeration"
    case home = "Home"
    case services = "Services"
    case orders = "Orders"
    
}

enum Navigation {
    static var currentNavigation  : UINavigationController?  = nil
}
