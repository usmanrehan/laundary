//
//  AppDelegate.swift
//  LaundryApp
//
//  Created by CMS Testing on 2/27/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import GoogleMaps
import GooglePlaces
import Realm
import RealmSwift
import MOLH
import UserNotifications
import Fabric
import Crashlytics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, MOLHResetable, UNUserNotificationCenterDelegate {
   var window: UIWindow?
    let center = UNUserNotificationCenter.current()


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        IQKeyboardManager.sharedManager().enable = true
        GMSPlacesClient.provideAPIKey(GoogleMap.key)
        GMSServices.provideAPIKey(GoogleMap.key)
        MOLH.shared.activate(true)
     //  registerForPushNotifications()
        center.delegate = self
        pushRequest(application: application)
        checkDeviceToken()
        // Check if launched from notification
        // 1
        if let notification = launchOptions?[.remoteNotification] as? [String: AnyObject] {
            // 2
           //--ww  let aps = notification["aps"] as! [String: AnyObject]
             OrderData.orderNotification = ("",0)
            print("from appLaunch", notification)
           
            let aps = notification["aps"] as! [String: AnyObject]
            let extraPayLoad = aps["extraPayLoad"] as! [String: AnyObject]
            if let type = extraPayLoad["action_type"] as? String{
            OrderData.orderNotification = (type , Int(extraPayLoad["ref_id"] as? String ?? "0")! )
            }
            
        }
    
        UIApplication.shared.applicationIconBadgeNumber = 0
             Fabric.with([Crashlytics.self])
        return true
      
    }
    
    func checkDeviceToken() {
   
        if let token = UserDefaults.standard.string(forKey: UserDefaultKey.deviceToken) {
              CurrentUser.deviceToken = token
        }
    }
    
    func reset() {
        let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
       let nav = UINavigationController.init(rootViewController: HomeViewController.loadVC())
        rootviewcontroller.rootViewController = nav
    }
    
    func getFontNames()  {
        for fontFamilyName in UIFont.familyNames{
            for fontName in UIFont.fontNames(forFamilyName: fontFamilyName){
                print("Family: \(fontFamilyName)     Font: \(fontName)")
            }
        }
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
   
    
    func registerForPushNotifications() {
        
        if #available(iOS 10.0, *) {
           
            center.delegate = self
           center.requestAuthorization(options: [.alert, .sound, .badge]) {
                (granted, error) in
                print("Permission granted: \(granted)")
                
                guard granted else {
                     //--ww self.showPermissionAlert()
                    return
                }
                
                self.getNotificationSettings()
            }
        } else {
            
            let settings = UIUserNotificationSettings(types: [.alert, .sound, .badge], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
            UIApplication.shared.registerForRemoteNotifications()
        }
    }
    
    @available(iOS 10.0, *)
    func getNotificationSettings() {
      
        center.getNotificationSettings { (settings) in
           guard settings.authorizationStatus == .authorized else { return }
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
    }
    
    
    func pushRequest(application:UIApplication)
    {
//        let center = UNUserNotificationCenter.current()
//        center.delegate = self
        // set the type as sound or badge
        center.requestAuthorization(options: [.sound,.alert,.badge]) { (granted, error) in
            // Enable or disable features based on authorization
            
            if granted
            {
                print("access granted")
            }
            else
            {
                // Utility.showAlert(title: NSLocalizedString("Push Notification", comment: ""), message: NSLocalizedString("Please grant access for push notification", comment: ""))
            }
        }
        
        application.registerForRemoteNotifications()
    }
    
    
    func showPermissionAlert() {

        Alert.showWithTwoActions(msg: "Please enable access to Notifications in the Settings app.", okBtnTitle: "Settings", okBtnAction: {
             self.gotoAppSettings()
        }, cancelBtnTitle: "Cancel", cancelBtnAction: {
            
        })
        
    }
    
    private func gotoAppSettings() {
        
        guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
            return
        }
        
        if UIApplication.shared.canOpenURL(settingsUrl) {
            UIApplication.shared.open(settingsUrl, options: [:], completionHandler: nil)
        }
    }

    func application(application: UIApplication, didRegisterUserNotificationSettings notificationSettings: UNNotificationSetting) {
        
        if notificationSettings != .none {
            application.registerForRemoteNotifications()
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        
        let token = tokenParts.joined()
        CurrentUser.deviceToken = token
       // UserDefaults.standard.set( CurrentUser.deviceToken , forKey: UserDefaultKey.deviceToken)
   
        if let savedToken = UserDefaults.standard.string(forKey: UserDefaultKey.deviceToken) {
            if savedToken != token {
               DispatchQueue.global().async(execute: {
                    UpdateTokenManager().updateToken {
                        CurrentUser.deviceToken = token
                        UserDefaults.standard.set( CurrentUser.deviceToken , forKey: UserDefaultKey.deviceToken)
                    }
                })
            
            }
        }
 
        
        print("Device Token: \(token)")
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
        print("Failed to register:", error)
    }
    
    
    func application(
        _ application: UIApplication,
        didReceiveRemoteNotification userInfo: [AnyHashable : Any],
        fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
      let aps = userInfo["aps"] as! [String: AnyObject]
      let extraPayLoad = aps["extraPayLoad"] as! [String: AnyObject]
        if let type = extraPayLoad["action_type"] as? String{
            
          GlobalStatic.showNotificationVC(type: type, id: extraPayLoad["ref_id"] as? Int ?? 0)
        }
        print("from didRecieve", userInfo) 
//
//        RNNotificationView.show(withImage: UIImage(named: "logo-small"),
//                                title: "My Laundry",
//                                message: "Your order has been completed",
//                                duration: 5,
//                                iconSize: CGSize(width: 35, height: 35),
//                                onTap: {
//                                    print("Did tap notification")
//        })
    }
   
  
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        completionHandler([.alert, .badge, .sound])
    }
    
}
