//
//  CustomTextFields.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/21/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit

class NameTextField: BaseUITextField {
    
    override func txtEditing(_ txt: UITextField) {
        
        if let typedText = self.text {
            if typedText.count > Constants.maximumLengthName{
                txt.deleteBackward()
            }
        }
    }
}


class NumberTextField: BaseUITextField {
    
    override func txtEditing(_ txt: UITextField) {
        
        if let typedText = self.text {
            if typedText.count > Constants.maximumLengthNumber{
                txt.deleteBackward()
            }
        }
    }
}
