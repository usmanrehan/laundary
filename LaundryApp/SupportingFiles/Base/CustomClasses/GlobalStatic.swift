//
//  GlobalStatic.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/14/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit
open class GlobalStatic {
    
    static func getFormatedAddress(add: String) -> String {
        let addArr = add.components(separatedBy: "|")
        var addString = addArr[0]
        for i in 1..<addArr.count {
            if !addArr[i].isEmptyStr() {
            addString = addString + ", " + addArr[i]
            }
        }
        return addString
    }
    
    class func showLoginAlert(vc : UIViewController){
        DispatchQueue.main.async {
            Alert.showWithTwoActions(msg: GlobalStatic.getLocalizedString("login_needed"), okBtnTitle: GlobalStatic.getLocalizedString("login"), okBtnAction: {
                FP.loginGuest = true
          vc.navigationController?.setViewControllers([LoginViewController.loadVC()], animated: true)
            }, cancelBtnTitle: GlobalStatic.getLocalizedString("cancel"), cancelBtnAction: {
                
            })
        }
        
    }
    
   open class func getLocalizedString(_ key : String) -> String {
        return NSLocalizedString(key, comment: "")
    }
    
    
 
    
  class func createAttributedString(fullString: String, boldStringArray: [String]) -> NSMutableAttributedString
    {
        
        let boldFontAttribute = [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 13.0)]
        
        let attributedString = NSMutableAttributedString(string:fullString)
        for ind in 0..<boldStringArray.count {
            
            let range = (fullString as NSString).range(of: boldStringArray[ind])
            attributedString.addAttributes(boldFontAttribute, range: range)
            
            
        }
        
        return attributedString
    }
    
    
    
    class func showNotificationVC(type : String , id : Int){
        
        if UserDefaults.standard.bool(forKey: Login.isLoggedIn) == true {
        switch type {
        case "order_status":
            GlobalStatic.goToOrderDetail(orderID: id)
        case "general":
            GlobalStatic.goToNotification()
        default:
             GlobalStatic.goToNotification()
            break
        }
        }
    }
    
    
    
    class func goToOrderDetail(orderID : Int){
        
        let home =  HomeViewController.loadVC()
        let orderListing =  MyOrdersViewController.loadVC()
        orderListing.SwiftPagesCurrentPageNumber(currentIndex: 1)
        let orderDetail  = OrderDetailViewController.loadVC()
        orderDetail.orderID = orderID
        
        if  Navigation.currentNavigation != nil {
            Navigation.currentNavigation?.topViewController?.dismiss(animated: true, completion: nil)
            Navigation.currentNavigation?.setViewControllers([home, orderListing , orderDetail], animated: true)
        }else{
            let nav = UINavigationController.init(rootViewController: home)
            nav.setViewControllers([home, orderListing , orderDetail], animated: true)
         }
    }
    
    
    
    
    class func goToNotification(){
        
        let home =  HomeViewController.loadVC()
        let notification =  NotificationViewController.loadVC()
   
       
        if  Navigation.currentNavigation != nil {
            Navigation.currentNavigation?.topViewController?.dismiss(animated: true, completion: nil)
            Navigation.currentNavigation?.setViewControllers([home, notification], animated: true)
        }else{
            let nav = UINavigationController.init(rootViewController: home)
            nav.setViewControllers([home, notification], animated: true)
        }
    }
    
    
    
    class func goToOrderInfo(nc : UINavigationController){
        
        let home =  HomeViewController.loadVC()
        let service =  ServicesViewController.loadVC()
        let cart  = MyCartViewController.loadVC()
        let order = OrderInformationViewController.loadVC()
     
           nc.setViewControllers([home, service , cart , order], animated: true)
        
    }
    
    
    class func saveCartData(){
        
        
        var saveDict : [String : [String:Any]] = [:]
        
        for dd in CartData.cartDict {
            
            let key = dd.key
            let q = dd.value.quantity
            let a = dd.value.amount
            let s = dd.value.section
            let i = dd.value.item
            saveDict["\(key)"] = ["quantity" : q, "amount" : a , "item" : i , "section" :s]
            }
 
        
        
      let dict = [
        CartKeys.amtFinal.rawValue : CartData.amtFinal,
        CartKeys.cartDict.rawValue : saveDict,
      //  CartKeys.instanceSurCharge : CartData.instanceSurCharge,
          CartKeys.orderDetail.rawValue : CartData.orderDetail,
         CartKeys.params.rawValue : CartData.params,
     //   CartKeys.SD : CartData.SD,
         CartKeys.totalAmount.rawValue : CartData.totalAmount,
         CartKeys.totalQuantity.rawValue : CartData.totalQuantity
        ] as [String : Any]
        
        UserDefaults.standard.set(dict, forKey: UserDefaultKey.cardData)
    }
    
    class func getCartData() {
        
   
        if let cd = UserDefaults.standard.value(forKey: UserDefaultKey.cardData) as? [String: Any] {
            CartData.amtFinal = cd[CartKeys.amtFinal.rawValue] as! Float
            
            
            
            var getDict : [Int : (quantity : Int, amount: Float , item : String , section :Int)] = [:]
            
            for dd in (cd[CartKeys.cartDict.rawValue] as? [String : [String:Any]])!  {
                
                let key = dd.key
                let q = dd.value["quantity"] as! Int
                let a = dd.value["amount"] as! Float
                let s = dd.value["section"] as! Int
                let i = dd.value["item"] as! String
                getDict[Int(key)!] = (quantity : q, amount : a , item : i , section :s)
            }
            
            
            CartData.cartDict = getDict
        //    CartData.instanceSurCharge = cd[CartKeys.instanceSurCharge.rawValue] as! Float
           CartData.orderDetail = cd[CartKeys.orderDetail.rawValue] as! [[String : AnyObject]]
           CartData.params = cd[CartKeys.params.rawValue] as! [String : AnyObject]
          //  CartData.SD = cd[CartKeys.SD.rawValue] as! SheduleData
            CartData.totalAmount = cd[CartKeys.totalAmount.rawValue] as! Float
            CartData.totalQuantity = cd[CartKeys.totalQuantity.rawValue] as! Int
            
        }
    }
    
    class func deleteCartData(){
      UserDefaults.standard.removeObject(forKey: UserDefaultKey.cardData)
    }
    
}

extension UILabel {
    
    func colorString(text: String?, coloredText: String?, color: UIColor? = .red) {
        let boldFontAttribute = [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: DesignUtility.getFontSize(fSize: 16))]
        let attributedString = NSMutableAttributedString(string: text!)
        let range = (text! as NSString).range(of: coloredText!)
        attributedString.setAttributes([NSAttributedStringKey.foregroundColor: color!],
                                       range: range)
         attributedString.addAttributes(boldFontAttribute, range: range)
        
        self.attributedText = attributedString
}
}

enum CartKeys : String {
    case cartDict
    case totalAmount
    case totalQuantity
    case params
    case orderDetail
    case instanceSurCharge
    case amtFinal
    case SD
}
