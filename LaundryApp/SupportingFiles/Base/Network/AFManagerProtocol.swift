//
//  AFManagerProtocol.swift
//  BaseProject
//
//  Created by Waqas Ali on 31/01/2018.
//  Copyright © 2018 Waqas Ali. All rights reserved.
//

import Foundation

public protocol AFManagerProtocol {
    func api(_ param: AFParam, completion: @escaping () -> Void)
}
