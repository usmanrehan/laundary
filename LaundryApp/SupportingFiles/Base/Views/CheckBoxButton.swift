//
//  CheckBoxButton.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/3/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit

public class CheckBoxButton : BaseUIButton {
    
   
}

extension CheckBoxButton{
    
    func setTitleInset() {
        self.titleEdgeInsets = UIEdgeInsetsMake(20,0,0,0)

    }
}
