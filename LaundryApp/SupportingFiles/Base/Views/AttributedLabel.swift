//
//  AttributedLabel.swift
//  LaundryApp
//
//  Created by Waqas Ali on 2/28/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import Foundation

class AttributedLabel : BaseUILabel{
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
//        let myString = self.text
//        let myAttribute = ""
//        let myAttrString = NSAttributedString(string: myString, attributes: myAttribute)
//
//        // set attributed text on a UILabel
//        self.attributedText = myAttrString
        
        self.font = self.font.withSize(40)
    }
    
    
  
}
