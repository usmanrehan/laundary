//
//  Integer+Extensions.swift
//  LaundryApp
//
//  Created by Waqas Ali on 4/16/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import Foundation

extension Int {
    var boolValue: Bool { return self != 0 }
}
