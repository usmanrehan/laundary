//
//  SplashViewController.swift
//  LaundryApp
//
//  Created by CMS Testing on 2/27/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController, StoryBoardHandler {
    static var myStoryBoard: (forIphone: String, forIpad: String?) = (Storyboards.main.rawValue , nil)
    
    @IBOutlet weak var iconLogo: UIImageView!
    
    @IBOutlet weak var textLogo: UIImageView!
    
    @IBOutlet weak var iconLogoYconstraint: NSLayoutConstraint!
    
    @IBOutlet weak var overViewLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var textLogoWidthConstraint: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()

     self.navigationController?.isNavigationBarHidden = true
        iconLogo.isHidden = true
        textLogo.isHidden = true
     
      }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
//        self.navigationController?.setViewControllers([ServicesViewController.loadVC()], animated: true)
//        return
        self.rotate(view: self.iconLogo)
        popInView(view: iconLogo)
        self.textLogo.isHidden = false
        UIView.animate(withDuration: 2, delay: 0.7, options: .curveLinear, animations: {
            self.overViewLeadingConstraint.constant = self.view.frame.width
            self.view.layoutIfNeeded()
        }) { (v) in
           DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            self.checkLogin()
            }
        }
        
     
    }
   
    func checkLogin(){
        
        let userDefault = UserDefaults.standard
        if userDefault.bool(forKey: Login.isLoggedIn) == true {
            if let data = UserDefaults.standard.value(forKey: Login.userData) as? Data {
                CurrentUser.data = try? PropertyListDecoder().decode(User.self, from: data)
            }
            CurrentUser.token = UserDefaults.standard.value(forKey: Login.token) as! String
            
           
            self.navigationController?.setViewControllers([HomeViewController.loadVC()], animated: true)
           
        }else{
            FP.loginGuest = false; self.navigationController?.setViewControllers([WalkThroughViewController.loadVC()], animated: true)
        }
      
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func rotate(view : UIImageView) {
        let animation = CABasicAnimation(keyPath: "transform.rotation.z")
        animation.fromValue = 0
        animation.toValue = 2 * Double.pi
        animation.duration = 1 * 2
        animation.repeatCount = Float.infinity
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        view.layer.add(animation, forKey: animation.keyPath)
    }
 
    func popInView(view: UIView)  {
        
        view.isHidden = false
        view.alpha = 0.0
        
        
        
        view.transform = CGAffineTransform(scaleX: 0.001, y: 0.001);
        UIView.animate(withDuration: 0.5, delay: 0.5, options: .curveLinear, animations: {
            view.alpha = 1
            view.transform = CGAffineTransform(scaleX: 1.2, y: 1.2);
            
           
            
            
        }) { (finished) in
            
            UIView.animate(withDuration: 0.3, delay: 0.1, options: .curveLinear, animations: {
                
                view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0);
              
            }) { (finished) in
                
            }
            
        }
    }
    
}
