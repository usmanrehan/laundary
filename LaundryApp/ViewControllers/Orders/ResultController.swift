//
//  ResultController.swift
//  Telr_SDK
//
//  Created by Staff on 5/25/17.
//  Copyright © 2017 Telr. All rights reserved.
//

import UIKit
import TelrSDK

class ResultController: TelrResponseController{
    
    
    @IBOutlet var statusLabel: UILabel!

    @IBOutlet weak var btnOkay: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
       // startActivity()
        
        statusLabel.text = message!
//        if let ref = tranRef{
//            saveData(key: "ref", value: ref)
//        }
//        if let last4 = cardLast4{
//            saveData(key: "last4", value: last4)
//        }
        
        // Return payment results
       
         print(trace ?? "trace")
         print(status ?? "status")
         print(avs ?? "avs")
         print(code ?? "code")
         print(ca_valid ?? "ca_valid")
         print(cardCode ?? "cardCode")
         print(cardLast4 ?? "cardLast4")
          print(cvv ?? "cvv")
         print(tranRef ?? "tranRef")
        
        
        
 
    }
    
    
    @IBAction func okayButtonPressed(_ sender: UIButton) {
        
        if status == "A" {
       
                self.dismiss(animated: true) {
                    
                    
                    let formatter = DateFormatter()
                    formatter.dateFormat = "dd/MM/yyyy"
                    
                    CartData.params.updateValue("card" as AnyObject, forKey: "payment_type")
                    CartData.params.updateValue(self.tranRef as AnyObject, forKey: "transaction_id")
                    CartData.params.updateValue(formatter.string(from: Date()) as AnyObject, forKey: "payment_date")
                 //--ww    CartData.params.updateValue("card" as AnyObject, forKey: "payment_response")
                    self.saveOrder()
            }
        }else{
          
            self.dismiss(animated: true) {
             Navigation.currentNavigation?.popViewController(animated: true)
            }
        }
    }
    
    @IBAction func backBtnPressed(_ sender: Any) {
        
       // let viewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        
      //  self.navigationController?.pushViewController(viewController, animated: true)
        
      //  self.present(viewController, animated: true, completion: nil)
      //  self.navigationController?.popViewController(animated: true)
         //  endActivity()
        self.dismiss(animated: true) {
            print("dismissed")
            
          //   Navigation.current?.popViewController(animated: true)
        }
            
            
       

        
    }
    
    private func saveData(key:String, value:String){
        let defaults = UserDefaults.standard
        defaults.set(value, forKey: key)
    }

}
// MARK: - Networking
extension ResultController {
    
    func saveOrder() {
        
        let manager = SaveOrderManager()
        let requestParam = manager.params(parametes: CartData.params)
         manager.api(requestParam, completion: {
            
            if manager.isSuccess {
               
                CartData.SD.isInstance = false
                
                let vc = OrderSummaryViewController.loadVC()
                vc.orderData = manager.orderData
                Navigation.currentNavigation?.pushViewController(vc, animated: true)
              
            }else {
                
                print("failed")
            }
        })
    }
}
