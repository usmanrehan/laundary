//
//  ActiveOrdersViewController.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/9/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit

protocol ActiveOrdersViewControllerDelegate : class {
    func didTapOnOrder(indexNo : Int) -> Void
}

class ActiveOrdersViewController: UIViewController {
  
    @IBOutlet weak var tblView: UITableView!
    weak static var delegateOrder : ActiveOrdersViewControllerDelegate?
    let cellReuseIdentifier = "MyOrderTableViewCell"
      var manager = ActiveOrderManager()
     var managerOrderCancel = OrderCancelManager()
     var activeOrdersHolder : [OLData] = []
    
    lazy var noRecordView : NoRecordFoundView = {
        let view = NoRecordFoundView.init(frame: tblView.bounds)
        return view
    }()
    lazy var refreshControl = CustomReferesh()
    
    var limit = 10
    var offset = 1
    var pageNo = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setUpTable()
        getActiveOrders(isSpinnerNeeded: true, offset: offset , limit: limit)
      
 }

    override func viewDidAppear(_ animated: Bool) {
  
        CustomReferesh.imgLogo.layer.removeAllAnimations()
        CustomReferesh.imgLogo.rotate()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        refreshControl.endRefreshing()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func setUpTable() {
        
        tblView.delegate = self
        tblView.dataSource = self
        
        tblView.register(UINib.init(nibName: cellReuseIdentifier, bundle: nil), forCellReuseIdentifier: cellReuseIdentifier)
        
        tblView.estimatedRowHeight =  DesignUtility.getValueFromRatio(200)
        tblView.backgroundView = noRecordView
        tblView.backgroundView?.isHidden = true
        tblView.isHidden = true
        tblView.addSubview(refreshControl)
        refreshControl.addTarget(self, action:
            #selector(handleRefresh),
                                 for: UIControlEvents.valueChanged)
        CustomReferesh.addCustomLoader(refereshControl: refreshControl)
        tblView.tableFooterView = nil //CustomReferesh.getCustomLoaderToFooter()
        
    }
    
    @objc func handleRefresh()  {
        print("handleRefresh")
        if !CustomReferesh.imgLogo.isAnimating{
          CustomReferesh.imgLogo.rotate()
        }
        if refreshControl.isRefreshing{
           // CustomReferesh.imgLogo.rotate()
            tblView.isUserInteractionEnabled = false
        }
        
    }

}
extension ActiveOrdersViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        tblView.backgroundView?.isHidden = manager.activeOrders.count > 0
        return manager.activeOrders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! MyOrderTableViewCell
        cell.configCell(data: manager.activeOrders[indexPath.row])
        cell.selectionStyle = .none
        return cell
        
    }
    
      func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return DesignUtility.getValueFromRatio(150)
        }

    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        checkForMoreRecords(indexNo: indexPath.row)
}
    
    
    func checkForMoreRecords(indexNo : Int) {
        
     
        if indexNo == (manager.activeOrders.count) - 1 {
            if pageNo <= manager.totalPages {
                if  !CustomReferesh.imgLogoFooter.isAnimating{
                    CustomReferesh.imgLogoFooter.rotate()
                }
                   print("indexNo" , indexNo)
                print("pageNo" , pageNo)
               offset = pageNo //--ww (pageNo * limit) - 1
                getActiveOrders(isSpinnerNeeded: false, offset: offset, limit: limit)
            }else{
                tblView.tableFooterView = nil
            }
        }
        
    }
    
}

extension ActiveOrdersViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       let id = manager.activeOrders[indexPath.row].id
            ActiveOrdersViewController.delegateOrder?.didTapOnOrder(indexNo: id )
        
        }
    
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return GlobalStatic.getLocalizedString("cancel")
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        let currentOrder = self.manager.activeOrders[indexPath.row]
        if currentOrder.orderStatus == 0 {
            return true
        }
         return false
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            
            Alert.showWithTwoActions(msg: GlobalStatic.getLocalizedString("cancel_order_confirm"), okBtnTitle: GlobalStatic.getLocalizedString("yes"), okBtnAction: {
                self.cancelOrder(indexNo: indexPath.row)
            }, cancelBtnTitle: GlobalStatic.getLocalizedString("no"), cancelBtnAction: {
              self.tblView.reloadRows(at: [indexPath], with: .automatic)
            })
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if refreshControl.isRefreshing {
            print("refreshControl.isRefreshing")
            tblView.isUserInteractionEnabled = false
            activeOrdersHolder = self.manager.activeOrders
            self.manager.activeOrders = []
            pageNo = 1
            offset = 1
           //--ww limit = 10
            tblView.tableFooterView = nil //CustomReferesh.getCustomLoaderToFooter()
          getActiveOrders(isSpinnerNeeded: false, offset: offset , limit: limit)
        }
    }
}
// MARK: - Networking
extension ActiveOrdersViewController {
    
    func getActiveOrders(isSpinnerNeeded : Bool , offset : Int , limit : Int) {
        
//        if let arr = self.manager.activeOrders , arr.count > 0 {
//            return
//        }
        
        UIApplication.shared.beginIgnoringInteractionEvents()
        let requestParam = self.manager.params(status: "active" , offset: offset , limit: limit)
        
        self.manager.apiCall(requestParam, showSpinner: isSpinnerNeeded, completion: {
            
            self.refreshControl.endRefreshing()
             self.tblView.isHidden = false
            if self.manager.isSuccess {
                print("totalPages" , self.manager.totalPages)
                self.pageNo += 1
                if self.manager.activeOrders.count >  limit - 1{
                    self.tblView.tableFooterView = CustomReferesh.getCustomLoaderToFooter()
                }else{
                    self.tblView.tableFooterView = nil //CustomReferesh.getCustomLoaderToFooter()
                }
                
                 self.tblView.isUserInteractionEnabled = true
                UIApplication.shared.endIgnoringInteractionEvents()
              self.tblView.reloadData()
            }else {
                 self.tblView.isUserInteractionEnabled = true
                UIApplication.shared.endIgnoringInteractionEvents()
                print("failed")
                
                self.manager.activeOrders =  self.activeOrdersHolder
            }
        })
    }
    
    
    
    
    func cancelOrder(indexNo : Int) {
        
        let orderData =  self.manager.activeOrders[indexNo]
        
        let requestParam = self.managerOrderCancel.paramsCancelOrder(orderId: orderData.id, orderStatus: orderData.orderStatus)
        
        self.managerOrderCancel.api(requestParam, completion: {
 
            if self.managerOrderCancel.isSuccess {
                 self.tblView.isUserInteractionEnabled = true
                
              //  print(self.managerOrderCancel.orderData)
              //   orderData.orderStatus = 7
              //  self.manager.activeOrders![indexNo] =  orderData
                
                self.manager.activeOrders.remove(at: indexNo)
                self.tblView.deleteRows(at: [IndexPath.init(row: indexNo, section: 0)], with: .automatic)
                self.tblView.reloadData()
            }else {
                
                print("failed")
            }
        })
    }
}



