//
//  OrderDetailViewController.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/9/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit

class OrderDetailViewController: BaseViewController  , StoryBoardHandler {
    static var myStoryBoard: (forIphone: String, forIpad: String?) = (Storyboards.orders.rawValue , nil)
    
    
    var orderID : Int = 0
    
    @IBOutlet weak var tblView: UITableView!
    let topCellReuseIdentifier = "OrderDetailTopTableViewCell"
    let dateTimeCellReuseIdentifier = "OrderDetailDateTimeTableViewCell"
    let cartCellReuseIdentifier = "CartViewContainerTableViewCell"
    let paymentCellReuseIdentifier = "OrderDetailPaymentTableViewCell"
    let footerCellReuseIdentifier = "OrderDetailFooterTableViewCell"
    
 
    
  var sectionArray = [
    GlobalStatic.getLocalizedString("customer_details") ,
    GlobalStatic.getLocalizedString("pickup_details") ,
    GlobalStatic.getLocalizedString("delivery_details"),
    GlobalStatic.getLocalizedString("item_list"),
    GlobalStatic.getLocalizedString("method_payment") ,
    GlobalStatic.getLocalizedString("total_price")
    ]
    
    var dataArray = [[(title: String, imgName: String )]] ()
      var dataArrayCart = [MyCartModel]()
    let sectionHeaderHeight = DesignUtility.getValueFromRatio(35)
    
    var manager  = OrderDetailManager()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = GlobalStatic.getLocalizedString("order_detail")
       
        getDetails(orderId: orderID)
    }

    func setupTable(){
        
        tblView.delegate = self
        tblView.dataSource = self
        
        tblView.register(UINib.init(nibName: topCellReuseIdentifier, bundle: nil), forCellReuseIdentifier: topCellReuseIdentifier)
        tblView.register(UINib.init(nibName: dateTimeCellReuseIdentifier, bundle: nil), forCellReuseIdentifier: dateTimeCellReuseIdentifier)
        tblView.register(UINib.init(nibName: cartCellReuseIdentifier, bundle: nil), forCellReuseIdentifier: cartCellReuseIdentifier)
        tblView.register(UINib.init(nibName: paymentCellReuseIdentifier, bundle: nil), forCellReuseIdentifier: paymentCellReuseIdentifier)
        tblView.register(UINib.init(nibName: footerCellReuseIdentifier, bundle: nil), forCellReuseIdentifier: footerCellReuseIdentifier)
        tblView.estimatedRowHeight = DesignUtility.getValueFromRatio(200)  //160
}
    
    func setupModel(){
        dataArrayCart = [
            MyCartModel(title: "Dry Clean" , imgName : "wash-fold-cart" , dArray : [
                (item : "2 x Shirt" , cost: "AED 15"),
                (item : "3 x T-shirts" , cost: "AED 23"),
                (item : "1 x Jeans" , cost: "AED 6"),
                
                ]),
            
            MyCartModel(title: "Steam Ironing" , imgName : "iron-cart" , dArray : [
                (item : "2 x Shirt" , cost: "AED 15"),
                (item : "3 x T-shirts" , cost: "AED 23"),
                (item : "1 x Jeans" , cost: "AED 6"),
                (item : "1 x Jeans" , cost: "AED 6"),
                (item : "1 x Jeans" , cost: "AED 6")
                ]),
            
            MyCartModel(title: "Wash & Iron" , imgName : "wash-iron-cart" , dArray : [
                (item : "2 x Shirt" , cost: "AED 15"),
                (item : "3 x T-shirts" , cost: "AED 23"),
                (item : "1 x Jeans" , cost: "AED 6")
                ]),
        ]
        
        tblView.reloadData()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
extension OrderDetailViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return  6 // sectionArray.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 3 {
            return 3
        }
        return  1// dataArray[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell  = tableView.dequeueReusableCell(withIdentifier: topCellReuseIdentifier) as! OrderDetailTopTableViewCell
            cell.configCell(data: (manager.orderData?.orderuser)! , orderStatus:(manager.orderData?.orderStatus)!)
            cell.selectionStyle = .none
            cell.trackingAction = { [weak self] in
                
              let vc = OrderTrackingViewController.loadVC()
                
                var status = 0
                switch (self?.manager.orderData?.orderStatus ?? 0) {
                case 7:
                   status = 3
                    
                case 3,4 :
                    status = (self?.manager.orderData?.orderStatus)! + 1
                default:
                    status = (self?.manager.orderData?.orderStatus)!
                }
                
                
                
                vc.stepNo =  status // self?.manager.orderData?.orderStatus ?? 0
                
                self?.show(viewcontrollerInstance: vc)
            }
            return cell
        }else if indexPath.section < 3 {
            
            let cell  = tableView.dequeueReusableCell(withIdentifier: dateTimeCellReuseIdentifier) as! OrderDetailDateTimeTableViewCell
            cell.configCell(data: manager.orderData!, sec: indexPath.section)
            cell.selectionStyle = .none
            return cell
            
        }else if indexPath.section == 3 {
            
            let cell  = tableView.dequeueReusableCell(withIdentifier: cartCellReuseIdentifier) as! CartViewContainerTableViewCell
            cell.selectionStyle = .none
            if manager.mainArray[indexPath.row].count > 0 {
       cell.configCellFromOrderDetail(data: manager.mainArray[indexPath.row], indexNo: indexPath.row, from: .fromDetail)
            
             cell.setNeedsLayout()
            }
            if indexPath.row == 2 {
                cell.viewLineBottom.isHidden = true
            }else{
                 cell.viewLineBottom.isHidden = false
            }
            return cell
            
        } else if indexPath.section == 4 {
            
            let cell  = tableView.dequeueReusableCell(withIdentifier: paymentCellReuseIdentifier) as! OrderDetailPaymentTableViewCell
             let payType = manager.orderData?.paymentType! == "cash" ? GlobalStatic.getLocalizedString("by_cash_small") : manager.orderData?.paymentType!
            cell.lblPaymentType.text = payType
            cell.selectionStyle = .none
        
            return cell
        }
        else{
            
            let cell  = tableView.dequeueReusableCell(withIdentifier: footerCellReuseIdentifier) as! OrderDetailFooterTableViewCell
            
            cell.configCell(data: manager.orderData!)
            cell.selectionStyle = .none
            
            return cell
        }
    }
    
    
//    
//    @objc func slideCell(cell : UITableViewCell)  {
//        Animations.slideView(view: cell)
//    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    }
    
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
       
        return sectionHeaderHeight
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
           let  orderStatus = manager.orderData?.orderStatus ?? 0
            
            if orderStatus > 4 && orderStatus != 7 {
                return DesignUtility.getValueFromRatio(100)
            }else{
              return DesignUtility.getValueFromRatio(170)
            }
            
            
        } else if indexPath.section == 3 {
            if manager.mainArray[indexPath.row].count < 1 {
                return 0
            }else{
                 return UITableViewAutomaticDimension
                
            }
            
        }
        else if indexPath.section  < 6 {
            
            return UITableViewAutomaticDimension //DesignUtility.getValueFromRatio(160)
        }
        return DesignUtility.getValueFromRatio(65)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: sectionHeaderHeight))
        view.backgroundColor = UIColor.groupTableViewBackground
        
        let label = BaseUILabel(frame: CGRect(x: DesignUtility.getValueFromRatio(25), y: 0, width: tableView.bounds.width - DesignUtility.getValueFromRatio(60), height: sectionHeaderHeight))
        
        label.fontNameTheme = "fontDefault"
        label.fontColorTheme = "darkGray"
        label.fontSizeTheme = "sizeSmall"
        label.text = sectionArray[section]
        label.tag = -1
        view.addSubview(label)
        return view
    }
}

extension OrderDetailViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     
    }
}
// MARK: - Networking
extension OrderDetailViewController {
    
    func getDetails(orderId : Int) {
        
        let requestParam = self.manager.params(orderID: orderId)
        
        self.manager.api(requestParam, completion: {
            
            if self.manager.isSuccess {
                
                print(self.manager.orderData)
                if self.manager.orderData != nil {
                self.setupTable()
                self.setupModel()
                }
            }else {
                print("failed")
            }
        })
    }
}


