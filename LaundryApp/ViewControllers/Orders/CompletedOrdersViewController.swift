//
//  CompletedOrdersViewController.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/9/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit

protocol CompletedOrdersViewControllerDelegate : class {
    func didTapOnCompletedOrder(indexNo : Int) -> Void
}

class CompletedOrdersViewController: UIViewController {
    
    
   
    @IBOutlet weak var tblView: UITableView!
    
    weak static var delegateOrder : CompletedOrdersViewControllerDelegate?
    let cellReuseIdentifier = "MyOrderTableViewCell"
    
    var manager = CompletedOrderManager()
    var completedOrdersHolder : [OLData] = []
    
    lazy var noRecordView : NoRecordFoundView = {
        let view = NoRecordFoundView.init(frame: tblView.bounds)
        return view
    }()
    
  //  var noRecordView : NoRecordFoundView!
    
    lazy var refreshControl = CustomReferesh()
    
    var limit = 10
    var offset = 1
    var pageNo = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
           NotificationCenter.default.addObserver(self, selector: #selector(getDatafromServer(notfication:)), name: .getCompletedOrder, object: nil)
 
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        print("CompletedOrdersViewController")
      
           self.setUpTable()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(animated)
     
    }
    
    override func viewDidDisappear(_ animated: Bool) {
    refreshControl.endRefreshing()
    }
    
    deinit {
        
           NotificationCenter.default.removeObserver(self, name: .getCompletedOrder, object: nil)
    }
    
    func printMyName()  {
        print("me name is CompletedOrdersViewController")
    }
    
    @objc func getDatafromServer(notfication: NSNotification) {
        if self.manager.completedOrders.count < 1  {
        getCompletedOrders(isSpinnerNeeded: true, offset: 1, limit: 10)
        }
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUpTable() {
        
        tblView.delegate = self
        tblView.dataSource = self
        
        tblView.register(UINib.init(nibName: cellReuseIdentifier, bundle: nil), forCellReuseIdentifier: cellReuseIdentifier)
        
        tblView.estimatedRowHeight =  DesignUtility.getValueFromRatio(200)
   
         tblView.backgroundView = noRecordView
         tblView.backgroundView?.isHidden = true
         tblView.isHidden = true
        tblView.addSubview(refreshControl)
        refreshControl.addTarget(self, action:
            #selector(handleRefresh),
                                 for: UIControlEvents.valueChanged)
    CustomReferesh.addCustomLoaderForOrders(refereshControl: refreshControl)
        tblView.tableFooterView = nil //CustomReferesh.getCustomLoaderToFooter()
    }
    
    @objc func handleRefresh()  {
        print("handleRefresh")
        if !CustomReferesh.imgLogoTwo.isAnimating{
            CustomReferesh.imgLogoTwo.rotate()
        }
        if refreshControl.isRefreshing{
            // CustomReferesh.imgLogo.rotate()
            tblView.isUserInteractionEnabled = false
        }
        
    }
    
}
extension CompletedOrdersViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
     
        tblView.backgroundView?.isHidden = manager.completedOrders.count > 0
        return manager.completedOrders.count
    
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! MyOrderTableViewCell
         cell.configCell(data: manager.completedOrders[indexPath.row])
        cell.selectionStyle = .none
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return DesignUtility.getValueFromRatio(150)
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        checkForMoreRecords(indexNo: indexPath.row)
    }
    
    
    func checkForMoreRecords(indexNo : Int) {
        
        
        if indexNo == (manager.completedOrders.count - 1) {
            if pageNo <= manager.totalPages {
                if  !CustomReferesh.imgLogoFooter.isAnimating{
                    CustomReferesh.imgLogoFooter.rotate()
                }
                print("indexNo" , indexNo)
                print("pageNo" , pageNo)
                offset = pageNo //--ww (pageNo * limit) - 1
                getCompletedOrders(isSpinnerNeeded: false, offset: offset, limit: limit)
            }else{
                tblView.tableFooterView = nil
            }
        }
        
    }
    
    
}

extension CompletedOrdersViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        let id = manager.completedOrders[indexPath.row].id
        CompletedOrdersViewController.delegateOrder?.didTapOnCompletedOrder(indexNo: id)
        
    }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if refreshControl.isRefreshing {
            print("refreshControl.isRefreshing")
            tblView.isUserInteractionEnabled = false
            completedOrdersHolder = self.manager.completedOrders
            self.manager.completedOrders = []
            pageNo = 1
            offset = 1
            //--ww limit = 10
            tblView.tableFooterView = nil //CustomReferesh.getCustomLoaderToFooter()
            getCompletedOrders(isSpinnerNeeded: false, offset: offset , limit: limit)
        }
    }
}

// MARK: - Networking
extension CompletedOrdersViewController {
    
    func getCompletedOrders(isSpinnerNeeded : Bool , offset : Int , limit : Int) {
        
//
//        if self.manager.completedOrders.count > 0 {
//            return
//        }
        UIApplication.shared.beginIgnoringInteractionEvents()
        let requestParam = self.manager.params(status: "completed")
        
         self.manager.apiCall(requestParam, showSpinner: isSpinnerNeeded, completion: {
            
            self.refreshControl.endRefreshing()
            self.tblView.isHidden = false
            
            if self.manager.isSuccess {
                print("totalPages" , self.manager.totalPages)
                self.pageNo += 1
                if self.manager.completedOrders.count >  limit - 1{
                    self.tblView.tableFooterView = CustomReferesh.getCustomLoaderToFooter()
                }else{
                    self.tblView.tableFooterView = nil //CustomReferesh.getCustomLoaderToFooter()
                }
                
                self.tblView.isUserInteractionEnabled = true
                UIApplication.shared.endIgnoringInteractionEvents()
                self.tblView.reloadData()
            }else {
                UIApplication.shared.endIgnoringInteractionEvents()
                self.tblView.isUserInteractionEnabled = true
                self.manager.completedOrders =  self.completedOrdersHolder
                print("failed")
            }
        })
    }
}



extension Notification.Name {
    static let getCompletedOrder = Notification.Name("getCompletedOrder")
}

