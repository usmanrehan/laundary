//
//  OrderSummaryViewController.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/8/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit
protocol OrderSummaryProtocol  :class {
    func deleteCart() -> Void
}

class OrderSummaryViewController: BaseViewController , StoryBoardHandler {
    static var myStoryBoard: (forIphone: String, forIpad: String?) = (Storyboards.orders.rawValue , nil)
    
    
    @IBOutlet weak var tblOrder: UITableView!
    let cellReuseIdentifier = "OrderSummaryHeaderTableViewCell"
    let topCellReuseIdentifier = "OrderTopTableViewCell"
    let dataCellReuseIdentifier = "OrderSummaryTableViewCell"
    let footerCellReuseIdentifier = "OrderFooterTableViewCell"
    
    var dataArray = [(String, String)]()
    var orderData : SOOrder!
    weak static var delegateOrderSummary : OrderSummaryProtocol?

    override func viewDidLoad() {
        super.viewDidLoad()
    self.title = GlobalStatic.getLocalizedString("thank_you")
        setUpTable()
        setupModel()
       GlobalStatic.deleteCartData()
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func setUpTable() {
        
        tblOrder.delegate = self
        tblOrder.dataSource = self
        
        tblOrder.register(UINib.init(nibName: cellReuseIdentifier, bundle: nil), forCellReuseIdentifier: cellReuseIdentifier)
        
          tblOrder.register(UINib.init(nibName: topCellReuseIdentifier, bundle: nil), forCellReuseIdentifier: topCellReuseIdentifier)
        
         tblOrder.register(UINib.init(nibName: dataCellReuseIdentifier, bundle: nil), forCellReuseIdentifier: dataCellReuseIdentifier)
        
         tblOrder.register(UINib.init(nibName: footerCellReuseIdentifier, bundle: nil), forCellReuseIdentifier: footerCellReuseIdentifier)
        
        
        
        tblOrder.estimatedRowHeight =  DesignUtility.getValueFromRatio(70)
        
    }
    
    func setupModel(){
        
        
        let payType = orderData.paymentType! == "cash" ? GlobalStatic.getLocalizedString("by_cash_small") : orderData.paymentType!
        dataArray = [
            (GlobalStatic.getLocalizedString("provide_instruction").uppercased(), getProvideInstructionString()),
            (GlobalStatic.getLocalizedString("address").uppercased(), orderData.pickupLocation!),
            (GlobalStatic.getLocalizedString("payment_methods").uppercased(), payType),
            (GlobalStatic.getLocalizedString("add_pickup"), orderData.pickupLocation!),
            (GlobalStatic.getLocalizedString("add_delivery"),orderData.dropLocation!)
        ]
    }
    
    func getProvideInstructionString() -> String {
        
        let instruction = orderData.orderInstructions!
        var message = ""
        if instruction.isFold == 1 {
            message = GlobalStatic.getLocalizedString("fold_cloths") + "\n"
        }
        if instruction.atMyDoor == 1 {
         message = message + GlobalStatic.getLocalizedString("at_my_door") + "\n"
        }
        if instruction.callMeBeforePickup == 1 {
            message = message + GlobalStatic.getLocalizedString("call_before_pickup") + "\n"
        }
        
        if instruction.callMeBeforeDelivery == 1 {
            message = message + GlobalStatic.getLocalizedString("call_before_delivery") + "\n"
        }
        message = message + instruction.other!
        
        return message
    }
    
    
    override func goBack() {
        CartData.cartDict = [:]
        CartData.totalQuantity = 0
        CartData.totalAmount = 0
        CartData.params = [:]
        
        self.pushOrPopViewController(navigationController: (self.navigationController)!, animation: true, viewControllerClass: HomeViewController.self, viewControllerStoryboad: (Storyboards.home.rawValue, nil))
    }
}

extension OrderSummaryViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count + 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
        let cell  = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! OrderSummaryHeaderTableViewCell
       
        cell.selectionStyle = .none
            cell.historyAction = { [weak self] in
                
                let vc = OrderDetailViewController.loadVC()
                vc.orderID  = (self?.orderData.id!)!
                
                self?.show(viewcontrollerInstance: vc)
            }
            
            cell.continueLaundryAction = { [weak self] in
                
                 //--ww CartData.cartDict = [:]
                OrderSummaryViewController.delegateOrderSummary?.deleteCart()
                self?.pushOrPopViewController(navigationController: (self?.navigationController)!, animation: true, viewControllerClass: ServicesViewController.self, viewControllerStoryboad: (Storyboards.services.rawValue, nil))
            }
            
        return cell
        }else if indexPath.row == 1 {
            
            let cell  = tableView.dequeueReusableCell(withIdentifier: topCellReuseIdentifier) as! OrderTopTableViewCell
            cell.configCell(data: orderData)
            cell.selectionStyle = .none
            return cell
            
        }else if indexPath.row > 1 && indexPath.row < 7{
            
            let cell  = tableView.dequeueReusableCell(withIdentifier: dataCellReuseIdentifier) as! OrderSummaryTableViewCell
            
            cell.configCell(data: dataArray[indexPath.row - 2])
            cell.selectionStyle = .none
            return cell
            
           
        }else{
            
            let cell  = tableView.dequeueReusableCell(withIdentifier: footerCellReuseIdentifier) as! OrderFooterTableViewCell
            cell.configCell(data: orderData)
            cell.selectionStyle = .none
            return cell
           }
}
    
    
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    
            if indexPath.row  == 0  {
                return DesignUtility.getValueFromRatio(310)
            }else if indexPath.row == 1 {
                
                return DesignUtility.getValueFromRatio(180)
            }else if indexPath.row == 7 {
                
                 return  UITableViewAutomaticDimension //DesignUtility.getValueFromRatio(180)
            }
    
            return UITableViewAutomaticDimension
        }
    
}

extension OrderSummaryViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}


