//
//  MyOrdersViewController.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/9/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit



class MyOrdersViewController: BaseViewController , StoryBoardHandler {
    static var myStoryBoard: (forIphone: String, forIpad: String?) = (Storyboards.orders.rawValue , nil)
    
   var currentPage = 0
   
    @IBOutlet weak var swiftPagesView: SwiftPages!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = GlobalStatic.getLocalizedString("my_order_list")
        setupTabs()
    }

    
    override func viewDidAppear(_ animated: Bool) {
     // let vc  = swiftPagesView.viewControllerForPage(page: currentPage)
      //  vc.viewDidAppear(animated)
        CustomReferesh.imgLogo.rotate()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func setupTabs() {
      //  swiftPagesView = SwiftPages(frame: CGRect(x :0, y :70, width : self.view.frame.width, height: self.view.frame.height))
        // Do any additional setup after loading the view.
        let VCIDs : [String] = ["ActiveOrdersViewController", "CompletedOrdersViewController"]
        let buttonTitles : [String] = [GlobalStatic.getLocalizedString("active_orders"), GlobalStatic.getLocalizedString("completed_orders")]
        
        swiftPagesView.initializeWithVCIDsArrayAndButtonTitlesArray(VCIDsArray: VCIDs, buttonTitlesArray: buttonTitles , storyBoardName : Storyboards.orders.rawValue)
        
       //  swiftPagesView.enableAeroEffectInTopBar(boolValue: true)
         swiftPagesView.setButtonsTextColor(color: UIColor.white)
      //  swiftPagesView.setAnimatedBarColor(color: UIColor.white)
        //swiftPagesView.initializeWithVCIDsArrayAndButtonImagesArray([], buttonImagesArray: buttonImages)
        swiftPagesView.setTopBarBackground(color: UIColor(red: 182/255, green: 14/255, blue: 37/255, alpha: 1.0))
        swiftPagesView.setAnimatedBarColor(color: UIColor.white)
        swiftPagesView.delegate = self
    
        
        ActiveOrdersViewController.delegateOrder = self
        CompletedOrdersViewController.delegateOrder = self
      //  self.view.addSubview(swiftPagesView)
    }
    
    deinit {
        print("MyOrders Deallaocted")
    }

}
extension MyOrdersViewController : SwiftPagesDelegate{
    
    func didEndDeceleratingPageNumber(currentIndex: Int) {
         print("didEndDeceleratingPageNumber" ,currentIndex)
        switch currentIndex {
        case 0:
            if swiftPagesView.viewControllerForPage(page: currentIndex) is ActiveOrdersViewController{
                }
            case 1:
//            if let vc = swiftPagesView.viewControllerForPage(page: currentIndex) as? CompletedOrdersViewController{
//                vc.getCompletedOrders(isSpinnerNeeded: true, offset: 1, limit: 10)
//            }
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
                     NotificationCenter.default.post(name: .getCompletedOrder, object: nil)
                })
        default:
            break
        }
        
        currentPage = currentIndex
    }
    func SwiftPagesCurrentPageNumber(currentIndex: Int) {
         print("SwiftPagesCurrentPageNumber" ,currentIndex)
    }
    
}
extension MyOrdersViewController : ActiveOrdersViewControllerDelegate {
    
  func didTapOnOrder(indexNo: Int) {
        let vc = OrderDetailViewController.loadVC()
       vc.orderID = indexNo
        show(viewcontrollerInstance: vc)
    }
}

extension MyOrdersViewController : CompletedOrdersViewControllerDelegate {
    
    func didTapOnCompletedOrder(indexNo: Int) {
        let vc = OrderDetailViewController.loadVC()
        vc.orderID = indexNo
        show(viewcontrollerInstance: vc)
    }
}
