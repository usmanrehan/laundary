//
//  OrderTrackingViewController.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/9/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit

class OrderTrackingViewController: BaseViewController, StoryBoardHandler {
    static var myStoryBoard: (forIphone: String, forIpad: String?) = (Storyboards.orders.rawValue , nil)
    
    
  
    @IBOutlet weak var steppedPregressBar: ABSteppedProgressBar!
    @IBOutlet weak var viewContainerOrderTracking: UIView!
    @IBOutlet weak var imgRedCircleOuter: UIImageView!
    @IBOutlet weak var imgRedCircleInner: UIImageView!
    @IBOutlet weak var imgCircleWhite: UIImageView!
    @IBOutlet weak var imgIconTruck: UIImageView!
    @IBOutlet weak var lblProcess: BaseUILabel!
    var indexNo = 0
    var animationTimer: Timer!
    var dataArray = [(String , String)]()
    let animationTimeInterval  = 0.5
    var stepNo = 0
    var perStepValue = 4
    
    override func viewDidLoad() {
        super.viewDidLoad()

       self.title = GlobalStatic.getLocalizedString("order_status_tracking")
        if stepNo > 5 {
            stepNo = 5
        }
  
         setupModel()
         lblProcess.text = dataArray[indexNo].0.uppercased()
        setupProgressBar()
        DispatchQueue.main.async { // Dispatch in order to render the subtitle in the next runloop
            self._addSubtitles()
        }
       
      
}
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        showProgressCircle(count : (stepNo + 1) * perStepValue )
        steppedPregressBar.currentIndex = stepNo
        indexNo += 1
      animationTimer = Timer.scheduledTimer(timeInterval: animationTimeInterval, target: self, selector: #selector(timerProcess), userInfo: nil, repeats: true)
    }
    
    @objc func timerProcess()  {
    
        if indexNo <= stepNo {
             lblProcess.text = dataArray[indexNo].0.uppercased()
           // lblProcess.alpha = 0
//            UIView.transition(with: imgIconTruck, duration: 0.2, options: .curveEaseIn, animations: {
//
//                self.imgIconTruck.image = UIImage.init(named: self.dataArray[self.indexNo].1)
//               // self.lblProcess.alpha = 1
//            }) { (v) in
//                // self.popInView(view: self)
//
//            }
            
          
                UIView.transition(with: self.imgIconTruck,
                                  duration: 0.5,
                                  options: .curveEaseInOut,
                                  animations: { self.imgIconTruck.image = UIImage.init(named: self.dataArray[self.indexNo].1) },
                                  completion: nil)
         
          
            
           
            
        }else{
           animationTimer.invalidate()
        }
        
         indexNo += 1
    }
    
    func setupProgressBar() {
        // Customise the progress bar here
        steppedPregressBar.numberOfPoints = 6
        steppedPregressBar.lineHeight = 3
        steppedPregressBar.radius = 7
        steppedPregressBar.progressRadius = 7
        steppedPregressBar.progressLineHeight = 3
        
       steppedPregressBar.currentIndex = 0
        steppedPregressBar.delegate = self
        steppedPregressBar.stepAnimationDuration = animationTimeInterval
        
        steppedPregressBar.stepTextColor = UIColor.white
       steppedPregressBar.stepTextFont = UIFont.init(name: "Poppins-Light", size: 20)
      //--ww  UIFont.systemFont(ofSize: 20)
        
        steppedPregressBar.backgroundShapeColor = UIColor(red: 231/255, green: 231/255, blue: 231/255, alpha: 1)
       steppedPregressBar.selectedBackgoundColor =  UIColor.init(hexString: "#B81F24")
    }
    
    func setupModel() {
        
       dataArray = [
             (GlobalStatic.getLocalizedString("order_placed"),"step1"),
             (GlobalStatic.getLocalizedString("order_pending"),"step2"),
             (GlobalStatic.getLocalizedString("pickup_process"),"step3"),
             (GlobalStatic.getLocalizedString("laundry_process"),"step4"),
            (GlobalStatic.getLocalizedString("delivery_process"),"step5"),
             (GlobalStatic.getLocalizedString("order_complete"),"step6")
            ]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    private func _addSubtitles() {
        let subtitleVerticalPosition: CGFloat = steppedPregressBar.frame.origin.y + steppedPregressBar.bounds.height + 0
        for (idx, point) in steppedPregressBar.centerPoints.enumerated() {
            let realPoint = steppedPregressBar.convert(point, to: self.view)
            let subtitle = UILabel(frame: CGRect(x: 0, y: subtitleVerticalPosition, width: 55, height: 30))
            subtitle.textAlignment = .center
            subtitle.center.x = realPoint.x
            subtitle.numberOfLines = 2
            subtitle.font = UIFont.systemFont(ofSize: 10)
            subtitle.textColor = UIColor.darkGray
            subtitle.text = self.progressBar(steppedPregressBar, textAtIndex: idx)
            
            
            self.view.addSubview(subtitle)
        }
    }

}
extension OrderTrackingViewController: ABSteppedProgressBarDelegate {
    
    func progressBar(_ progressBar: ABSteppedProgressBar,
                     willSelectItemAtIndex index: Int) {
        print("progressBar:willSelectItemAtIndex:\(index)")
        
    }
    
    func progressBar(_ progressBar: ABSteppedProgressBar,
                     didSelectItemAtIndex index: Int) {
        print("progressBar:didSelectItemAtIndex:\(index)")
        
    }
    
    func progressBar(_ progressBar: ABSteppedProgressBar,
                     canSelectItemAtIndex index: Int) -> Bool {
        print("progressBar:canSelectItemAtIndex:\(index)")
        //Only next (or previous) step can be selected
        let offset = abs(progressBar.currentIndex - index)
        return (offset <= 1)
    }
    
    func progressBar(_ progressBar: ABSteppedProgressBar,
                     textAtIndex index: Int) -> String {
        
        return dataArray[index].0
//        let text: String
//        switch index {
//        case 0:
//            text = dataArray[0
//        case 1:
//            text = "Order Pending"
//        case 2:
//            text = "Pickup Process"
//        case 3:
//            text = "Delivery Process"
//        case 4:
//            text = "Order Complete"
//        default:
//            text = ""
//        }
//    //--ww    print("progressBar:textAtIndex:\(index)")
//        return text
    }
    
}

extension OrderTrackingViewController {
    
    func showProgressCircle(count : Int) {
        
        let dFactor = DesignUtility.getValueFromRatio(1.97)
        let fFactor = DesignUtility.getValueFromRatio(2.1)
        
        let centerX = DesignUtility.getValueFromRatio(viewContainerOrderTracking.frame.width / dFactor)
        let centerY = DesignUtility.getValueFromRatio(viewContainerOrderTracking.frame.height / fFactor)
        
        let arrayPoint =  drawCirclePoints(points: 24, radius: DesignUtility.getValueFromRatio(115) , center:CGPoint(x: centerX, y: centerY) )
        
        let dimension = DesignUtility.getValueFromRatio(18)
    
        for i in 0..<arrayPoint.count {
        
            var index = 20 + i  //--ww  18 + i
            
            if index >= 24  //--ww 24
            {
                index = i - 4  //--ww 6
                
            }
            
        let point = arrayPoint[index]
        //let xPos = startPos +  (CGFloat(i) * xFactor)
      // let yPos = 10.0 +  (CGFloat(i) * yFactor)
        let imgCircle = UIImageView.init(image: UIImage.init(named: "circle-progress"))
      imgCircle.frame = CGRect(x: 0  , y : 0 , width :dimension , height : dimension)
            
             // imgCircle.center = point
            if i >= 0 && i  < 24 //--ww 20
            {
                imgCircle.center = CGPoint(x :point.x - DesignUtility.getValueFromRatio(5) , y : point.y )
            }else {
                imgCircle.center = point
            }
//
             // imgCircle.center = CGPoint(x :point.x - 4 , y : point.y )
            imgCircle.isHidden = true
        viewContainerOrderTracking.addSubview(imgCircle)
        viewContainerOrderTracking.bringSubview(toFront: imgCircle)
            
            let multi =  Double(stepNo) * 0.5 / Double(count)
            
            imgCircle.popInFast(delay: CGFloat(i) , multiplier: CGFloat(multi))
            
            if i == count - 1 {
                break
            }
           
        }
    }
    
}

extension CGPath {
    
    func forEach( body: @convention(block) (CGPathElement) -> Void) {
        typealias Body = @convention(block) (CGPathElement) -> Void
        let callback: @convention(c) (UnsafeMutableRawPointer, UnsafePointer<CGPathElement>) -> Void = { (info, element) in
            let body = unsafeBitCast(info, to: Body.self)
            body(element.pointee)
        }
        print(MemoryLayout.size(ofValue: body))
        let unsafeBody = unsafeBitCast(body, to: UnsafeMutableRawPointer.self)
        self.apply(info: unsafeBody, function: unsafeBitCast(callback, to: CGPathApplierFunction.self))
    }
    
    
    func getPathElementsPoints() -> [CGPoint] {
        var arrayPoints : [CGPoint]! = [CGPoint]()
        self.forEach { element in
            switch (element.type) {
            case CGPathElementType.moveToPoint:
                arrayPoints.append(element.points[0])
            case .addLineToPoint:
                arrayPoints.append(element.points[0])
            case .addQuadCurveToPoint:
                arrayPoints.append(element.points[0])
                arrayPoints.append(element.points[1])
            case .addCurveToPoint:
                arrayPoints.append(element.points[0])
                arrayPoints.append(element.points[1])
                arrayPoints.append(element.points[2])
            default: break
            }
        }
        return arrayPoints
    }
    
    func getPathElementsPointsAndTypes() -> ([CGPoint],[CGPathElementType]) {
        var arrayPoints : [CGPoint]! = [CGPoint]()
        var arrayTypes : [CGPathElementType]! = [CGPathElementType]()
        self.forEach { element in
            switch (element.type) {
            case CGPathElementType.moveToPoint:
                arrayPoints.append(element.points[0])
                arrayTypes.append(element.type)
            case .addLineToPoint:
                arrayPoints.append(element.points[0])
                arrayTypes.append(element.type)
            case .addQuadCurveToPoint:
                arrayPoints.append(element.points[0])
                arrayPoints.append(element.points[1])
                arrayTypes.append(element.type)
                arrayTypes.append(element.type)
            case .addCurveToPoint:
                arrayPoints.append(element.points[0])
                arrayPoints.append(element.points[1])
                arrayPoints.append(element.points[2])
                arrayTypes.append(element.type)
                arrayTypes.append(element.type)
                arrayTypes.append(element.type)
            default: break
            }
        }
        return (arrayPoints,arrayTypes)
    }
}

extension OrderTrackingViewController {
    
  
    
    func drawCirclePoints (points : Int, radius : CGFloat, center : CGPoint) -> [CGPoint] {
        
   var result = [CGPoint]()
   let radius = Double(radius)
    let pi = 3.14159265358979
    let slice = Double(pi * 2) / Double(points)
        for i in 0..<points {
        
    let angle = slice * Double(i)
    let newX = Double(center.x) + (radius * cos(angle))
    let newY = Double(center.y) + (radius * sin(angle))
    let point = CGPoint(x:newX , y:newY)
            
            result.append(point)
    print("circular points" , point)
        }
   
        
        return result
    }
}

/* let startPos = imgRedCircleOuter.frame.width / 2 - dimension / 2
 let xFactor = (imgRedCircleOuter.frame.width + dimension ) / 12
 let yFactor = (imgRedCircleOuter.frame.width + dimension ) / 40
 
 let circlePath = UIBezierPath(arcCenter: imgRedCircleInner.center, radius: imgRedCircleInner.frame.height / 2, startAngle: 0, endAngle: .pi*2, clockwise: true)
 
 let pointsArray = circlePath.cgPath.getPathElementsPoints()
 print("circlePath.cgPath" , circlePath.cgPath)
 */
