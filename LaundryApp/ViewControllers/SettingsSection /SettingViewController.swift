//
//  SettingViewController.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/2/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit
import MOLH

class SettingViewController: BaseViewController, StoryBoardHandler {
    static var myStoryBoard: (forIphone: String, forIpad: String?) = (Storyboards.home.rawValue, nil)
    
    @IBOutlet weak var tblSettings: UITableView!
     let settingCellReuseIdentifier = "SettingsTableViewCell"
    
    let dataArray : [CellType] = [.pushNotification, .changePwd, .language, .logout ]
    var manager = UpdateProfileManager()
    var managerLogout = LoginManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setUpHeader()
        setUpTable()
    }
    
    func setUpHeader() {
        self.title =  GlobalStatic.getLocalizedString("settings")
       
    }

    func setUpTable() {
        
        tblSettings.delegate = self
        tblSettings.dataSource = self
      tblSettings.register(UINib.init(nibName: settingCellReuseIdentifier, bundle: nil), forCellReuseIdentifier: settingCellReuseIdentifier)
    }
    
 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
extension SettingViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            let cell  = tableView.dequeueReusableCell(withIdentifier: settingCellReuseIdentifier) as! SettingsTableViewCell
        cell.configCell(data: dataArray[indexPath.row])
        cell.switchAction = { [weak self] bool , int in
            print(bool , int)
          //  let st = (bool) ? 1 : 0
      self?.setPushNotificationStatus(status: bool ? 1 : 0)
            
        }
         cell.selectionStyle = .none
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  DesignUtility.getValueFromRatio(75)
    }
    
    
}

extension SettingViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      let option = dataArray[indexPath.row]
        switch option {
        case .pushNotification:
         
          break
        case .changePwd:
            show(viewcontrollerInstance: ResetPasswordViewController.loadVC())
         break
        case .language:
            
         //--ww   Alert.showMsg(msg: "This feature will be implemented in next phase!")
        
            Alert.showWithTwoActions(msg: GlobalStatic.getLocalizedString("choose_language"), okBtnTitle: "English", okBtnAction: {
                if MOLHLanguage.currentAppleLanguage() == "ar" {
                MOLH.setLanguageTo("en")
                MOLH.reset()
                }
            }, cancelBtnTitle: "عربي", cancelBtnAction: {
               // MOLHLanguage.currentAppleLanguage() == "en" ? "ar" : "en"
                 if MOLHLanguage.currentAppleLanguage() == "en" {
                  MOLH.setLanguageTo("ar")
                MOLH.reset()
                }
                
            }, vc : self)

         break
        case .logout:
            
         print("LogOut")
         Alert.showWithTwoActions(msg: GlobalStatic.getLocalizedString("logout_confirmation"), okBtnTitle: GlobalStatic.getLocalizedString("yes"), okBtnAction: {
            
               self.logOutFromServer()
            }, cancelBtnTitle: GlobalStatic.getLocalizedString("no"), cancelBtnAction: {
                
         }, vc : self)

            
            break
        }
    }
    
    func doLogout() {
        UIApplication.shared.applicationIconBadgeNumber = 0
        GlobalStatic.deleteCartData()
        CartData.cartDict = [:]
        CartData.totalQuantity = 0
        CartData.totalAmount = 0
        let userDefault = UserDefaults.standard
        userDefault.set(false, forKey: Login.isLoggedIn)
        self.navigationController?.setViewControllers([LoginViewController.loadVC()], animated: true)
    }
}



enum CellType : String {
    case pushNotification = "push_notification"
    case changePwd = "change_pwd"
    case language = "language"
    case logout = "logout"
}

// MARK: - Networking
extension SettingViewController {
    
    func setPushNotificationStatus(status : Int) {
        
        let requestParam = self.manager.paramsNotificationToggle(status: status)
        
        self.manager.apiNotification(requestParam, completion: {

            if self.manager.isSuccess {
                Alert.showWithCompletion(msg:self.manager.message, completionAction: {
                })
            }
            else {
                
                print("failed")
            }
        })
    }
    
    
    func logOutFromServer() {
        
        let requestParam = self.managerLogout.paramsLogout()
        
        self.managerLogout.apiLogOut(requestParam, completion: {
            
            if self.managerLogout.isSuccess {
                self.doLogout()
            }
            else {
                
                print("failed")
            }
        })
    }
}
