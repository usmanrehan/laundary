//
//  NotificationViewController.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/8/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit
//import RNNotificationView

class NotificationViewController: BaseViewController , StoryBoardHandler {
    static var myStoryBoard: (forIphone: String, forIpad: String?) = (Storyboards.home.rawValue , nil)
    
    @IBOutlet weak var tblNotification: UITableView!
    let cellReuseIdentifier = "NotificationTableViewCell"
    var manager = GetNotificationManager()
    
    lazy var noRecordView : NoRecordFoundView = {
        let view = NoRecordFoundView.init(frame: tblNotification.bounds)
        return view
    }()
    
    lazy var refreshControl = CustomReferesh()
    
    var limit = 10
    var offset = 1
    var pageNo = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
      UIApplication.shared.applicationIconBadgeNumber = 0
         self.title = GlobalStatic.getLocalizedString("notifications")
         setUpTable()
          getNotification(offset: pageNo , limit: limit, isSpinnerNeeded: true)
    }

    
    override func viewDidAppear(_ animated: Bool) {
        
        //--ww showAlertAction()
//        guard let home = getViewController(navigationController: Navigation.currentNavigation!, viewControllerClass: HomeViewController.self, viewControllerStoryboad: (Storyboards.home.rawValue, nil)) else{
//            return
//
//        }
//
//
//        guard let orderListing = getViewController(navigationController: Navigation.currentNavigation!, viewControllerClass: MyOrdersViewController.self, viewControllerStoryboad: (Storyboards.orders.rawValue, nil)) else{
//            return
//
//        }
        
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
//            GlobalStatic.goToOrderDetail(orderID: 180)
//        }
        
}
    

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    func setUpTable() {
        
        tblNotification.delegate = self
        tblNotification.dataSource = self
        
        tblNotification.register(UINib.init(nibName: cellReuseIdentifier, bundle: nil), forCellReuseIdentifier: cellReuseIdentifier)
        
        
        tblNotification.backgroundView = noRecordView
        tblNotification.backgroundView?.isHidden = true
      
        tblNotification.estimatedRowHeight =  DesignUtility.getValueFromRatio(60)
        
        tblNotification.addSubview(refreshControl)
        refreshControl.addTarget(self, action:
            #selector(handleRefresh),
                                 for: UIControlEvents.valueChanged)
        CustomReferesh.addCustomLoader(refereshControl: refreshControl)
        tblNotification.tableFooterView = nil
        
    }
    
    
    @objc func handleRefresh()  {
        print("handleRefresh")
        if !CustomReferesh.imgLogo.isAnimating{
            CustomReferesh.imgLogo.rotate()
        }
        if refreshControl.isRefreshing{
            // CustomReferesh.imgLogo.rotate()
            tblNotification.isUserInteractionEnabled = false
        }
        
    }
}

extension NotificationViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
         tblNotification.backgroundView?.isHidden =  manager.notificationData.count > 0
        return self.manager.notificationData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! NotificationTableViewCell
         let data = self.manager.notificationData[indexPath.row]
         cell.configCell(noti: data.message!, time: data.createdAt! , didRead: data.isRead!)
            cell.selectionStyle = .none
            return cell
 
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        checkForMoreRecords(indexNo: indexPath.row)
    }
    
    
    func checkForMoreRecords(indexNo : Int) {
        
        
        if indexNo == (manager.notificationData.count - 1) {
            
            if pageNo <= manager.totalPages {
                if  !CustomReferesh.imgLogoFooter.isAnimating{
                    CustomReferesh.imgLogoFooter.rotate()
                }
                print("indexNo" , indexNo)
                print("pageNo" , pageNo)
                offset = pageNo //--ww (pageNo * limit) - 1
                getNotification(offset: pageNo , limit: limit, isSpinnerNeeded: false)
            }else{
                tblNotification.tableFooterView = nil
            }
        }
        
    }
    
}

extension NotificationViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
      //--ww   readNotification(indexNo: indexPath.row)
        doPerformAction(indexNo: indexPath.row)
    }
    
    func doPerformAction(indexNo : Int) {
        let noteData = manager.notificationData[indexNo]
        
        switch noteData.actionType! {
        case "order_status":
            let vc = OrderDetailViewController.loadVC()
            vc.orderID = Int(noteData.refId ?? 0)
            show(viewcontrollerInstance: vc)
            break
       default:
        
        Alert.showMsg(msg: noteData.message ?? "")
            break
        }
    }
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return GlobalStatic.getLocalizedString("delete")
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            
            Alert.showWithTwoActions(msg: GlobalStatic.getLocalizedString("delete_notification_confirm"), okBtnTitle: GlobalStatic.getLocalizedString("yes"), okBtnAction: {
               //--ww self.cancelOrder(indexNo: indexPath.row)
                self.deleteNotification(indexNo: indexPath.row)
            }, cancelBtnTitle: GlobalStatic.getLocalizedString("no"), cancelBtnAction: {
                self.tblNotification.reloadRows(at: [indexPath], with: .automatic)
            })
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if refreshControl.isRefreshing {
            print("refreshControl.isRefreshing")
            tblNotification.isUserInteractionEnabled = false
            self.manager.notificationData = []
            pageNo = 1
            offset = 1
            //--ww limit = 10
            tblNotification.tableFooterView = nil //CustomReferesh.getCustomLoaderToFooter()
            getNotification(offset: pageNo , limit: limit, isSpinnerNeeded: false)
        }
    }
}

// MARK: - Networking
extension NotificationViewController {
    
    func getNotification(offset : Int , limit : Int , isSpinnerNeeded : Bool) {
        let requestParam = self.manager.params(offset: offset , limit: limit)
        
        self.manager.apiCall(requestParam, showSpinner: isSpinnerNeeded, completion: {
            
            self.refreshControl.endRefreshing()
            if self.manager.isSuccess {
                print("self.manager.totalPages", self.manager.totalPages)
                self.pageNo += 1
                if self.manager.notificationData.count >  limit - 1{
                    self.tblNotification.tableFooterView = CustomReferesh.getCustomLoaderToFooter()
                }else{
                    self.tblNotification.tableFooterView = nil //CustomReferesh.getCustomLoaderToFooter()
                }
                
                self.tblNotification.isUserInteractionEnabled = true
                self.tblNotification.reloadData()
            }
            else {
                
                print("failed")
            }
        })
    }
    
    func deleteNotification(indexNo : Int)  {
        let noteData = self.manager.notificationData[indexNo]
        let noteID = noteData.id!
        
        let requestParam = self.manager.paramsDelete(notificationID: noteID)
        
        self.manager.apiCall(requestParam, showSpinner: true, completion: {
            
          
            if self.manager.isSuccess {
           
                self.manager.notificationData.remove(at: indexNo)
                self.tblNotification.deleteRows(at: [IndexPath.init(row: indexNo, section: 0)], with: .automatic)
            }
            else {
                
                print("failed")
            }
        })
    }
    
    
    
    func readNotification(indexNo : Int)  {
        let noteData = self.manager.notificationData[indexNo]
        let noteID = noteData.id!
        
        let requestParam = self.manager.paramsReadNotification(notificationID: noteID)
        
        self.manager.apiReadNotification(requestParam, showSpinner: false, completion: {
            
            if self.manager.isSuccess {
                noteData.isRead = 1
             self.manager.notificationData[indexNo] = noteData
                self.tblNotification.reloadRows(at: [IndexPath.init(row: indexNo, section: 0)], with: .automatic)
              
            }
            else {
                
                print("failed")
            }
        })
    }
}
