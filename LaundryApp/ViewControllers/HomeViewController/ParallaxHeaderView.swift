//
//  ParallaxHeaderView.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/1/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit

final class ParallaxHeaderView: UIView {
    
    fileprivate var heightLayoutConstraint = NSLayoutConstraint()
    fileprivate var bottomLayoutConstraint = NSLayoutConstraint()
    fileprivate var containerView = UIView()
    fileprivate var containerLayoutConstraint = NSLayoutConstraint()
    fileprivate var homeTopView : HomeTopView!
    var isAnimating = false
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        self.backgroundColor = .clear
        
        containerView.translatesAutoresizingMaskIntoConstraints = false
        containerView.backgroundColor = UIColor.clear
        
        self.addSubview(containerView)
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[containerView]|",
                                                           options: NSLayoutFormatOptions(rawValue: 0),
                                                           metrics: nil,
                                                           views: ["containerView" : containerView]))
        
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[containerView]|",
                                                           options: NSLayoutFormatOptions(rawValue: 0),
                                                           metrics: nil,
                                                           views: ["containerView" : containerView]))
        
        containerLayoutConstraint = NSLayoutConstraint(item: containerView,
                                                       attribute: .height,
                                                       relatedBy: .equal,
                                                       toItem: self,
                                                       attribute: .height,
                                                       multiplier: 1.0,
                                                       constant: 0.0)
        self.addConstraint(containerLayoutConstraint)
        
        
        
        
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName:"HomeTopView" , bundle: bundle)
        homeTopView = nib.instantiate(withOwner: self, options: nil)[0] as! HomeTopView
        homeTopView.frame = containerView.bounds
        homeTopView.translatesAutoresizingMaskIntoConstraints = false
        homeTopView.backgroundColor = .clear
        homeTopView.lblFirst.text = GlobalStatic.getLocalizedString("laundry_problem")
        homeTopView.lblSecond.text = GlobalStatic.getLocalizedString("we_can")
        homeTopView.lblTitle.text = GlobalStatic.getLocalizedString("my-laundry")
        containerView.addSubview(homeTopView)
        
        
        containerView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[homeTopView]|",
                                                                    options: NSLayoutFormatOptions(rawValue: 0),
                                                                    metrics: nil,
                                                                    views: ["homeTopView" : homeTopView]))
        
        bottomLayoutConstraint = NSLayoutConstraint(item: homeTopView,
                                                    attribute: .bottom,
                                                    relatedBy: .equal,
                                                    toItem: containerView,
                                                    attribute: .bottom,
                                                    multiplier: 1.0,
                                                    constant: 0.0)
        
        containerView.addConstraint(bottomLayoutConstraint)
        
        heightLayoutConstraint = NSLayoutConstraint(item: homeTopView,
                                                    attribute: .height,
                                                    relatedBy: .equal,
                                                    toItem: containerView,
                                                    attribute: .height,
                                                    multiplier: 1.0,
                                                    constant: 0.0)
        
        containerView.addConstraint(heightLayoutConstraint)
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        
        containerLayoutConstraint.constant = scrollView.contentInset.top;
        let offsetY = -(scrollView.contentOffset.y + scrollView.contentInset.top);
        containerView.clipsToBounds = offsetY <= 0
        bottomLayoutConstraint.constant = offsetY >= 0 ? 0 : -offsetY / 2
        heightLayoutConstraint.constant = max(offsetY + scrollView.contentInset.top, scrollView.contentInset.top)
        
        
        let height = DesignUtility.getValueFromRatio(257) * 0.33
        
        //   print("imgHeight", homeTopView.imgLogo.frame.height)
        //    print("calcHeight", height)
        
        if homeTopView.imgLogo.frame.height > height {
        
            if isAnimating == false{
                isAnimating = true
                 self.homeTopView.imgLogo.layer.speed = 1
                if !homeTopView.imgLogo.isAnimating{
               homeTopView.imgLogo.rotate()
                }
               }
            let txt = GlobalStatic.getLocalizedString("we_can")
            if homeTopView.lblSecond.text == txt {
                homeTopView.lblSecond.animate(newText: txt, characterDelay: 0.1)
            }
        }else{
            
            if isAnimating == true {
            isAnimating = false
           homeTopView.imgLogo.stopAnimationAndFinish()
            }
        }
    }
}

