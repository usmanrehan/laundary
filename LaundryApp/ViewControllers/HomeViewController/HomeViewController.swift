//
//  HomeViewController.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/1/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit

class HomeViewController: BaseViewController, StoryBoardHandler {
    static var myStoryBoard: (forIphone: String, forIpad: String?) = (Storyboards.home.rawValue, nil)
    
    @IBOutlet weak var tblHome: UITableView!
    let homeCellReuseIdentifier = "HomeTableViewCell"
    
    var dataArray = [(image:String,title:String)]()
    var isCellAnimated = false
    var manager = GetNotificationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupHeader()
        setUpTable()
        setUpModel()
        Navigation.currentNavigation = self.navigationController
    }
    

    
    func setupHeader() {
        self.navigationController?.isNavigationBarHidden = false
        let img = UIImage.init(named: "cart")?.flipIfNeeded()
       
        self.addLanguageButton()
        self.addBarButtonItemWithImage(img!,CustomNavBarEnum.CustomBarButtonItemPosition.BarButtonItemPositionRight, self, #selector(cartButtonPressed))
        
        self.addBarButtonItemWithImage(UIImage.init(named: "bell")!,CustomNavBarEnum.CustomBarButtonItemPosition.BarButtonItemPositionRight, self, #selector(bellButtonPressed))
         let imgView = UIImageView.init(image: UIImage.init(named: "logo-small-home"))
        imgView.contentMode = .center
        self.addCustomTitleView(imgView)
    }
    
    @objc func bellButtonPressed() {
        
        print("bellButtonPressed")
        if  CurrentUser.userType == .guest {
            
         GlobalStatic.showLoginAlert(vc: self)
        }else {
        show(viewcontrollerInstance: NotificationViewController.loadVC())
        }
    }
    
    @objc func cartButtonPressed() {
        
        print("cartButtonPressed")
      //  if  CurrentUser.userType == .guest {
      //      GlobalStatic.showLoginAlert(vc: self)
       // }else{
            let totalCategoryCount = CartData.cartDict.count
            if totalCategoryCount > 0 {
                show(viewcontrollerInstance: MyCartViewController.loadVC())
            }else{
        show(viewcontrollerInstance: CartPlaceholderViewController.loadVC())
            }
       // }
    }

    func setUpTable() {
        
        tblHome.delegate = self
        tblHome.dataSource = self
        
        let parallaxViewFrame = CGRect(x: 0, y: 0, width: self.tblHome.bounds.width, height: DesignUtility.getValueFromRatio(257))
         tblHome.tableHeaderView  = ParallaxHeaderView(frame: parallaxViewFrame)
        
     
        tblHome.register(UINib.init(nibName: homeCellReuseIdentifier, bundle: nil), forCellReuseIdentifier: homeCellReuseIdentifier)
     }
    
    func setUpModel(){
        
        dataArray = [
            (image: "services",title:GlobalStatic.getLocalizedString("services")),
            (image: "profile",title:GlobalStatic.getLocalizedString("profile")),
            (image: "my_order",title:GlobalStatic.getLocalizedString("my_order")),
            (image: "settings",title:GlobalStatic.getLocalizedString("settings")),
            (image: "contact-us",title:GlobalStatic.getLocalizedString("contact-us"))
            ]
        
       
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    
    override func viewWillAppear(_ animated: Bool) {
         GlobalStatic.getCartData()
        
        if CurrentUser.userType != .guest {
         getNotification(offset: 1, limit: 1, isSpinnerNeeded: false)
        }else{
            let rightBarButtons = self.navigationItem.rightBarButtonItems
            let lastBarButton = rightBarButtons?[1]
              lastBarButton?.removeBadge()
        }
        
        let rightBarButtons = self.navigationItem.rightBarButtonItems
        let lastBarButton = rightBarButtons?[0]
        let totalCategoryCount = CartData.cartDict.count
        //--ww  CartData.totalQuantity
        if totalCategoryCount == 0 {
            lastBarButton?.removeBadge()
        }else{
            lastBarButton?.setBadge(text: "\(totalCategoryCount)")
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        tblHome.reloadData()
         tblHome.isUserInteractionEnabled = true
       
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if !OrderData.orderNotification.type.isEmptyStr() {
            OrderData.orderNotification = ("",0)
            GlobalStatic.showNotificationVC(type: OrderData.orderNotification.type, id: OrderData.orderNotification.id)
        }
    }
    
}
extension HomeViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell  = tableView.dequeueReusableCell(withIdentifier: homeCellReuseIdentifier) as! HomeTableViewCell
     
        cell.configCell(data: dataArray[indexPath.row], index : indexPath.row ,isAnimated: isCellAnimated)
        
        
         if indexPath.row < 5 && isCellAnimated == false {
            let delay = Float(indexPath.row + 1) * 0.20
            perform(#selector(self.slideCell), with: cell, afterDelay:TimeInterval(delay))
        }
       
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
         if indexPath.row < 5 && isCellAnimated  == false {
        cell.isHidden = true
        }
    }
    
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == 4
        {
            isCellAnimated = true
        }
    }
    
//    @objc func slideCell(cell : UITableViewCell)  {
//        Animations.slideView(view: cell)
//    }
}
extension HomeViewController : UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return DesignUtility.getValueFromRatio(83)
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tblHome.isUserInteractionEnabled = false
        
        let cell = tblHome.cellForRow(at: indexPath) as! HomeTableViewCell
        cell.clipsToBounds = true
        cell.viewContainer.clipsToBounds = true
        let option = Options(rawValue : indexPath.row)!
        
        switch option {
        case .services:
            print("services")
             cell.slideViewFromLeftToRight { [weak self] in
            self?.show(viewcontrollerInstance: ServicesViewController.loadVC())
            }
        case .myProfile:
             print("myProfile")
            
           if  CurrentUser.userType != .guest {
            
            cell.slideViewFromLeftToRight { [weak self] in
                 customLoader.show()
                DispatchQueue.main.async {
                      self?.show(viewcontrollerInstance: MyProfileViewController.loadVC())
                 
                }
            
            }
            }else{
            GlobalStatic.showLoginAlert(vc: self)
            self.tblHome.isUserInteractionEnabled = true
            }
            
           
        case .myOrder:
            print("myOrder")
            
            if  CurrentUser.userType == .guest {
                GlobalStatic.showLoginAlert(vc: self)
                self.tblHome.isUserInteractionEnabled = true
            }else{
            cell.slideViewFromLeftToRight { [weak self] in
               
                self?.show(viewcontrollerInstance: MyOrdersViewController.loadVC())
            }
            }
        case .settings:
            
            print("settings")
            
             if  CurrentUser.userType != .guest {
            cell.slideViewFromLeftToRight { [weak self] in
                self?.show(viewcontrollerInstance: SettingViewController.loadVC())
            }
              }else{
                GlobalStatic.showLoginAlert(vc: self)
                self.tblHome.isUserInteractionEnabled = true
            }
        
        case .contactUs:
           // if  CurrentUser.userType == .guest {
           //          GlobalStatic.showLoginAlert(vc: self)
           //         self.tblHome.isUserInteractionEnabled = true
         //   }else{
           cell.slideViewFromLeftToRight { [weak self] in
                    self?.show(viewcontrollerInstance: ContactUsViewController.loadVC())
                }
           // }
            
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let headerView = self.tblHome.tableHeaderView as! ParallaxHeaderView
        headerView.scrollViewDidScroll(scrollView: scrollView)
        
      //--ww  print("headerView" , headerView.frame)
        
    }
    
    
    
}


extension HomeViewController {
    
    func getNotification(offset : Int , limit : Int , isSpinnerNeeded : Bool) {
        let requestParam = self.manager.paramsCountNotification()
        
        self.manager.apiCallGetCount(requestParam, showSpinner: isSpinnerNeeded, completion: {
            
           
            if self.manager.isSuccess {
                
                let rightBarButtons = self.navigationItem.rightBarButtonItems
                let lastBarButton = rightBarButtons?[1]
                
                if self.manager.unreadCount == 0 {
                    lastBarButton?.removeBadge()
                }else{
                   //--ww lastBarButton?.setBadge(text: "\(self.manager.unreadCount)")
                    lastBarButton?.setBadge(text:  "\(self.manager.unreadCount)" , withOffsetFromTopRight: CGPoint.init(x: -5, y: 0))
                }
            
            }
            else {
                
                print("failed")
            }
        })
    }
}

enum Options : Int {
    case services = 0
    case myProfile = 1
    case myOrder = 2
    case settings = 3
    case contactUs = 4
}
