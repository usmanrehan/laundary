//
//  ReportIssueViewController.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/3/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit

class ReportIssueViewController: BaseViewController , StoryBoardHandler {
    static var myStoryBoard: (forIphone: String, forIpad: String?) = (Storyboards.home.rawValue , nil)
    
    @IBOutlet weak var tblReport: UITableView!

    fileprivate var reportFooterView : MyProfileFooterView!
    
    let emailCellReuseIdentifier = "ReportEmailTableViewCell"
    let checkboxCellReuseIdentifier = "ReportChekboxTableViewCell"
    let txtViewCellReuseIdentifier = "ReportTextViewTableViewCell"
    let btnViewCellReuseIdentifier = "ReportButtonTableViewCell"
    
    
    var dataArray = [
     GlobalStatic.getLocalizedString("missing_products"),
     GlobalStatic.getLocalizedString("late_order"),
     GlobalStatic.getLocalizedString("payment_issue"),
     GlobalStatic.getLocalizedString("something_else")
    ]
    
    var selectedFeedback : FeedBackType = .nothing
    var manager = ReportIssueManager()
    var selectedIndexRow : Int!

    override func viewDidLoad() {
        super.viewDidLoad()

     
        self.title = GlobalStatic.getLocalizedString("report_issue")
        setupTable()
        //setupTbleFooter()
        //setupModel()
    }
    
   
    func setupTable(){
        
        tblReport.delegate = self
        tblReport.dataSource = self
    
        tblReport.register(UINib.init(nibName: emailCellReuseIdentifier, bundle: nil), forCellReuseIdentifier: emailCellReuseIdentifier)
        tblReport.register(UINib.init(nibName: checkboxCellReuseIdentifier, bundle: nil), forCellReuseIdentifier: checkboxCellReuseIdentifier)
        tblReport.register(UINib.init(nibName: txtViewCellReuseIdentifier, bundle: nil), forCellReuseIdentifier: txtViewCellReuseIdentifier)
        tblReport.register(UINib.init(nibName: btnViewCellReuseIdentifier, bundle: nil), forCellReuseIdentifier: btnViewCellReuseIdentifier)
        
      }

    
    
    func setupTbleFooter(){
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName:"MyProfileFooterView" , bundle: bundle)
        reportFooterView = nib.instantiate(withOwner: self, options: nil)[0] as! MyProfileFooterView
        reportFooterView.frame = CGRect(x: tblReport.frame.origin.x,
                                           y: tblReport.frame.origin.y,
                                           width : tblReport.frame.size.width,
                                           height : DesignUtility.getValueFromRatio(75))
        
        reportFooterView.btnUpdate .setTitle(GlobalStatic.getLocalizedString("report"), for: .normal)
        
        reportFooterView.translatesAutoresizingMaskIntoConstraints = false
        
        reportFooterView.updateAction = { [weak self] in
            
            self?.validateInput()
           
        }
        
    
        tblReport.tableFooterView = reportFooterView
    }
    
    
 
    func validateInput() {
        
        let cell = tblReport.cellForRow(at: IndexPath.init(row: 5, section: 0)) as! ReportTextViewTableViewCell
        
        let comments = cell.txtIssues.text
        if selectedFeedback == . nothing {
            Alert.showMsg(msg: GlobalStatic.getLocalizedString("select_option"))
        }else if selectedFeedback == .something_else{
            if comments!.isEmptyStr(){
                
                Alert.showMsg(msg: GlobalStatic.getLocalizedString("issue_detail"))
            }else {
                reportIssues(txt: comments! , type: selectedFeedback)
            }
        }else{
             reportIssues(txt: comments! , type: selectedFeedback)
        }
            
        
       //  self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
extension ReportIssueViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  7 //dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if indexPath.row == 0 {
            
            let cell  = tableView.dequeueReusableCell(withIdentifier: emailCellReuseIdentifier) as! ReportEmailTableViewCell
            
             cell.selectionStyle = .none
            return cell
        }else if indexPath.row > 0 && indexPath.row < 5 {
            let cell  = tableView.dequeueReusableCell(withIdentifier: checkboxCellReuseIdentifier) as! ReportChekboxTableViewCell
            
            cell.configCell(data: dataArray[indexPath.row - 1 ])
             cell.selectionStyle = .none
            if (selectedIndexRow != nil) && selectedIndexRow == indexPath.row {
                cell.isChecked = true
                cell.btnCheckBox .setImage(UIImage.init(named: "checkbox-sel"), for: .normal)
                selectedFeedback = FeedBackType(rawValue : indexPath.row)!
            }else{
               cell.isChecked = false
                cell.btnCheckBox .setImage(UIImage.init(named: "checkbox"), for: .normal)
                //selectedFeedback = .nothing
            }
            
            
            return cell
        }else if indexPath.row == 5{
            
            let cell  = tableView.dequeueReusableCell(withIdentifier: txtViewCellReuseIdentifier) as! ReportTextViewTableViewCell
            
          //  cell.configCell(data: dataArray[indexPath.row])
             cell.selectionStyle = .none
            return cell
        }else {
            
            let cell  = tableView.dequeueReusableCell(withIdentifier: btnViewCellReuseIdentifier) as! ReportButtonTableViewCell
            
            cell.reportBtnAction = { [weak self] in
                
              self?.validateInput()
            }
            
             cell.selectionStyle = .none
            return cell
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return DesignUtility.getValueFromRatio(150)
        }
        if indexPath.row > 0 && indexPath.row < 5  {
            return DesignUtility.getValueFromRatio(40)
        }else if indexPath.row == 5{
       return DesignUtility.getValueFromRatio(230)
        }else{
           return DesignUtility.getValueFromRatio(80)
        }
    }
    
}

extension ReportIssueViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row > 0 && indexPath.row < 5 {
        selectedIndexRow = indexPath.row
      tblReport.reloadData()
        }
//        if indexPath.row > 0 && indexPath.row < 5 {
//        let cell = tblReport.cellForRow(at: indexPath) as! ReportChekboxTableViewCell
//
//
//            if cell.isChecked == false {
//
//                cell.isChecked = true
//
//                cell.btnCheckBox .setImage(UIImage.init(named: "checkbox-sel"), for: .normal)
//                selectedFeedback = FeedBackType(rawValue : indexPath.row)!
//            }else{
//                cell.isChecked = false
//                cell.btnCheckBox .setImage(UIImage.init(named: "checkbox"), for: .normal)
//                selectedFeedback = .nothing
//            }
//        }
        
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
//        if indexPath.row > 0 && indexPath.row < 5 {
//            let cell = tblReport.cellForRow(at: indexPath) as! ReportChekboxTableViewCell
//           cell.isChecked = false
//            cell.btnCheckBox .setImage(UIImage.init(named: "checkbox"), for: .normal)
//
//        }
        
    }
    
}

enum FeedBackType: Int{
    case missing_product = 1
    case late_order
    case payment_issue
    case something_else
    case nothing
    
    
}

extension ReportIssueViewController{
    
    func reportIssues(txt : String , type : FeedBackType) {
        
        
        let requestParam = self.manager.params(text : txt , index: type.rawValue)
        
        self.manager.api(requestParam, completion: {
            
            if self.manager.isSuccess {
                //self.manager.message ?? "Your feedback has been recorded successfully!"
                Alert.showWithCompletion(msg: self.manager.message, completionAction: {
                    self.navigationController?.popViewController(animated: true)
                })
            }
            else {
                
            }
        })
    }
}
