//
//  FeedbackViewController.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/3/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit
import MOLH

class FeedbackViewController: BaseViewController , StoryBoardHandler {
    static var myStoryBoard: (forIphone: String, forIpad: String?) = (Storyboards.home.rawValue, nil)
    
    @IBOutlet weak var txtViewFeedback: BaseUITextView!
    @IBOutlet weak var lblCounter: BaseUILabel!
    @IBOutlet weak var btnDone: CustomButton!
    
    var manager = FeedbackManager()
      let characterCountLimit = Constants.maximumLengthTextView
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = GlobalStatic.getLocalizedString("feedback")
        txtViewFeedback.delegate = self
        lblCounter.text = "0 / \(characterCountLimit)"
      txtViewFeedback.tag = -1
       
        txtViewFeedback.placeholderTxt = GlobalStatic.getLocalizedString("enter_detail")
        btnDone.setTitle(GlobalStatic.getLocalizedString("done"), for: .normal)
        if MOLHLanguage.currentAppleLanguage() == "en" {
            txtViewFeedback.textAlignment = .left
        }else{
            txtViewFeedback.textAlignment = .right
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func doneBtnPressed(_ sender: Any) {
       // self.navigationController?.popViewController(animated: true)
        
        let comments = txtViewFeedback.text
        
        if (comments?.isEmptyStr())! {
            Alert.showMsg(msg: GlobalStatic.getLocalizedString("empty_feedback"))
        }else{
            postFeedback(txt: comments!)
        }
    }
    
    

}
extension FeedbackViewController : UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
      
        
        // We need to figure out how many characters would be in the string after the change happens
        let startingLength = txtViewFeedback.text?.count ?? 0
        let lengthToAdd = text.count
        let lengthToReplace = range.length
        
        let newLength = startingLength + lengthToAdd - lengthToReplace
        if newLength <= characterCountLimit{
        lblCounter.text = "\(newLength) / \(characterCountLimit)"
            return true
        }
        return  false //newLength <= characterCountLimit
    }
}

extension FeedbackViewController{
    
    func postFeedback(txt : String) {
        
        
        let requestParam = self.manager.params(text : txt)
        
        self.manager.api(requestParam, completion: {
            
            if self.manager.isSuccess {
             //self.manager.message ??  "Your feedback has been recorded successfully!"
                Alert.showWithCompletion(msg:self.manager.message , completionAction: {
                    self.navigationController?.popViewController(animated: true)
                })
            }
            else {
                
            }
        })
    }
}
