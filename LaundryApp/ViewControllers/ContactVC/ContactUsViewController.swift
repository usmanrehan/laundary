//
//  ContactUsViewController.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/2/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit
import MessageUI
import SwiftyPickerPopover

class ContactUsViewController: BaseViewController, StoryBoardHandler {
    static var myStoryBoard: (forIphone: String, forIpad: String?) = (Storyboards.home.rawValue , nil)
    
    @IBOutlet weak var tblContactUs: UITableView!
    
    let contactHeaderTableViewCell = "ContactHeaderTableViewCell"
    let contactTableViewCell = "ContactTableViewCell"
    
    var dataArray = [(imgName : String , title : String)]()
    var manager = GetContactManager()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.title = GlobalStatic.getLocalizedString("contact-us")
        setupTable()
        setupModel()
        getContactInfo()
    }

    func setupTable(){
        
        tblContactUs.delegate = self
        tblContactUs.dataSource = self
   
        
        tblContactUs.register(UINib.init(nibName: contactHeaderTableViewCell, bundle: nil), forCellReuseIdentifier: contactHeaderTableViewCell)
        
        tblContactUs.register(UINib.init(nibName: contactTableViewCell, bundle: nil), forCellReuseIdentifier: contactTableViewCell)
        
       
    }
    
    func setupModel(){
        
        dataArray = [
        (imgName : "contact" , title : "(123) 456-7890"),
        (imgName : "contact" , title : "N/A"),
        (imgName : "email" , title : "N/A"),
        (imgName : "feedback" , title : GlobalStatic.getLocalizedString("feedback")),
        (imgName : "report" , title : GlobalStatic.getLocalizedString("report_issue"))
        ]
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
extension ContactUsViewController : UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell  = tableView.dequeueReusableCell(withIdentifier: contactHeaderTableViewCell) as! ContactHeaderTableViewCell
            cell.selectionStyle = .none
           // cell.configCell(data: dataArray[indexPath.row])
            return cell
        }else{
            
            let cell  = tableView.dequeueReusableCell(withIdentifier: contactTableViewCell) as! ContactTableViewCell
            
          cell.configCell(data: dataArray[indexPath.row])
            cell.selectionStyle = .none
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 0 {
            return DesignUtility.getValueFromRatio(270)
        }
        
        return DesignUtility.getValueFromRatio(75)
    }
    
}
extension ContactUsViewController : UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row {
        case 0:
            break
        case 1:
            
            
           let phoneArray =  dataArray[indexPath.row].title.components(separatedBy: ",")
            
            let theCell = tblContactUs.cellForRow(at: indexPath)
            
            let p = StringPickerPopover(title: GlobalStatic.getLocalizedString("contact-us"), choices: phoneArray)
               .setDoneButton(title:"Call", action: { (popover, selectedRow, selectedString) in
                print("done row \(selectedRow) \(selectedString)")
                 selectedString.makePhoneCall()
               })
                .setCancelButton(title:"Cancel", action: { (_, _, _) in print("cancel")} )
            
            p.appear(originView: theCell!, baseViewWhenOriginViewHasNoSuperview: tblContactUs, baseViewController: self)
            
        
           
            break
        case 2:
             let emailString = dataArray[indexPath.row].title
            sendEmail(email: emailString)
            break
        case 3:
            if CurrentUser.userType == .registered {
                show(viewcontrollerInstance: FeedbackViewController.loadVC())
            }else{
             GlobalStatic.showLoginAlert(vc: self)
            }
           
            break
        case 4:
            if CurrentUser.userType == .registered {
                show(viewcontrollerInstance: ReportIssueViewController.loadVC())
            }else{
                GlobalStatic.showLoginAlert(vc: self)
            }
            
            break
        default:
            break
        }
    }
    
}
extension ContactUsViewController : MFMailComposeViewControllerDelegate{
    
    func sendEmail(email : String) {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([email])
            mail.setMessageBody("<p>This is sample email</p>", isHTML: true)
            
            present(mail, animated: true)
        } else {
            // show failure alert
        }
    }
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
}

extension ContactUsViewController{
    
    func getContactInfo() {
        
        
        let requestParam = self.manager.params()
        
        self.manager.api(requestParam, completion: {
            
            if self.manager.isSuccess {
                self.dataArray[1].title = self.manager.resultData?.phone ?? "N/A"
                self.dataArray[2].title = self.manager.resultData?.email ?? ""
                self.tblContactUs.reloadData()
            }
            else {
                
            }
        })
    }
}
