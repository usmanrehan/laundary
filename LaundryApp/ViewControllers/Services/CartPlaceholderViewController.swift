//
//  CartPlaceholderViewController.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/8/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit

class CartPlaceholderViewController: BaseViewController , StoryBoardHandler {
    static var myStoryBoard: (forIphone: String, forIpad: String?) = (Storyboards.services.rawValue, nil)
    
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var lblAddCloths: BaseUILabel!
    @IBOutlet weak var btnStart: CustomButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = GlobalStatic.getLocalizedString("cart")
        btnStart.setTitle(GlobalStatic.getLocalizedString("start_laundry"), for: .normal)
        lblAddCloths.text = GlobalStatic.getLocalizedString("add_cloths")
        
        btnStart .addTarget(self, action: #selector(startLaundry), for: .touchUpInside)
        setupForAnimation()
        perform(#selector(animate), with: nil, afterDelay: 0.2)
    }
    

    
    func setupForAnimation() {
        imgLogo.isHidden = true
        lblAddCloths.isHidden = true
        btnStart.isHidden = true
    }
    
    @objc func animate(){
        
        // var t = 0.1
        //  var diff = 0.5
        
        perform(#selector(flipImg), with: nil, afterDelay: 0.0)
        perform(#selector(flipLabel), with: nil, afterDelay: 0.1)
        perform(#selector(flipStart), with: nil, afterDelay: 0.2)
   
        
        // perform(#selector(imgLogo.flipFromRight), with: nil, afterDelay: 0.17)
    }
    
    @objc func flipImg() {
        imgLogo.flipFromRight()
    }
    
    @objc func flipLabel() {
        lblAddCloths.flipFromRight()
    }
    
    @objc func flipStart() {
        btnStart.flipFromRight()
    }
   
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func startLaundry()  {
        
        self.pushOrPopViewController(navigationController: (self.navigationController)!, animation: true, viewControllerClass: ServicesViewController.self, viewControllerStoryboad: (Storyboards.services.rawValue, nil))
      //  show(viewcontrollerInstance: ServicesViewController.loadVC())
    }
    
}
