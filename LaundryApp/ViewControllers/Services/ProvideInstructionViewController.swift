//
//  ProvideInstructionViewController.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/8/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit

class ProvideInstructionViewController: BaseViewController , StoryBoardHandler {
    static var myStoryBoard: (forIphone: String, forIpad: String?) = (Storyboards.services.rawValue , nil)
    
    @IBOutlet weak var tblInstruction: UITableView!
    let settingCellReuseIdentifier = "SettingsTableViewCell"
    let txtViewCellReuseIdentifier = "ReportTextViewTableViewCell"
    let btnViewCellReuseIdentifier = "ReportButtonTableViewCell"
    
   var manager = UpdateInstructionsManager()
    
    var dataArray = [
        (GlobalStatic.getLocalizedString("fold_cloths") , false),
        (GlobalStatic.getLocalizedString("at_my_door"), false),
        (GlobalStatic.getLocalizedString("call_before_pickup"), false),
        (GlobalStatic.getLocalizedString("call_before_delivery"), false),
        (GlobalStatic.getLocalizedString("save_configuration") , false)
     ]
    var provideInstruction = ProvideInstructionModel()
    //--ww var saveThis : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
      self.title = GlobalStatic.getLocalizedString("provide_instruction")
      
        
        if let data = UserDefaults.standard.value(forKey: UserDefaultKey.isConfigurationSaved) as? Data {
            let dictInstruction = try? PropertyListDecoder().decode(ProvideInstructionModel.self, from: data)
            
           if dictInstruction?.saveConfiguration == true {
             provideInstruction = dictInstruction ?? ProvideInstructionModel()
             dataArray[0].1 = dictInstruction?.isFold ?? false
             dataArray[1].1 = dictInstruction?.atMyDoor ?? false
             dataArray[2].1 = dictInstruction?.callBeforePickup ?? false
             dataArray[3].1 = dictInstruction?.callBeforeDelivery ?? false
             dataArray[4].1 = dictInstruction?.saveConfiguration ?? false
           //--ww saveThis = dictInstruction?.saveConfiguration ?? false
            }
        }
        
        dataArray[0].1 = CurrentUser.data?.orderInstruction?.isFold?.boolValue ?? false
        dataArray[1].1 = CurrentUser.data?.orderInstruction?.atMyDoor?.boolValue ?? false
        dataArray[2].1 = CurrentUser.data?.orderInstruction?.callMeBeforePickup?.boolValue ?? false
        dataArray[3].1 = CurrentUser.data?.orderInstruction?.callMeBeforeDelivery?.boolValue ?? false
     
        
          setUpTable()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUpTable() {
        
        tblInstruction.delegate = self
        tblInstruction.dataSource = self
        
        tblInstruction.register(UINib.init(nibName: settingCellReuseIdentifier, bundle: nil), forCellReuseIdentifier: settingCellReuseIdentifier)
        
        tblInstruction.register(UINib.init(nibName: txtViewCellReuseIdentifier, bundle: nil), forCellReuseIdentifier: txtViewCellReuseIdentifier)
        
        tblInstruction.register(UINib.init(nibName: btnViewCellReuseIdentifier, bundle: nil), forCellReuseIdentifier: btnViewCellReuseIdentifier)
    }

}
extension ProvideInstructionViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count + 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row < 5 {
        let cell  = tableView.dequeueReusableCell(withIdentifier: settingCellReuseIdentifier) as! SettingsTableViewCell
            cell.configCellProvideInstruction(data: dataArray[indexPath.row] , indexNo : indexPath.row)
            cell.switchAction = { [weak self] state , indexNo in
            self?.handleSwitchAction(state, indexNo)
            }
        cell.selectionStyle = .none
        return cell
    }else if indexPath.row == 5{
            
            let cell  = tableView.dequeueReusableCell(withIdentifier: txtViewCellReuseIdentifier) as! ReportTextViewTableViewCell
            cell.txtIssues.text =  CurrentUser.data?.orderInstruction?.other ?? "" //provideInstruction.otherInstruction
            cell.configCellProvideInstruction()
            cell.selectionStyle = .none
            return cell
        }else {
            
            let cell  = tableView.dequeueReusableCell(withIdentifier: btnViewCellReuseIdentifier) as! ReportButtonTableViewCell
            
           
            cell.configCell(data:GlobalStatic.getLocalizedString("save_instruction"))
            cell.reportBtnAction = { [weak self] in
                
               self?.saveBtnPressed()
            }
            
            cell.selectionStyle = .none
            return cell
            
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row < 5 {
        return  DesignUtility.getValueFromRatio(75)
        } else if indexPath.row == 5{
            return DesignUtility.getValueFromRatio(170)
        }else{
            return DesignUtility.getValueFromRatio(100)
        }
    }
    
    
    func handleSwitchAction(_ state : Bool , _ indexNo : Int){
        
        switch indexNo {
        case 0:
            provideInstruction.isFold = state
            dataArray[0].1 = state
            break
        case 1:
             provideInstruction.atMyDoor = state
             dataArray[1].1 = state
            break
        case 2:
             provideInstruction.callBeforePickup = state
             dataArray[2].1 = state
            break
        case 3:
             provideInstruction.callBeforeDelivery = state
             dataArray[3].1 = state
            break
        case 4:
             provideInstruction.saveConfiguration = state
            break
        default:
            break
        }
        
    }
    
    func saveBtnPressed() {
        
        if let cell  = tblInstruction.cellForRow(at: IndexPath.init(row: 5, section: 0)) as? ReportTextViewTableViewCell{
            provideInstruction.otherInstruction = cell.txtIssues.text
        }
     if provideInstruction.saveConfiguration == true {
        saveInstruction()
    }
          UserDefaults.standard.set(try? PropertyListEncoder().encode(provideInstruction), forKey:UserDefaultKey.isConfigurationSaved)
        
        let paramsDict = [
            "is_fold": dataArray[0].1,
            "at_my_door":dataArray[1].1,
            "call_me_before_pickup":dataArray[2].1,
            "call_me_before_delivery":dataArray[3].1,
            "other":provideInstruction.otherInstruction
        ] as [String : AnyObject]
        
        CartData.params["order_instructions"] = paramsDict as AnyObject
        
        self.navigationController?.popViewController(animated: true)
        
    }
}

extension ProvideInstructionViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        
    }
    
  
}

extension ProvideInstructionViewController {
    
    func saveInstruction() {
        
        let paramsDict = [
            "user_id" : CurrentUser.data?.id ?? 0,
            "is_fold": provideInstruction.isFold,
            "at_my_door":provideInstruction.atMyDoor,
            "call_me_before_pickup":provideInstruction.callBeforePickup,
            "call_me_before_delivery":provideInstruction.callBeforeDelivery,
            "other":provideInstruction.otherInstruction
            ] as [String : AnyObject]
        
        let requestParam = self.manager.params(parameters: paramsDict )
        
        self.manager.api(requestParam, completion: {
            
            if self.manager.isSuccess {
              
            }
            else {
                
                print("failed")
            }
        })
}
}



