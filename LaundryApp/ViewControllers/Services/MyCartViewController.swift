//
//  MyCartViewController.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/7/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit

class MyCartViewController: BaseViewController, StoryBoardHandler {
    static var myStoryBoard: (forIphone: String, forIpad: String?) = (Storyboards.services.rawValue, nil)
    

    @IBOutlet weak var lblTotalCost: BaseUILabel!
    @IBOutlet weak var lblCostNeeded: BaseUILabel!
    @IBOutlet weak var lblNeededText: BaseUILabel!
    @IBOutlet weak var imgBottomArrow: UIImageView!
    @IBOutlet weak var lblTotalText: BaseUILabel!
    @IBOutlet weak var tblCart: UITableView!
    
    let cellReuseIdentifier = "CartViewContainerTableViewCell"
    var dataArray = [MyCartModel]()
    var mainArray = [[(quantity : Int, amount: Float , item : String , section :Int)]]()
    var isCellAnimated = false
    override func viewDidLoad() {
        super.viewDidLoad()
     // Do any additional setup after loading the view.
        self.title = GlobalStatic.getLocalizedString("cart")
        setupTable()
        setupModel()
        lblTotalCost.text = "\(CartData.totalAmount) AED"
        lblCostNeeded.text = "10.00 AED"
        lblNeededText.text = GlobalStatic.getLocalizedString("needed_amt")
        lblTotalText.text = GlobalStatic.getLocalizedString("total")
        imgBottomArrow.image = UIImage.init(named: "bottom_arrow")?.flipIfNeeded()
        print(mainArray)
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupTable() {
        tblCart.delegate = self
        tblCart.dataSource = self
        
        tblCart.register(UINib.init(nibName: cellReuseIdentifier, bundle: nil), forCellReuseIdentifier: cellReuseIdentifier)
        tblCart.estimatedRowHeight = DesignUtility.getValueFromRatio(200)
        
       
        
    }
    
    func setupModel(){
        dataArray = [
            MyCartModel(title: "Dry Clean" , imgName : "wash-fold-cart" , dArray : [
                (item : "2 x Shirt" , cost: "AED 15"),
                (item : "3 x T-shirts" , cost: "AED 23"),
                (item : "1 x Jeans" , cost: "AED 6")
             
                ]),
            
            MyCartModel(title: "Steam Ironing" , imgName : "iron-cart" , dArray : [
                (item : "2 x Shirt" , cost: "AED 15"),
                (item : "3 x T-shirts" , cost: "AED 23"),
                (item : "1 x Jeans" , cost: "AED 6"),
                (item : "1 x Jeans" , cost: "AED 6"),
                (item : "1 x Jeans" , cost: "AED 6")
                ]),
            
            MyCartModel(title: "Wash & Iron" , imgName : "wash-iron-cart" , dArray : [
                (item : "2 x Shirt" , cost: "AED 15"),
                (item : "3 x T-shirts" , cost: "AED 23"),
                (item : "1 x Jeans" , cost: "AED 6")
                ]),
        ]
        
      //--ww  var mainArray = [[(quantity : Int, amount: Float , item : String , section :Int)]]()
        let data = CartData.cartDict
        for i in 0...2 {
            let filtered = data.filter { $0.value.section == i}
            let array = Array(filtered.values)
            
            mainArray.append(array)
            
        }
        
         tblCart.reloadData()
        
        
    }
    
    @IBAction func footerBtnTapped(_ sender: Any) {
        
        
        if CartData.totalAmount < 10 {
            Alert.showMsg(msg: GlobalStatic.getLocalizedString("minimum_amount_for_order"), btnActionTitle: GlobalStatic.getLocalizedString("ok"), vc: self)
        }else{
        typealias itemDict = [String : AnyObject]
            var arr = [itemDict]()
            for data in CartData.cartDict{
             let dict = [
                "amount":data.value.amount,
                "item_id" : data.key,
                 "quantity" : data.value.quantity
                ] as [String : AnyObject]
                
             arr.append(dict)
            }
           
           CartData.params["order_detail"] =  arr as [itemDict] as AnyObject
         //--ww   CartData.orderDetail = arr
            CartData.params["amount"] = CartData.totalAmount as AnyObject
            CartData.params["total_amount"] = CartData.totalAmount as AnyObject
            
            print(CartData.params)
            
            GlobalStatic.saveCartData()
            
        show(viewcontrollerInstance: OrderInformationViewController.loadVC())
        }
    }
    
}
extension MyCartViewController : UITableViewDataSource {
    
 
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  mainArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! CartViewContainerTableViewCell
        cell.selectionStyle = .none
       //--ww cell.configCell(data: dataArray[indexPath.row])
        cell.configCellFromServerData(data: mainArray[indexPath.row], indexNo: indexPath.row)
        
        cell.isHidden = true
        if cell.animated == false {
            cell.animated = true
        let delay = Float(indexPath.row + 1) * 0.20
        perform(#selector(self.slideCell), with: cell, afterDelay:TimeInterval(delay))
            cell.setNeedsLayout()
        }else{
            cell.isHidden = false
        }
    
        return cell
        
    }
    
//    @objc func slideCell(cell : UITableViewCell)  {
//        Animations.slideView(view: cell)
//    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
       
       //cell.isHidden = true
    }
        

    
    
//
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if mainArray[indexPath.row].count < 1 {
            return 0
        }
        return UITableViewAutomaticDimension
    }

    
}

extension MyCartViewController : UITableViewDelegate {
    
    
}
