//
//  PaymentMethodViewController.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/7/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit
import TelrSDK

class PaymentMethodViewController: BaseViewController , StoryBoardHandler {
    static var myStoryBoard: (forIphone: String, forIpad: String?) = (Storyboards.services.rawValue , nil)
    
    @IBOutlet weak var tblPayment: UITableView!
    
    let cellReuseIdentifier = "OrderInfoTableViewCell"
    let dataArray = [GlobalStatic.getLocalizedString("by_credit") , GlobalStatic.getLocalizedString("by_cash")]
    var manager = SaveOrderManager()
    
    let KEY:String = "pLDV3-hfsk#nZ82Z"  // TODO fill key
    let STOREID:String = "21187"         // TODO fill store id
    let EMAIL:String = "info@gt2030.ae"  // TODO fill email
    
     var paymentRequest:PaymentRequest?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title =  GlobalStatic.getLocalizedString("payment_methods")
        setUpTable()
         CartData.params["user_id"] = CurrentUser.data?.id as AnyObject
         CartData.params["delivery_amount"] = CartData.instanceSurCharge as AnyObject
         print(CartData.params)
        
      
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func setUpTable() {
        
        tblPayment.delegate = self
        tblPayment.dataSource = self
        tblPayment.register(UINib.init(nibName: cellReuseIdentifier, bundle: nil), forCellReuseIdentifier: cellReuseIdentifier)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? TelrController{
             destination.paymentRequest = paymentRequest!
        }
    }
    
    private func preparePaymentRequest() -> PaymentRequest{
        
        let paymentReq = PaymentRequest()
        paymentReq.key = KEY
        paymentReq.store = STOREID
        paymentReq.appId = "123456789"
        paymentReq.appName = "Laundry 2030"
        paymentReq.appUser = "123456"
        paymentReq.appVersion = "0.0.1"
        paymentReq.transTest = "1"
        paymentReq.transType = "auth"
        paymentReq.transClass = "paypage"
        paymentReq.transCartid = String(arc4random())
        paymentReq.transDesc = "iOS API"
        paymentReq.transCurrency = "AED"
        paymentReq.transAmount =  "\(CartData.params["total_amount"] as? Double ?? 0.0 )"
        paymentReq.transLanguage = "en"
        paymentReq.billingEmail = EMAIL
        paymentReq.billingFName =  CurrentUser.data?.name ?? "James"
        paymentReq.billingLName = ""
        paymentReq.billingTitle = "Mr"
        paymentReq.city = ""
        paymentReq.country = ""
        paymentReq.region = "Dubai" // \(Cart.user?.location?.street ?? "Dubai")"
        paymentReq.address = CurrentUser.data?.address ?? "" //\(Cart.user?.location?.buildingName ?? "Dubai")"
        paymentReq.billingPhone = CurrentUser.data?.phone ?? ""
        return paymentReq
    }
    
    
}
extension PaymentMethodViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! OrderInfoTableViewCell
        cell.configCell(data: dataArray[indexPath.row])
        cell.selectionStyle = .none
        cell.isHidden = true
        if cell.isAnimated == false {
            cell.isAnimated = true
            let delay = Float(indexPath.row + 1) * 0.20
            perform(#selector(self.slideCell), with: cell, afterDelay:TimeInterval(delay))
        }else{
            cell.isHidden = false
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  DesignUtility.getValueFromRatio(95)
    }
    
    
}

extension PaymentMethodViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
           //--ww  Alert.showMsg(msg: GlobalStatic.getLocalizedString("implemented_soon"))
            self.navigationController?.isNavigationBarHidden = true
            paymentRequest = preparePaymentRequest()
            performSegue(withIdentifier: "TelrSegue", sender: paymentRequest)
        }else{
            
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/yyyy"
            CartData.params.updateValue("cash" as AnyObject, forKey: "payment_type")
            CartData.params.updateValue(formatter.string(from: Date()) as AnyObject, forKey: "payment_date")
            CartData.params.updateValue("" as AnyObject, forKey: "transaction_id")
            saveOrder()
       
        }
    }
    
    
}
// MARK: - Networking
extension PaymentMethodViewController {
    
    func saveOrder() {
        
         let requestParam = self.manager.params(parametes: CartData.params)
       self.manager.api(requestParam, completion: {
            
            if self.manager.isSuccess {
                
             //--ww  print(self.manager.orderData)
                CartData.SD.isInstance = false
                
                let vc = OrderSummaryViewController.loadVC()
                vc.orderData = self.manager.orderData
                self.show(viewcontrollerInstance: vc)
            }else {
                
                print("failed")
            }
        })
    }
}

