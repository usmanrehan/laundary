//
//  OrderInformationViewController.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/7/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit

class OrderInformationViewController: BaseViewController , StoryBoardHandler {
    static var myStoryBoard: (forIphone: String, forIpad: String?) = (Storyboards.services.rawValue , nil)
    

    @IBOutlet weak var tblOrderInfo: UITableView!
    @IBOutlet weak var lblsubTotal: BaseUILabel!
    @IBOutlet weak var txtCoupone: BaseUITextField!
    @IBOutlet weak var lblTotal: BaseUILabel!
    @IBOutlet weak var lblFooterTotal: BaseUILabel!
    @IBOutlet weak var lblTextSubTotal: BaseUILabel!
    @IBOutlet weak var lblTextTotal: BaseUILabel!
    @IBOutlet weak var lblCheckOut: BaseUILabel!
    @IBOutlet weak var imgBottomArrow: UIImageView!
    
    
    @IBOutlet weak var lblTextInstantSurcharge: BaseUILabel!
    @IBOutlet weak var lblAmountInstantSurcharge: BaseUILabel!
    @IBOutlet weak var lblInstantorderTopConstraint: NSLayoutConstraint!
    
    let settingCellReuseIdentifier = "SettingsTableViewCell"

    let dataArray = [GlobalStatic.getLocalizedString("schedule_wash"), GlobalStatic.getLocalizedString("provide_instruction")
    ]
    var manager = PromoCodeManager()
    var managerInstant = InstantOrderManager()
  
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
         self.navigationController?.isNavigationBarHidden = false
        self.title = GlobalStatic.getLocalizedString("order_info")
        lblsubTotal.text = "\(CartData.totalAmount) AED"
        lblTotal.text = "\(CartData.totalAmount) AED"
        lblFooterTotal.text = "\(CartData.totalAmount) AED"
        txtCoupone.text = "" //"LNKipM"
        txtCoupone.placeholder = GlobalStatic.getLocalizedString("promo_code")
        lblTextSubTotal.text = GlobalStatic.getLocalizedString("sub_total")
        lblTextTotal.text = GlobalStatic.getLocalizedString("payment")
        lblCheckOut.text = GlobalStatic.getLocalizedString("check_out")
        imgBottomArrow.image = UIImage.init(named: "bottom_arrow")?.flipIfNeeded()
        setUpTable()
        CartData.instanceSurCharge = 0.0
        lblTextInstantSurcharge.text = ""
        lblAmountInstantSurcharge.text = ""
        lblInstantorderTopConstraint.constant = 0
         getInstantText()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
     
        super.viewDidAppear(animated)
        
        if Float(CartData.instanceSurCharge) > Float(0.0) {
            lblTextInstantSurcharge.text = GlobalStatic.getLocalizedString("instant_surcharge")
            lblAmountInstantSurcharge.text = "\(CartData.instanceSurCharge) AED"
            lblInstantorderTopConstraint.constant = DesignUtility.getValueFromRatio(20)
            
            let amount = CartData.totalAmount + Float(CartData.instanceSurCharge)
            lblsubTotal.text = "\(CartData.totalAmount) AED"
            lblTotal.text = "\(amount) AED"
            lblFooterTotal.text = "\(amount) AED"
            
            
            self.view.layoutIfNeeded()
        }else{
            lblTextInstantSurcharge.text = ""
            lblAmountInstantSurcharge.text = ""
            lblInstantorderTopConstraint.constant = 0
            
            lblsubTotal.text = "\(CartData.totalAmount) AED "
            lblTotal.text = "\(CartData.totalAmount) AED "
            lblFooterTotal.text = "\(CartData.totalAmount) AED "
             self.view.layoutIfNeeded()
            }
    }
    

    @IBAction func footerBtnTapped(_ sender: Any) {
        
        
        var isSavedConfiguration = false
        
        if let data = UserDefaults.standard.value(forKey: UserDefaultKey.isConfigurationSaved) as? Data {
            let dictInstruction = try? PropertyListDecoder().decode(ProvideInstructionModel.self, from: data)
            isSavedConfiguration =  (dictInstruction?.saveConfiguration)!
            
            if isSavedConfiguration == true {
                let paramsDict  = [
                "is_fold": dictInstruction?.isFold ?? false,
                "at_my_door":dictInstruction?.atMyDoor ?? false,
                "call_me_before_pickup":dictInstruction?.callBeforePickup ?? false,
                "call_me_before_delivery":dictInstruction?.callBeforeDelivery ?? false,
                "other":dictInstruction?.otherInstruction ?? ""
                    ] as [String : Any]
            
            CartData.params["order_instructions"] = paramsDict as AnyObject
            }
            }
        
            if CartData.params["pick_type"] == nil{
               Alert.showMsg(msg: GlobalStatic.getLocalizedString("select_address_time"))
            }else if CartData.params["order_instructions"] == nil && isSavedConfiguration == false {
                Alert.showMsg(msg: GlobalStatic.getLocalizedString("provide_instruction_needed"))
                
           }else if (txtCoupone.text?.isEmptyStr())!{
                
                let afterAddingSurcharge = CartData.totalAmount + CartData.instanceSurCharge
                CartData.params["amount"] =  CartData.totalAmount as AnyObject
                CartData.params["total_amount"] =  afterAddingSurcharge as AnyObject
                CartData.params["redeemed_code"] = "" as AnyObject
                CartData.params["redeemed_code_amount"] = 0 as AnyObject
           show(viewcontrollerInstance: PaymentMethodViewController.loadVC())
            }else {
                
                verifyCode(txt : txtCoupone.text!)
        }
     
    }
    
    func setUpTable() {
        
        tblOrderInfo.delegate = self
        tblOrderInfo.dataSource = self
        
        tblOrderInfo.register(UINib.init(nibName: settingCellReuseIdentifier, bundle: nil), forCellReuseIdentifier: settingCellReuseIdentifier)
    }
    

}
extension OrderInformationViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: settingCellReuseIdentifier) as! SettingsTableViewCell
     
        cell.selectionStyle = .none
         cell.isHidden = true
        if cell.isAnimated == false {
        let delay = Float(indexPath.row + 2) * 0.25
        perform(#selector(self.slideCell), with: cell, afterDelay:TimeInterval(delay))
            cell.isAnimated = true
        }else{
            
            cell.isHidden = false
        }
           cell.configCellOrderInfo(data: dataArray[indexPath.row])
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  DesignUtility.getValueFromRatio(75)
    }
    
    
}

extension OrderInformationViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if  CurrentUser.userType == .guest {
            GlobalStatic.showLoginAlert(vc: self)
        }else{
        if indexPath.row == 0 {
         show(viewcontrollerInstance: ScheduleWashViewController.loadVC())
           }else {
            show(viewcontrollerInstance: ProvideInstructionViewController.loadVC())
        }
        }
    }
}

// MARK: - Networking
extension OrderInformationViewController {
    
    func verifyCode(txt : String) {
        let requestParam = self.manager.params(code: txt)
        
        self.manager.api(requestParam, completion: {
            if self.manager.isSuccess {
                if self.manager.couponData?.isRedeemed == "0"{
                    self.adjustTotalAmount(data: self.manager.couponData!)
                    }else{
               
                    Alert.showWithCompletion(msg: GlobalStatic.getLocalizedString("code_expired"), completionAction: {
                        self.txtCoupone.text = ""
                    })
                }
              
            }
            else {
                
                print("failed")
                self.txtCoupone.text = ""
            }
        })
    }
    
    
    func adjustTotalAmount(data : GCResult) {
        
        let originalAmount = CartData.totalAmount
        var afterAddingSurcharge = CartData.totalAmount + CartData.instanceSurCharge
        var discountAmount : Float = 0.0
       // redeemed_code and coupon amount redeemed_code_amount
        if data.type == "percentage" {
            
            discountAmount = afterAddingSurcharge * Float(data.amount!) / 100
            
        }else{
            discountAmount =  Float(data.amount!)
            
        }
        
       afterAddingSurcharge = afterAddingSurcharge - discountAmount
        
      //--ww  CartData.amtFinal = afterAddingSurcharge
        lblsubTotal.text = "\(originalAmount) AED"
        lblTotal.text = "\(afterAddingSurcharge) AED"
        lblFooterTotal.text = "\(afterAddingSurcharge) AED"
        
        CartData.params["amount"] = originalAmount as AnyObject
        CartData.params["total_amount"] =  afterAddingSurcharge as AnyObject
        CartData.params["redeemed_code"] = data.code as AnyObject
        CartData.params["redeemed_code_amount"] = discountAmount as AnyObject
        
        Alert.showWithCompletion(msg:  "\(GlobalStatic.getLocalizedString("total_amount_in_aed")) \(originalAmount) \n \(GlobalStatic.getLocalizedString("discount_amount_aed"))  \(discountAmount) \n \(GlobalStatic.getLocalizedString("payable_amount_aed")) \(afterAddingSurcharge)", completionAction: {
              self.show(viewcontrollerInstance: PaymentMethodViewController.loadVC())
        })
        
     //--ww   DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {}
      
    }
    
    
    func getInstantText() {
        let requestParam = self.managerInstant.params()
        
        self.managerInstant.api(requestParam, completion: {
            if self.managerInstant.isSuccess {
             
                CurrentUser.instantOrder = self.managerInstant.instanceText
            }else {
                print("failed")
             }
        })
    }
}
