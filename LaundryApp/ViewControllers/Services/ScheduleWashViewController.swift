//
//  ScheduleWashViewController.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/8/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit
import SwiftyPickerPopover
import MOLH

class ScheduleWashViewController: BaseViewController , StoryBoardHandler {
    static var myStoryBoard: (forIphone: String, forIpad: String?) = (Storyboards.services.rawValue , nil)
    
    @IBOutlet weak var tblSheduleWash: UITableView!
    let topCellReuseIdentifier = "InstantWashTableViewCell"
    let optionCellReuseIdentifier = "ScheduleTableViewCell"
    let btnViewCellReuseIdentifier = "ReportButtonTableViewCell"
    var sectionArray = ["" , GlobalStatic.getLocalizedString("pickup_schedule") ,  GlobalStatic.getLocalizedString("delivery_schedule")]
    var dataArray = [[(title: String, imgName: String )]] ()
     let sectionHeaderHeight = DesignUtility.getValueFromRatio(30)
     var managerAddress = GetAddressManager()
     var managerTimeSlot = TimeSlotManager()
     var pickupTimeSlots =  [TSAvailableTimeSlot]()
     var deliveryTimeSlots =  [TSAvailableTimeSlot]()
     var isInstance  = false
     var selectedTimes : (String? , String?) = (nil, nil)
     var selectedDates : (Date? , Date?) = (nil, nil)
     var selectedAddress : (String? , String?) = (nil, nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = GlobalStatic.getLocalizedString("schedule_wash")
        setupTable()
        setupModel()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func goBack() {
       //--ww   CartData.params["pick_type"] = nil
        super.goBack()
    }
    
    func setupTable(){
        tblSheduleWash.delegate = self
        tblSheduleWash.dataSource = self
        tblSheduleWash.register(UINib.init(nibName: topCellReuseIdentifier, bundle: nil), forCellReuseIdentifier: topCellReuseIdentifier)
        tblSheduleWash.register(UINib.init(nibName: optionCellReuseIdentifier, bundle: nil), forCellReuseIdentifier: optionCellReuseIdentifier)
        tblSheduleWash.register(UINib.init(nibName: btnViewCellReuseIdentifier, bundle: nil), forCellReuseIdentifier: btnViewCellReuseIdentifier)
        }
    
    func setupModel() {
        
        if CartData.params["pick_type"] == nil{
        dataArray = [
                [
                    (title: GlobalStatic.getLocalizedString("choose_pickup_address"), imgName: "right_arrow" ),
                ],
                [
                    (title: GlobalStatic.getLocalizedString("choose_pickup_address"), imgName: "right_arrow" ),
                    (title: GlobalStatic.getLocalizedString("choose_pickup_date"), imgName: "pickup_date" ),
                    (title:GlobalStatic.getLocalizedString("choose_pickup_time"), imgName: "down_arrow" )
                ],
                [
                    (title: GlobalStatic.getLocalizedString("choose_delivery_address"), imgName: "right_arrow" ),
                    (title: GlobalStatic.getLocalizedString("choose_delivery_date"), imgName: "pickup_date" ),
                    (title: GlobalStatic.getLocalizedString("choose_delivery_time"), imgName: "down_arrow" ),
                    (title:GlobalStatic.getLocalizedString("choose_delivery_time") , imgName: "down_arrow" )
            ]
        ]
    }else{
            
           let cd = CartData.SD
            selectedAddress =  cd.selectedAddress
            selectedDates =  cd.selectedDates
            selectedTimes =  cd.selectedTimes
            isInstance = cd.isInstance
         
        let pickDateString = (CartData.params["pickup_date"] as! String).toFormatedDateString()
        let deliveryDateString = (CartData.params["delivery_date"] as! String).toFormatedDateString()
            
            let partsPickupTime = (CartData.params["slot"] as! String).components(separatedBy: "-")
            let joinedPickupTime  = " \((partsPickupTime[0] ).to12HourTime()) - \((partsPickupTime[1] ).to12HourTime())"
            
            let partsDeliveryTime = (CartData.params["delivery_slot"] as! String).components(separatedBy: "-")
            let joinedDeliveryTime  = " \((partsDeliveryTime[0] ).to12HourTime()) - \((partsDeliveryTime[1] ).to12HourTime())"
            
            dataArray = [
                [
                    (title: GlobalStatic.getLocalizedString("choose_pickup_address"), imgName: "right_arrow" ),
                    ],
                [
                    (title:CartData.params["pickup_location"] as! String, imgName: "right_arrow" ),
                    (title: pickDateString, imgName: "pickup_date" ),
                    (title:joinedPickupTime, imgName: "down_arrow" )
                ],
                [
                    (title: CartData.params["drop_location"] as! String, imgName: "right_arrow" ),
                    (title: deliveryDateString, imgName: "pickup_date" ),
                    (title: joinedDeliveryTime, imgName: "down_arrow" ),
                    (title:GlobalStatic.getLocalizedString("choose_delivery_time") , imgName: "down_arrow" )
                ]
            ]
            
        }
    }

}
extension ScheduleWashViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionArray.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 && indexPath.section == 0 {
        let cell  = tableView.dequeueReusableCell(withIdentifier: topCellReuseIdentifier) as! InstantWashTableViewCell
        cell.selectionStyle = .none
           cell.configCell(isOn: CartData.SD.isInstance)
            cell.switchAction = { [weak self] state in
                
                self?.handleSwitchAction(isInstant: state )
             
            }
        return cell
        }else if indexPath.section == 2 && indexPath.row == 3 {
            let cell  = tableView.dequeueReusableCell(withIdentifier: btnViewCellReuseIdentifier) as! ReportButtonTableViewCell
            cell.configCell(data: GlobalStatic.getLocalizedString("save"))
            cell.reportBtnAction = { [weak self] in
                
            self?.submitBtnPressed()
            }
            
            cell.selectionStyle = .none
            return cell
        }
        else{
           
            let cell  = tableView.dequeueReusableCell(withIdentifier: optionCellReuseIdentifier) as! ScheduleTableViewCell
            cell.selectionStyle = .none
            cell.configCell(data: dataArray[indexPath.section][indexPath.row])
            return cell
            
        }
    }
    

    
    func handleSwitchAction(isInstant : Bool)  {
        
        self.isInstance = isInstant
        CartData.SD.isInstance = isInstant
        
   //     if self.isInstance == false {
            //--ww setupModel()
            self.dataArray[1][1].title = GlobalStatic.getLocalizedString("choose_pickup_date")
            self.dataArray[1][2].title = GlobalStatic.getLocalizedString("choose_pickup_time")
            self.dataArray[2][1].title = GlobalStatic.getLocalizedString("choose_delivery_date")
            self.dataArray[2][2].title = GlobalStatic.getLocalizedString("choose_delivery_time")
            selectedTimes  = (nil, nil)
            selectedDates  = (nil, nil)
           //--ww selectedAddress  = (nil, nil)
  
    tblSheduleWash.reloadSections(IndexSet.init(integer: 1), with: .automatic)
    tblSheduleWash.reloadSections(IndexSet.init(integer: 2), with: .automatic)
        
     //   }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
      }
    
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        }
    

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0
        }
        return sectionHeaderHeight
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 1 && indexPath.row == 2 {
            return DesignUtility.getValueFromRatio(110)
        }else if indexPath.section == 2 && indexPath.row == 3 {
            return DesignUtility.getValueFromRatio(100)
        }
        return DesignUtility.getValueFromRatio(66)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: sectionHeaderHeight))
        view.backgroundColor = UIColor.groupTableViewBackground
        
        let label = BaseUILabel(frame: CGRect(x: DesignUtility.getValueFromRatio(25), y: 0, width: tableView.bounds.width -  DesignUtility.getValueFromRatio(60), height: sectionHeaderHeight))
        
        label.fontNameTheme = "fontDefault"
        label.fontColorTheme = "darkGray"
        label.fontSizeTheme = "sizeSmall"
        label.text = sectionArray[section]
        label.tag = -1
        view.addSubview(label)
        return view
    }
    
    func submitBtnPressed(){
        
        let status = validateForm()
        
        if status.0 == false {
            Alert.showMsg(msg: status.1)
            }else {
            
            CartData.SD.selectedAddress = selectedAddress
            CartData.SD.selectedDates = selectedDates
            CartData.SD.selectedTimes = selectedTimes
            CartData.SD.isInstance = isInstance
            
            
            
            let pickAdd = selectedAddress.0 ?? ""
            let pickDate =  selectedDates.0?.toString() ?? ""
            let pickTime =  selectedTimes.0 ?? ""
            
            let deliveryAdd = selectedAddress.1 ?? ""
            let deliveryDate = selectedDates.1?.toString() ?? ""
            let deliveryTime = selectedTimes.1 ?? ""
            
        //--ww    CartData.params["pick_type"] = "pickup" as AnyObject
            CartData.params["pickup_location"] = pickAdd as AnyObject
            CartData.params["pickup_date"] = pickDate as AnyObject
            CartData.params["slot"] = pickTime as AnyObject
         
            CartData.params["drop_type"] = "delivery" as AnyObject
            CartData.params["drop_location"] = deliveryAdd as AnyObject
            CartData.params["delivery_date"] = deliveryDate as AnyObject
            CartData.params["delivery_slot"] = deliveryTime as AnyObject
            CartData.params["delivery_amount"] = 0 as AnyObject
            
            self.navigationController?.popViewController(animated: true)
            
        }
    }
}

extension ScheduleWashViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
         if indexPath.section == 1 || indexPath.section == 2{
            
            switch indexPath.row {
            case 0 :
                getAddressData(sec: indexPath.section)
            case 1 :
              
            let cell = tblSheduleWash.cellForRow(at: indexPath) as! ScheduleTableViewCell
            DispatchQueue.main.async {
                    self.showDatePicker(cell : cell , sec: indexPath.section)
                }
            case 2 :
               
                let cell = tblSheduleWash.cellForRow(at: indexPath) as! ScheduleTableViewCell
                DispatchQueue.main.async {
                    self.showTimeSlotPicker(cell: cell, sec: indexPath.section)
                }
            default:
                break
                }
            }
        }
    
    
    func  showDatePicker(cell : ScheduleTableViewCell , sec : Int) {
         var dt =  Date()
        if sec == 2 {
            if selectedDates.0 == nil {
                Alert.showMsg(msg: GlobalStatic.getLocalizedString("pickup_date_needed"))
                return
            }
           dt = selectedDates.0 != nil ? selectedDates.0! : Date()
        }
        
        let title = sec == 1 ? GlobalStatic.getLocalizedString("pickup") : GlobalStatic.getLocalizedString("delivery")
     //--ww   let dt = selectedDates.0 != nil ? selectedDates.0! : Date()
    DatePickerPopover(title: "\(title)")
    .setDateMode(.date)
    .setSelectedDate(Date())
    .setMinimumDate(dt)
    .setLocale(Locale(identifier:"en"))
    .setDoneButton(action: { popover, selectedDate in
        print("selectedDate \(selectedDate)")
        
        if sec == 2 && self.isInstance == false && selectedDate.toString() == self.selectedDates.0?.toString() {
                Alert.showMsg(msg: GlobalStatic.getLocalizedString("instant_wash_dates"))
            
        }else if sec == 2 && self.isInstance == true && selectedDate.toString()
            != self.selectedDates.0?.toString() {
           
             Alert.showMsg(msg: GlobalStatic.getLocalizedString("instant_wash_dates_active"))
        }
        else {
            self.clearExistingValues(section : sec)
            cell.lblTitle.text = selectedDate.toString("dd MMM yyy")
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                   self.getTimeSlot(section: sec, date: selectedDate)
            })
            self.dataArray[sec][1].title = selectedDate.toString("dd MMM yyy")
        }
        
    })
    .setCancelButton(action: { _, _ in print("cancel")})
    .appear(originView: cell.lblTitle, baseViewWhenOriginViewHasNoSuperview: cell, baseViewController: self)
        
    }
    
    
    func clearExistingValues(section : Int) {
        if section == 2 {
           selectedTimes.1 = nil
            self.dataArray[2][2].title = GlobalStatic.getLocalizedString("choose_delivery_time")
            self.tblSheduleWash .reloadRows(at: [IndexPath.init(row: 2, section: 2)], with: .automatic)
            
        }else {
            selectedDates = (nil, nil)
            selectedTimes = (nil, nil)
          //  self.dataArray[1][1].title = GlobalStatic.getLocalizedString("choose_pickup_date")
            self.dataArray[1][2].title = GlobalStatic.getLocalizedString("choose_pickup_time")
            self.dataArray[2][1].title = GlobalStatic.getLocalizedString("choose_delivery_date")
            self.dataArray[2][2].title = GlobalStatic.getLocalizedString("choose_delivery_time")
            self.tblSheduleWash .reloadRows(at: [IndexPath.init(row: 2, section: 1), IndexPath.init(row: 1, section: 2), IndexPath.init(row: 2, section: 2)], with: .automatic)
        }
    }
    
    func getTimeSlot(section : Int , date : Date) {
        if section == 1 {
              selectedDates = (date, nil)
              self.getPickupTimeSlots(pickupDateString: date.toString())
        }else {
            selectedDates.1 = date
           self.getDeliveryTimeSlots(deliveryDate: date.toString())
            
        }
    }
    
    
    func  showTimeSlotPicker(cell : ScheduleTableViewCell , sec: Int) {
    
         let title = sec == 1 ? GlobalStatic.getLocalizedString("choose_pickup_time") : GlobalStatic.getLocalizedString("choose_delivery_time")
        
        let selectedArray = sec == 1 ? self.pickupTimeSlots : self.deliveryTimeSlots
        let selectedDate = sec == 1 ? selectedDates.0 : selectedDates.1
        if selectedDate != nil && selectedArray.count > 0 {
        var timeArray = [String]()
       
         for item in selectedArray {
            let parts = item.slotTime?.components(separatedBy: "-")
            let joined  = " \((parts![0] ).to12HourTime()) - \((parts![1] ).to12HourTime())"
            timeArray.append(joined)
        }
     
     StringPickerPopover(title: "\(title)", choices:timeArray)
            .setFontColor(.darkGray)
            .setDoneButton(
                action: {  popover, selectedRow, selectedString in
                    print("done row \(selectedRow) \(selectedString)")
                    cell.lblTitle.text = selectedString
                    self.dataArray[sec][2].title = selectedString
                    let tSlot = selectedArray[selectedRow].slotTime ?? selectedString
                    self.setTimeSlotData(section: sec , timeSlot : tSlot)
                    if sec == 1 {
                    if self.isInstance == true{
                    CartData.instanceSurCharge = selectedArray[selectedRow].instanceSurcharge!
                    }else{
                      CartData.instanceSurCharge = 0.0
                    }
                    }
            })
            .setCancelButton(action: {_, _, _ in
                print("cancel") })
       .appear(originView: cell.lblTitle, baseViewWhenOriginViewHasNoSuperview: cell, baseViewController: self)
        }else if selectedDate == nil{
            Alert.showMsg(msg: GlobalStatic.getLocalizedString("date_needed_time"))
            
        }else{
            getTimeSlot(section : sec , date : selectedDate!)
            if sec == 1 {
                self.dataArray[2][1].title = GlobalStatic.getLocalizedString("choose_delivery_date")
                self.dataArray[2][2].title = GlobalStatic.getLocalizedString("choose_delivery_time")
                selectedTimes.1  = nil
                selectedDates.1  =  nil
                self.tblSheduleWash.reloadData()
            }
        }
    }
    
    func setTimeSlotData(section: Int , timeSlot : String) {
        if section == 1 {
            selectedTimes.0 = timeSlot
        }else{
            selectedTimes.1 = timeSlot
        }
    }
   
}

// MARK: - Networking
extension ScheduleWashViewController {
    
    func getAddressData(sec : Int) {
        let requestParam = self.managerAddress.params(offset: 1 , limit: 10)
        
        self.managerAddress.api(requestParam, completion: {
            
            if self.managerAddress.isSuccess {
                if self.managerAddress.addressData.count > 0 {
                    let vc = MyAddressViewController.loadVC()
                    vc.addressArray = self.managerAddress.addressData
                    vc.addType = sec == 1 ? .pickup : .delivery
                    vc.delegateAddress = self
                    vc.totalPages = self.managerAddress.totalPages
                     self.show(viewcontrollerInstance:vc )
                }else{
                    
                    let vc = AddAddressViewController.loadVC()
                    vc.delegateAddAddress = self
                     vc.addressType = sec == 1 ? .pickup : .delivery
                    self.show(viewcontrollerInstance: vc)
                
                }
              
            }
            else {
                
            }
        })
    }
    
func getPickupTimeSlots(pickupDateString : String ) {
        
     let dict = [
        "date" : pickupDateString,
        "slot_type" : "order" ,
            "instance" : self.isInstance,
            ] as [String : AnyObject]
    
    print(dict)
    
        let requestParam = self.managerTimeSlot.paramsTimeSlot(parametes:dict)
        
        self.managerTimeSlot.api(requestParam, completion: {
            
            if self.managerTimeSlot.isSuccess {
               
                if self.managerTimeSlot.timeSlotData != nil &&  self.managerTimeSlot.timeSlotData!.count > 0 {
                    self.pickupTimeSlots =  self.managerTimeSlot.timeSlotData!
                    
                    let cell = self.tblSheduleWash.cellForRow(at: IndexPath.init(row: 2, section: 1)) as! ScheduleTableViewCell
                    DispatchQueue.main.async {
                        self.showTimeSlotPicker(cell: cell, sec: 1)
                    }
                    
                }else{
                    Alert.showMsg(msg: GlobalStatic.getLocalizedString("no_timeslot_pickup"))
                     self.selectedTimes.0  = nil
                    let cell = self.tblSheduleWash.cellForRow(at:IndexPath.init(row: 2, section: 1)) as! ScheduleTableViewCell
                    cell.lblTitle.text = GlobalStatic.getLocalizedString("choose_pickup_time")
                }
            }
            else {
                
            }
        })
    }
    
    func getDeliveryTimeSlots(deliveryDate : String ) {
        
        let dict = [
            "pickup_date" : selectedDates.0?.toString() as AnyObject,
            "date" : deliveryDate,
            "slot_type" : "delivery" ,
            "instance" : self.isInstance,
            "slot" : selectedTimes.0 as AnyObject
            ] as [String : AnyObject]
        
        let requestParam = self.managerTimeSlot.paramsTimeSlot(parametes:dict)
        
        self.managerTimeSlot.api(requestParam, completion: {
            
            if self.managerTimeSlot.isSuccess {
                if self.managerTimeSlot.timeSlotData != nil && self.managerTimeSlot.timeSlotData!.count > 0 {
                    self.deliveryTimeSlots =  self.managerTimeSlot.timeSlotData!
                    
                    let cell = self.tblSheduleWash.cellForRow(at: IndexPath.init(row: 2, section: 2)) as! ScheduleTableViewCell
                    DispatchQueue.main.async {
                        self.showTimeSlotPicker(cell: cell, sec: 2)
                    }
                }else{
                    Alert.showMsg(msg: GlobalStatic.getLocalizedString("no_timeslot_delivery"))
                    self.selectedTimes.1  = nil
                    let cell = self.tblSheduleWash.cellForRow(at:IndexPath.init(row: 2, section: 2)) as! ScheduleTableViewCell
                    cell.lblTitle.text = GlobalStatic.getLocalizedString("choose_delivery_time")
                }
            }
            else {
                
            }
        })
    }

    
    func validateForm() -> (Bool , String)  {
        
        let pickAdd = selectedAddress.0 ?? ""
        let pickDate =  selectedDates.0?.toString() ?? ""
        let pickTime =  selectedTimes.0 ?? ""
        
        let deliveryAdd = selectedAddress.1 ?? ""
        let deliveryDate = selectedDates.1?.toString() ?? ""
        let deliveryTime = selectedTimes.1 ?? ""
      
        if (pickAdd.isEmptyStr()){
            return (false, GlobalStatic.getLocalizedString("pickup_add_needed"))
        }else if pickDate.isEmptyStr(){
            return (false, GlobalStatic.getLocalizedString("pickup_date_needed"))
        }else if (pickTime.isEmptyStr()){
            return (false, GlobalStatic.getLocalizedString("pickup_time_needed"))
        }
        else if (deliveryAdd.isEmptyStr()){
            return (false, GlobalStatic.getLocalizedString("delivery_add_needed"))
        }else if deliveryDate.isEmptyStr(){
            return (false, GlobalStatic.getLocalizedString("delivery_date_needed"))
        }else if (deliveryTime.isEmptyStr()){
            return (false, GlobalStatic.getLocalizedString("delivery_time_needed"))
        }
        
        return (true, "")
    }
}


extension ScheduleWashViewController : MyAddressProtocol {
    func gotAddress(add: GAData, type: AddressType) {
        print("gotAddress" , add , type)
        
        if type == .pickup {
            selectedAddress.0 = GlobalStatic.getFormatedAddress(add: add.location ?? "")
            CartData.params["pickup_latitude"] = add.latitude as AnyObject
            CartData.params["pickup_longitude"] = add.latitude as AnyObject
            CartData.params["pick_type"] = add.type as AnyObject
        }else{
            selectedAddress.1 = GlobalStatic.getFormatedAddress(add: add.location ?? "")
            CartData.params["dropup_latitude"] = add.latitude as AnyObject
            CartData.params["dropup_longitude"] = add.latitude as AnyObject
            CartData.params["delivery_type"] = add.type as AnyObject
            }
        let secIndex = type == .pickup ? 1 : 2
        let addArr = add.location?.components(separatedBy: "|")
        dataArray[secIndex][0] = (title: addArr![0], imgName: "right_arrow" )
        
        tblSheduleWash.reloadData()
    }
}

extension ScheduleWashViewController : AddAddressProtocol {
    
    func addressSaved(add: GAData, type: AddressType) {
        
        CurrentUser.selectedAddressID = add.id!
        UserDefaults.standard.set(CurrentUser.selectedAddressID, forKey: UserDefaultKey.selectedAddress)
        if type == .pickup {
            selectedAddress.0 = GlobalStatic.getFormatedAddress(add: add.location ?? "")
            CartData.params["pickup_latitude"] = add.latitude as AnyObject
            CartData.params["pickup_longitude"] = add.latitude as AnyObject
            CartData.params["pick_type"] = add.type as AnyObject
        }else{
            selectedAddress.1 = GlobalStatic.getFormatedAddress(add: add.location ?? "")
            CartData.params["dropup_latitude"] = add.latitude as AnyObject
            CartData.params["dropup_longitude"] = add.latitude as AnyObject
            CartData.params["delivery_type"] = add.type as AnyObject
        }
        let secIndex = type == .pickup ? 1 : 2
        let addArr = add.location?.components(separatedBy: "|")
        dataArray[secIndex][0] = (title: addArr![0], imgName: "right_arrow" )
        
        tblSheduleWash.reloadData()
    }
  
}
