//
//  AddAddressViewController.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/8/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import GooglePlacePicker

protocol AddAddressProtocol  : class {
    func addressSaved(add: GAData , type : AddressType) -> Void
}

class AddAddressViewController: BaseViewController , StoryBoardHandler {
    static var myStoryBoard: (forIphone: String, forIpad: String?) = (Storyboards.services.rawValue , nil)
    
    var addressType : AddressType = .pickup
    var addressData : GAData? = nil
    var placesClient: GMSPlacesClient!
    var selectedPlace : GMSPlace!
    var managerAddAddress = AddAddressManager()
    var managerUpdateAddress = UpdateAddressManager()
    @IBOutlet weak var tblAddress: UITableView!
    
    let optionCellReuseIdentifier = "EditProfileTableViewCell"
    let twoTxtCellReuseIdentifier = "AddAddressTableViewCell"
    let btnViewCellReuseIdentifier = "ReportButtonTableViewCell"
    var optionsView: OptionsView!
    var  dataArray = [AddAddressModel]()
    var selectedCategory = "home"
    weak var delegateAddAddress : AddAddressProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
         placesClient = GMSPlacesClient.shared()
        if addressType == .pickup {
        self.title = GlobalStatic.getLocalizedString("add_pickup_address")
        }else{
        self.title = GlobalStatic.getLocalizedString("add_delivery_address")
        }
        setupTable()
        configOptionsView()
        setupModel()
        
    }

    func configOptionsView() {
        
        let frameHeader = CGRect(x: 0 , y : 0 , width : tblAddress.frame.width , height: DesignUtility.getValueFromRatio(190))
        optionsView = OptionsView.init(frame: frameHeader)
        
        optionsView.lblHeading.text = GlobalStatic.getLocalizedString("select_address")
     
        optionsView.dataArray = [
            (imgName: "home" , title : GlobalStatic.getLocalizedString("home")),
            (imgName: "office" , title : GlobalStatic.getLocalizedString("office")),
            (imgName: "location" , title : GlobalStatic.getLocalizedString("other")),
            
        ]
        
        optionsView.delegateOptions = self
        var index = 0
        
        if addressData != nil {
            index = getIndexFromType(addressData?.type ??  "")
            
        }
        optionsView.selectedCellIndex = index
        optionsView.colViewOptions.reloadData()
        
        optionsView.colViewOptions.selectItem(at:IndexPath.init(item: index, section: 0), animated: false, scrollPosition: .centeredHorizontally)
        
        tblAddress.tableHeaderView = optionsView
    }
    
    func getIndexFromType(_ type : String) -> Int {
        var index = 0
        switch type {
        case "home":
            index = 0
        case "office": // 
            index = 1
        case "other":
            index = 2
        default:
            index = 1
        }
        print("selected" , index)
        return index
    }
    
    func setupTable(){
        
        tblAddress.delegate = self
        tblAddress.dataSource = self
        
        
        tblAddress.register(UINib.init(nibName: optionCellReuseIdentifier, bundle: nil), forCellReuseIdentifier: optionCellReuseIdentifier)
        tblAddress.register(UINib.init(nibName: btnViewCellReuseIdentifier, bundle: nil), forCellReuseIdentifier: btnViewCellReuseIdentifier)
        tblAddress.register(UINib.init(nibName: twoTxtCellReuseIdentifier, bundle: nil), forCellReuseIdentifier: twoTxtCellReuseIdentifier)
        
     //--ww   tblAddress.estimatedRowHeight = DesignUtility.getValueFromRatio(100)
    }
    
    func setupModel() {
     if addressData == nil {
      dataArray = [
            AddAddressModel.init(_placeholders: (GlobalStatic.getLocalizedString("location"),""), _values: ("","")),
            AddAddressModel.init(_placeholders: (GlobalStatic.getLocalizedString("street_name"),""), _values: ("","")),
              AddAddressModel.init(_placeholders: (GlobalStatic.getLocalizedString("building_name"),""), _values: ("","")),
              AddAddressModel.init(_placeholders: (GlobalStatic.getLocalizedString("floor"),GlobalStatic.getLocalizedString("appartment")), _values: ("","")),
          AddAddressModel.init(_placeholders: (GlobalStatic.getLocalizedString("landmark"),""), _values: ("","")),
          AddAddressModel.init(_placeholders: (GlobalStatic.getLocalizedString("city"),GlobalStatic.getLocalizedString("country")), _values: ("",""))
        ]
        }else{
           
            let arrayAddress = addressData?.location?.components(separatedBy: "|")
            dataArray = [
                AddAddressModel.init(_placeholders: (GlobalStatic.getLocalizedString("location"),""), _values: (arrayAddress![0],"")),
                AddAddressModel.init(_placeholders: (GlobalStatic.getLocalizedString("street_name"),""), _values: (arrayAddress![1],"")),
                AddAddressModel.init(_placeholders: (GlobalStatic.getLocalizedString("building_name"),""), _values: (arrayAddress![2],"")),
                AddAddressModel.init(_placeholders: (GlobalStatic.getLocalizedString("floor"),GlobalStatic.getLocalizedString("appartment")), _values: (arrayAddress![3],arrayAddress![4])),
                AddAddressModel.init(_placeholders: (GlobalStatic.getLocalizedString("landmark"),""), _values: (arrayAddress![5],"")),
                AddAddressModel.init(_placeholders: (GlobalStatic.getLocalizedString("city"),GlobalStatic.getLocalizedString("country")), _values: (arrayAddress![6],arrayAddress![7])),
            ]
            
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}


extension AddAddressViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if indexPath.row == 3 || indexPath.row == 5{
            let cell  = tableView.dequeueReusableCell(withIdentifier: twoTxtCellReuseIdentifier) as! AddAddressTableViewCell
            cell.configCell(data: dataArray[indexPath.row])
            cell.selectionStyle = .none
            return cell
        }
        else if indexPath.row < 6 {
            let cell  = tableView.dequeueReusableCell(withIdentifier: optionCellReuseIdentifier) as! EditProfileTableViewCell
            cell.configCellDataAddress(data: dataArray[indexPath.row] , indexNo: indexPath.row)
            
            cell.selectionStyle = .none
            return cell
        }else if indexPath.row == 6 {
            let cell  = tableView.dequeueReusableCell(withIdentifier: btnViewCellReuseIdentifier) as! ReportButtonTableViewCell
            cell.configCell(data: GlobalStatic.getLocalizedString("save_address"))
            cell.reportBtnAction = { [weak self] in
                
               // print(self?.dataArray)
                self?.saveBtnTapped()
            }
            
            cell.selectionStyle = .none
            return cell
        }
        
        return UITableViewCell()
        
    }
    
    
    
//    @objc func slideCell(cell : UITableViewCell)  {
//        Animations.slideView(view: cell)
//    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    }
    
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
      }
    
 func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 6 {
            return DesignUtility.getValueFromRatio(100)
        }
        return DesignUtility.getValueFromRatio(65)
    }
    
   
}

extension AddAddressViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
        if indexPath.row == 0 {
            pickPlace()
        }
    }
    
    func pickPlace() {
      //--ww  25.197197,55.2743764
        let center = CLLocationCoordinate2D(latitude: 25.197197, longitude: 55.2743764)
        let northEast = CLLocationCoordinate2D(latitude: center.latitude + 0.001, longitude: center.longitude + 0.001)
        let southWest = CLLocationCoordinate2D(latitude: center.latitude - 0.001, longitude: center.longitude - 0.001)
        let viewport = GMSCoordinateBounds(coordinate: northEast, coordinate: southWest)
        let config = GMSPlacePickerConfig(viewport: viewport)
        let placePicker = GMSPlacePickerViewController(config: config)
        placePicker.delegate = self
        
        present(placePicker, animated: true, completion: nil)
      
    }
    
   
}
extension AddAddressViewController : GMSPlacePickerViewControllerDelegate {
    // To receive the results from the place picker 'self' will need to conform to
    // GMSPlacePickerViewControllerDelegate and implement this code.
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        // Dismiss the place picker, as it cannot dismiss itself.
        viewController.dismiss(animated: true, completion: nil)
        
      //  print("Place name \(place.name)")
      //  print("Place address \(place.formattedAddress)")
      //  print("Place attributions \(place.attributions)")
        
        selectedPlace = place
        var city : String? = nil
        var country : String? = nil
        
        
        if let addData  = place.addressComponents {
        for component in addData {
            if component.type == "locality" {
                print(component.name)
                city = component.name
            }
            if component.type == "country" {
                print(component.name)
                 country = component.name
            }
        }
            
//            dataArray = [
//                AddAddressModel.init(_placeholders: ("Location",""), _values: (place.formattedAddress ?? place.name ,"")),
//                AddAddressModel.init(_placeholders: ("Street Name",""), _values: ("","")),
//                AddAddressModel.init(_placeholders: ("Building Name",""), _values: ("","")),
//                AddAddressModel.init(_placeholders: ("Floor (optional)","Appartment (optional)"), _values: ("","")),
//                AddAddressModel.init(_placeholders: ("Nearest Landmark (optional)",""), _values: ("","")),
//                AddAddressModel.init(_placeholders: ("City","Country"), _values: (city ?? "", country ?? ""))
//            ]
            
            
            self.dataArray = [
                AddAddressModel.init(_placeholders: (GlobalStatic.getLocalizedString("location"),""), _values: (place.formattedAddress ?? place.name,"")),
                AddAddressModel.init(_placeholders: (GlobalStatic.getLocalizedString("street_name"),""), _values: ("","")),
                AddAddressModel.init(_placeholders: (GlobalStatic.getLocalizedString("building_name"),""), _values: ("","")),
                AddAddressModel.init(_placeholders: (GlobalStatic.getLocalizedString("floor"),GlobalStatic.getLocalizedString("appartment")), _values: ("","")),
                AddAddressModel.init(_placeholders: (GlobalStatic.getLocalizedString("landmark"),""), _values: ("","")),
                AddAddressModel.init(_placeholders: (GlobalStatic.getLocalizedString("city"),GlobalStatic.getLocalizedString("country")), _values: (city ?? "", country ?? ""))
            ]
            
            tblAddress.reloadData()
            
        }else{
            
            getAddress(location: place)
            
        }
        
       // let places = place.formattedAddress?.components(separatedBy: "-")
      //    AddAddressModel.init(_placeholders: ("City","Country"), _values: (city ?? "", country ?? "")),
     
    }
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        // Dismiss the place picker, as it cannot dismiss itself.
        viewController.dismiss(animated: true, completion: nil)
        
        print("No place selected")
    }
    
    func getAddress(location: GMSPlace){
        
      //--ww customLoader.show()
        let geocoder = GMSGeocoder()
        
        var currentAddress = String()
        
        geocoder.reverseGeocodeCoordinate(location.coordinate) { response , error in
          //  print(response)
            
            if let address = response?.firstResult() {
                let lines = address.lines! as [String]
                
                currentAddress = lines.joined(separator: ",")
                
                print(currentAddress)
                
                
//                self.dataArray = [
//                    AddAddressModel.init(_placeholders: ("Location",""), _values: (address.thoroughfare ?? currentAddress , "")),
//                    AddAddressModel.init(_placeholders: ("Street Name",""), _values: ("","")),
//                    AddAddressModel.init(_placeholders: ("Building Name",""), _values: ("","")),
//                    AddAddressModel.init(_placeholders: ("Floor (optional)","Appartment (optional)"), _values: ("","")),
//                    AddAddressModel.init(_placeholders: ("Nearest Landmark (optional)",""), _values: ("","")),
//                    AddAddressModel.init(_placeholders: ("City","Country"), _values: (address.locality ??  "", address.country ?? ""))
//                ]
                
                
                
                self.dataArray = [
                    AddAddressModel.init(_placeholders: (GlobalStatic.getLocalizedString("location"),""), _values: (address.thoroughfare ?? currentAddress , "")),
                    AddAddressModel.init(_placeholders: (GlobalStatic.getLocalizedString("street_name"),""), _values: ("","")),
                    AddAddressModel.init(_placeholders: (GlobalStatic.getLocalizedString("building_name"),""), _values: ("","")),
                    AddAddressModel.init(_placeholders: (GlobalStatic.getLocalizedString("floor"),GlobalStatic.getLocalizedString("appartment")), _values: ("","")),
                    AddAddressModel.init(_placeholders: (GlobalStatic.getLocalizedString("landmark"),""), _values: ("","")),
                    AddAddressModel.init(_placeholders: (GlobalStatic.getLocalizedString("city"),GlobalStatic.getLocalizedString("country")), _values: (address.locality ??  "", address.country ?? ""))
                ]
                
                
                self.tblAddress.reloadData()
               //--ww customLoader.hide()
            }
        }
    }
    
    
}





extension AddAddressViewController : OptionsViewProtocol {
    
    func didSelectItemAtIndex(index: Int) {
      print(optionsView.dataArray[index])
       selectedCategory = optionsView.dataArray[index].title
       /* addressData = nil
        dataArray = [
            AddAddressModel.init(_placeholders: ("Location",""), _values: ("","")),
            AddAddressModel.init(_placeholders: ("Street Name",""), _values: ("","")),
            AddAddressModel.init(_placeholders: ("Building Name",""), _values: ("","")),
            AddAddressModel.init(_placeholders: ("Floor (optional)","Appartment (optional)"), _values: ("","")),
            AddAddressModel.init(_placeholders: ("Nearest Landmark (optional)",""), _values: ("","")),
            AddAddressModel.init(_placeholders: ("City","Country"), _values: ("","")),
        ] */
        tblAddress.reloadData()
    }
}

extension AddAddressViewController {
  
    func saveBtnTapped() {
        let status = validateForm()
        if status.0 == false {
            Alert.showMsg(msg: status.1)
            
        }else {
            var address = dataArray[0].values.0
            for i in 1..<dataArray.count{
                address = address + "|" + dataArray[i].values.0
                if i == 3 || i == 5 {
                  address = address + "|" + dataArray[i].values.1
                }
                
            }
            
            if addressData == nil {
            let parameters = [
                "user_id" : CurrentUser.data?.id ?? 0,
                "latitude" : selectedPlace.coordinate.latitude,
                "longitude" :selectedPlace.coordinate.longitude,
                "location" :address,
                "type" : selectedCategory.lowercased(),
           
                ] as [String : AnyObject]
                
                print(parameters)
         
             addAddress(params: parameters)
            }else{
            let parameters = [
                "id" : addressData?.id ?? "",
                "user_id" : CurrentUser.data?.id ?? 0,
                "latitude" : addressData?.latitude!,
                "longitude" :addressData?.longitude!,
                "location" :address,
                "type" : selectedCategory.lowercased()
                
                ] as [String : AnyObject]
                
                // print(parameters)
                updateAddress(params: parameters)
                
            }
        }
    }
    
    
    
    func validateForm() -> (Bool , String)  {
        
        
        
        let location = dataArray[0].values.0
        let street =  dataArray[1].values.0
        let building =  dataArray[2].values.0
     /*
        let floor =  dataArray[3].values.0
        let appartment = dataArray[3].values.1
        let landmark =  dataArray[4].values.0
        let city =  dataArray[5].values.0
        let country = dataArray[5].values.1
        */
        if (location.isEmptyStr()){
            return (false, GlobalStatic.getLocalizedString("location_from_map"))
        }else if street.isEmptyStr(){
            return (false, GlobalStatic.getLocalizedString("street_name_needed"))
        }else if (building.isEmptyStr()){
            return (false, GlobalStatic.getLocalizedString("building_name_needed"))
        }
        
        return (true, "")
    }
}

enum AddressType : String{
    case pickup = "Pickup"
    case delivery = "Delivery"
}

// MARK: - Networking
extension AddAddressViewController {
    
    func addAddress(params : [String : AnyObject]) {
        let requestParam = self.managerAddAddress.paramsAdd(parametes: params as [String : AnyObject])
        
        self.managerAddAddress.api(requestParam, completion: {
            
            if self.managerAddAddress.isSuccess {
           
                self.delegateAddAddress?.addressSaved(add: self.managerAddAddress.addressObject!, type: self.addressType)
                 self.navigationController?.popViewController(animated: true)
              //--ww self.pushOrPopViewController(navigationController: self.navigationController!, animation: true, viewControllerClass: MyAddressViewController.self, viewControllerStoryboad: (Storyboards.services.rawValue , nil))
            }
            else {
                
                print("failed")
            }
        })
    }
    
    
    
    func updateAddress(params : [String : AnyObject]) {
        let requestParam = self.managerUpdateAddress.paramsUpdate(parametes: params as [String : AnyObject])
        
        self.managerUpdateAddress.api(requestParam, completion: {
            if self.managerUpdateAddress.isSuccess {
               
                self.delegateAddAddress?.addressSaved(add: self.managerUpdateAddress.addressObject!, type: self.addressType)
                self.navigationController?.popViewController(animated: true)
               // self.pushOrPopViewController(navigationController: self.navigationController!, animation: true, viewControllerClass: MyAddressViewController.self, viewControllerStoryboad: (Storyboards.services.rawValue , nil))
            }
            else {
                
                print("failed")
            }
        })
    }
}




