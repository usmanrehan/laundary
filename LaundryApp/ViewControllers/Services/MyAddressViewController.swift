//
//  MyAddressViewController.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/8/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit

protocol MyAddressProtocol  :class {
    func gotAddress(add: GAData , type : AddressType) -> Void
}
class MyAddressViewController: BaseViewController, StoryBoardHandler {
    static var myStoryBoard: (forIphone: String, forIpad: String?) = (Storyboards.services.rawValue , nil)
    
    @IBOutlet weak var tblAddress: UITableView!
    let cellReuseIdentifier = "MyAddressTableViewCell"
     let btnViewCellReuseIdentifier = "ReportButtonTableViewCell"
    var dataArray = [String]()
    var addressArray : [GAData]!
    var addType : AddressType!
    weak var delegateAddress : MyAddressProtocol?
    var isAnimationCompleted : Bool = false
    var manager = GetAddressManager()
    var selectedIndex : Int = -1
    var selectedCellIndex = -1
    var editingIndex = -1
    var totalPages = 0
    var limit = 10
    var offset = 1
    var pageNo = 1
     let cellToAnimate = DeviceUtility.getDeviceType()  == .iPhoneX ? 5 : 4
    
    @IBOutlet weak var btnSubmit: CustomButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        setupHeader()
        setUpTable()
        setupModel()
        btnSubmit.setTitle(GlobalStatic.getLocalizedString("submit"), for: .normal)
        CurrentUser.selectedAddressID = UserDefaults.standard.integer(forKey: UserDefaultKey.selectedAddress)
       
    }
    
    func setupHeader() {
          self.title = GlobalStatic.getLocalizedString("my_address")
          self.addBarButtonItemWithImage(UIImage.init(named: "add_icon")!,CustomNavBarEnum.CustomBarButtonItemPosition.BarButtonItemPositionRight, self, #selector(addButtonPressed))
    }
   

    
    @objc func addButtonPressed() {
                editingIndex = -1
                    let vc  = AddAddressViewController.loadVC()
                    vc.addressType = addType
                  vc.delegateAddAddress = self
                    self.navigationController?.pushViewController(vc, animated: true)
        }
    
    func setUpTable() {
        tblAddress.delegate = self
        tblAddress.dataSource = self
        tblAddress.register(UINib.init(nibName: cellReuseIdentifier, bundle: nil), forCellReuseIdentifier: cellReuseIdentifier)
        tblAddress.register(UINib.init(nibName: btnViewCellReuseIdentifier, bundle: nil), forCellReuseIdentifier: btnViewCellReuseIdentifier)
        tblAddress.estimatedRowHeight =  DesignUtility.getValueFromRatio(200)
        if addressArray.count >  limit - 1{
            self.tblAddress.tableFooterView = CustomReferesh.getCustomLoaderToFooter()
        }else{
            self.tblAddress.tableFooterView = nil //CustomReferesh.getCustomLoaderToFooter()
        }
       }
    
    
    func setupModel() {
        dataArray = [
        "Sheikh Khalifa Bin Salman Highway, Manama , Bahrain",
        "small",
        "Sheikh Khalifa Bin Salman Highway, Manama , Bahrain , Sheikh Khalifa Bin Salman Highway, Manama , Bahrain , Sheikh Khalifa Bin Salman Highway, Manama , Bahrain",
        "Sheikh Khalifa Bin Salman Highway, Manama , Bahrain"
        ]
        
        tblAddress.reloadData()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        print("MyAddressViewController deallocated")
    }
    
    @IBAction func submitButtonDidTapped(_ sender: Any) {
        
          self.submitButtonPressed()
    }
}
extension MyAddressViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return addressArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row < addressArray.count {
        let cell  = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! MyAddressTableViewCell
            let addData = addressArray[indexPath.row]
            cell.configCell(data:addData, type: addType)
            cell.actionEdit = { [weak self] address in
                self?.editingIndex = indexPath.row
                self?.goToAddAddress(addressData: address!)
            }
            
             if CurrentUser.selectedAddressID == addData.id {
               selectedCellIndex = indexPath.row
                self.selectedIndex = indexPath.row
             }else{
       
            }
            
            cell.isHidden = true
            if cell.animated == false && indexPath.row < cellToAnimate && isAnimationCompleted == false{
                cell.animated = true
                let delay = Float(indexPath.row + 1) * 0.20
                perform(#selector(self.slideCell), with: cell, afterDelay:TimeInterval(delay))
            }else{
                cell.isHidden = false
                isAnimationCompleted = true
            }
            cell.selectionStyle = .none
            return cell
        }else{
            let cell  = tableView.dequeueReusableCell(withIdentifier: btnViewCellReuseIdentifier) as! ReportButtonTableViewCell
                cell.configCell(data: GlobalStatic.getLocalizedString("submit"))
                cell.reportBtnAction = { [weak self] in
                   // self?.show(viewcontrollerInstance: AddAddressViewController.loadVC())
                 self?.submitButtonPressed()
                }
                cell.selectionStyle = .none
                return cell
           }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        if indexPath.row  == addressArray.count  {
              return DesignUtility.getValueFromRatio(100)
        }
        
         return UITableViewAutomaticDimension
    }
    
    func submitButtonPressed() {
        if ((self.selectedIndex) != -1) {
            
            let addData = self.addressArray[self.selectedIndex]
            CurrentUser.selectedAddressID = addData.id!
            UserDefaults.standard.set(CurrentUser.selectedAddressID, forKey: UserDefaultKey.selectedAddress)
            self.delegateAddress?.gotAddress(add: (addData), type: (self.addType)!)
            self.navigationController?.popViewController(animated: true)
        }else{
            Alert.showMsg(msg: GlobalStatic.getLocalizedString("choose_add_first"))
        }
    }
    
    func goToAddAddress(addressData : GAData) {
        let vc = AddAddressViewController.loadVC()
        vc.addressData = addressData
        vc.addressType = addType
        vc.delegateAddAddress = self
        show(viewcontrollerInstance: vc)
        
    }
    
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        checkForMoreRecords(indexNo: indexPath.row)
    }
    
    
    func checkForMoreRecords(indexNo : Int) {
        
        
        if indexNo == addressArray.count - 1 {
            if pageNo < totalPages {
                
                if  !CustomReferesh.imgLogoFooter.isAnimating{
                     CustomReferesh.imgLogoFooter.rotate()
                }
                
                print("indexNo" , indexNo)
                print("pageNo" , pageNo)
                pageNo += 1
                offset = pageNo //--ww (pageNo * limit) - 1
                getMoreAddress(isSpinnerNeeded: false, offset: offset, limit: limit)
            }else{
                tblAddress.tableFooterView = nil
            }
        }
        
    }
}

extension MyAddressViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
//        if let cell = tblAddress.cellForRow(at: indexPath) as? MyAddressTableViewCell {
//            cell.imgRadio.image = UIImage.init(named: "radio_btn_blank")
//        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let cellSelected = tblAddress.cellForRow(at: IndexPath.init(row: selectedCellIndex, section: 0)) as? MyAddressTableViewCell {
            cellSelected.imgRadio.image = UIImage.init(named: "radio_btn_blank")
         }
        
       if let cell = tblAddress.cellForRow(at: indexPath) as? MyAddressTableViewCell {
            cell.imgRadio.image = UIImage.init(named: "radio_btn_sel")
            selectedIndex = indexPath.row
           selectedCellIndex = indexPath.row
        CurrentUser.selectedAddressID = addressArray[indexPath.row].id!
        }
    }
    

    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            Alert.showWithTwoActions(msg: GlobalStatic.getLocalizedString("delete_address_confirm"), okBtnTitle: GlobalStatic.getLocalizedString("yes"), okBtnAction: {
                self.deleteNotification(indexpath: indexPath)
            }, cancelBtnTitle: GlobalStatic.getLocalizedString("no"), cancelBtnAction: {
                self.tblAddress.reloadRows(at: [indexPath], with: .automatic)
            })
        }
    }
}

// MARK: - Networking
extension MyAddressViewController {
    
    func deleteNotification(indexpath : IndexPath) {
        let add = addressArray[indexpath.row]
        let requestParam = self.manager.paramsDeleteAddress(addId: add.id!)
        self.manager.apiDelete(requestParam, completion: {
            if self.manager.isSuccess {
                self.addressArray.remove(at: indexpath.row)
                self.tblAddress.deleteRows(at: [indexpath], with: .automatic)
                if self.selectedIndex == indexpath.row {
                self.selectedCellIndex = -1
                self.selectedIndex = -1
                UserDefaults.standard.removeObject(forKey: UserDefaultKey.selectedAddress)
                }
                 self.tblAddress.reloadData()
            }else {
                print("failed")
            }
        })
    }
    
    
    
    func getMoreAddress(isSpinnerNeeded : Bool , offset : Int , limit : Int) {
  
        let requestParam = self.manager.params(offset: offset, limit: limit)
        
        self.manager.apiCall(requestParam, showSpinner: isSpinnerNeeded, completion: {
            
         
            if self.manager.isSuccess {
                print("totalPages" , self.manager.totalPages)
                self.addressArray = self.addressArray + self.manager.addressData
                if self.addressArray.count >  limit - 1{
                    self.tblAddress.tableFooterView = CustomReferesh.getCustomLoaderToFooter()
                }else{
                    self.tblAddress.tableFooterView = nil //CustomReferesh.getCustomLoaderToFooter()
                }
               
                
             
                self.tblAddress.reloadData()
            }else {
                
                print("failed")
            }
        })
    }
}
extension MyAddressViewController : AddAddressProtocol {
    
    func addressSaved(add: GAData, type: AddressType) {
        if editingIndex == -1 {
            addressArray .insert(add, at: 0)
        }else{
            addressArray[editingIndex] = add
        }
        tblAddress.reloadData()
    }
}

