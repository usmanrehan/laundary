//
//  ServicesViewController.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/5/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit

class ServicesViewController: BaseViewController , StoryBoardHandler {
    static var myStoryBoard: (forIphone: String, forIpad: String?) = (Storyboards.services.rawValue, nil)
    
    @IBOutlet weak var lblTextTotal: BaseUILabel!
    @IBOutlet weak var lblTotalAmount: BaseUILabel!
    @IBOutlet weak var tblViewServices: UITableView!
    @IBOutlet weak var footerBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var imgBottomArrow: UIImageView!
    @IBOutlet weak var viewFooter: UIView!
    
    let servicesHeaderCellReuseIdentifier = "ServiceHeaderTableViewCell"
    let serviceCellReuseIdentifier = "ServicesTableViewCell"
    var optionsView: OptionsView!
    var isCellAnimated = false
    var manager = GetServiceManager()
    var selectedCategory = 0
    var mainArray : [SLService]?
    var selectedItemDict = [Int : [SLItem]]()
    lazy var refreshControl = CustomReferesh()
    
    let cellToAnimate = DeviceUtility.getDeviceType()  == .iPhoneX ? 6 : 5
 
    override func viewDidLoad() {
        super.viewDidLoad()
        
         self.footerBottomConstraint.constant = -DesignUtility.getValueFromRatio(45)
        viewFooter.isHidden = true
         //--ww CartData.cartDict = [:]
        //--ww    lblTotalAmount.text = "0 AED"
            setupHeader()
            setupTable()
            configOptionsView()
            getServiceData(showSpinner : true)
           bottomTopSetting()
            OrderSummaryViewController.delegateOrderSummary = self
        lblTextTotal.text = GlobalStatic.getLocalizedString("total")
        imgBottomArrow.image = UIImage.init(named: "bottom_arrow")?.flipIfNeeded()
    }
    
    func setupHeader() {
        self.title = GlobalStatic.getLocalizedString("services")
         let img = UIImage.init(named: "cart")?.flipIfNeeded()
  self.addBarButtonItemWithImage(img!,CustomNavBarEnum.CustomBarButtonItemPosition.BarButtonItemPositionRight, self, #selector(cartButtonPressed))
    }
    
    @objc func cartButtonPressed() {
        
        print("cartButtonPressed")
        if CartData.totalQuantity > 0{
            goToCart()
        }else{
        show(viewcontrollerInstance: CartPlaceholderViewController.loadVC())
        }
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        CustomReferesh.imgLogo.layer.removeAllAnimations()
        CustomReferesh.imgLogo.rotate()
    }
    
    deinit {
        print("ServiceController deallocated")
    }
    
    override func goBack() {
        
        if CartData.totalQuantity > 0 {
        Alert.showWithTwoActions(msg: GlobalStatic.getLocalizedString("empty_cart_alert"), okBtnTitle: GlobalStatic.getLocalizedString("yes"), okBtnAction: {

            GlobalStatic.deleteCartData()
             self.pushOrPopViewController(navigationController: (self.navigationController)!, animation: true, viewControllerClass: HomeViewController.self, viewControllerStoryboad: (Storyboards.home.rawValue, nil))

        }, cancelBtnTitle: GlobalStatic.getLocalizedString("no"), cancelBtnAction: {

        })
       }else{
            
             self.pushOrPopViewController(navigationController: (self.navigationController)!, animation: true, viewControllerClass: HomeViewController.self, viewControllerStoryboad: (Storyboards.home.rawValue, nil))
        }
       
    }
    
    @IBAction func footerButtonTapped(_ sender: Any) {
        
         GlobalStatic.saveCartData()
            goToCart()
        }
    
    func goToCart() {
//        var mainArray = [[(quantity : Int, amount: Float , item : String , section :Int)]]()
//        let data = CartData.cartDict
//        for i in 0...2 {
//            let filtered = data.filter { $0.value.section == i}
//            let array = Array(filtered.values)
//            
//            mainArray.append(array)
//            
//        }
    
        let vc = MyCartViewController.loadVC()
       //--ww  vc.mainArray = mainArray
//        vc.dataArray = optionsView.dataArray
        show(viewcontrollerInstance: vc)
    }
    
    func configOptionsView() {
        
      //  let catArray = self.manager.serviceData
        
        let frameHeader = CGRect(x: 0 , y : 0 , width : tblViewServices.frame.width , height: DesignUtility.getValueFromRatio(200))
        optionsView = OptionsView.init(frame: frameHeader)
       optionsView.delegateOptions = self
        optionsView.lblHeading.text = GlobalStatic.getLocalizedString("service_type")
        tblViewServices.tableHeaderView = optionsView
    }

   
    func setupTable(){
        
        tblViewServices.delegate = self
        tblViewServices.dataSource = self
        
        tblViewServices.register(UINib.init(nibName: servicesHeaderCellReuseIdentifier, bundle: nil), forCellReuseIdentifier: servicesHeaderCellReuseIdentifier)
        tblViewServices.register(UINib.init(nibName: serviceCellReuseIdentifier, bundle: nil), forCellReuseIdentifier: serviceCellReuseIdentifier)
        
        refreshControl.addTarget(self, action:
            #selector(handleRefresh),
                                 for: UIControlEvents.valueChanged)
     tblViewServices.addSubview(refreshControl)
     CustomReferesh.addCustomLoader(refereshControl: refreshControl)
       
    }
    
    func fillOptionsView() {
        optionsView.dataArray = [
            (imgName: "dryClean" , title : mainArray![0].title ??  "Dry Clean"),
            (imgName: "iron" , title : mainArray![1].title ?? "Steam Ironing"),
            (imgName: "wash-iron" , title : mainArray![2].title ?? "Wash & Iron")
        ]
        
       //--ww   setInitialValues()
        optionsView.selectedCellIndex = 0
        optionsView.colViewOptions.reloadData()
         optionsView.colViewOptions.selectItem(at:IndexPath.init(item: 0, section: 0), animated: false, scrollPosition: .centeredHorizontally)
        
       
    }
    
    
    func setInitialValues() {
         //isCellAnimated = false
        selectedCategory = 0
        CartData.cartDict = [:]
        CartData.totalQuantity = 0
        CartData.totalAmount = 0
        CartData.params = [:]
//        let rightBarButtons = self.navigationItem.rightBarButtonItems
//        let lastBarButton = rightBarButtons?.last
//        lastBarButton?.removeBadge()
        
        animateFooter(const: -DesignUtility.getValueFromRatio(45))
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func handleRefresh()  {
          print("handleRefresh")
        
    }
    
    
    func tblViewReloadData(){
      
        tblViewServices.reloadData()
//        self.tblViewServices.isUserInteractionEnabled = false
//        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
//              self.tblViewServices.isUserInteractionEnabled = true
//        }
    }
}
extension ServicesViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return  self.mainArray?[selectedCategory].serviceType.count ?? 0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  self.mainArray?[selectedCategory].serviceType[section].item.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: serviceCellReuseIdentifier) as! ServicesTableViewCell
        cell.selectionStyle = .none
        cell.serviceDelegate = self
        cell.configCell(data: self.mainArray![selectedCategory].serviceType[indexPath.section].item[indexPath.row] , section: selectedCategory )
        if indexPath.section == 0 {
        if indexPath.row < cellToAnimate && isCellAnimated == false {
           
            let delay = Float(indexPath.row + 2) * 0.2
            perform(#selector(self.slideCell), with: cell, afterDelay:TimeInterval(delay))
        }else {
           /// let delay = Float(indexPath.row) * 0.10
           /// perform(#selector(self.slideCell), with: cell, afterDelay:TimeInterval(delay))
            
        }
        
       
        }
        
        if indexPath.section == 0 {
            if indexPath.row == cellToAnimate {
                
                isCellAnimated = true
            }
            
        }
        return cell
        
    }
    

    
//    @objc func slideCell(cell : UITableViewCell)  {
//        Animations.slideView(view: cell)
//    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
         if indexPath.section == 0 {
         //   print("isCellAnimated" , isCellAnimated)
        if indexPath.row < cellToAnimate && isCellAnimated == false {
          
     cell.isHidden = true
        }
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
//        if indexPath.section == 0 {
//            if indexPath.row == 2 {
//
//                isCellAnimated = true
//            }
//
//        }
    }
   
    
    @objc func slideView (cell : UITableViewCell){
        
        let viewFrame = cell.frame
        cell.frame = CGRect(x:-cell.frame.size.width, y:cell.frame.origin.y , width :cell.frame.size.width , height : cell.frame.size.height )
        cell.isHidden = false
      
        UIView.animate(withDuration: 1, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 1, options: .curveEaseInOut, animations: {
            cell.frame = viewFrame
        }) { _ in
            
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
       return DesignUtility.getValueFromRatio(35)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      return DesignUtility.getValueFromRatio(100)
      }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: DesignUtility.getValueFromRatio(35)))
        view.backgroundColor = UIColor.groupTableViewBackground
        
        let label = BaseUILabel(frame: CGRect(x: DesignUtility.getValueFromRatio(25), y: 0, width: tableView.bounds.width - DesignUtility.getValueFromRatio(60), height: DesignUtility.getValueFromRatio(35)))
        
        label.fontNameTheme = "fontDefault"
        label.fontColorTheme = "darkGray"
        label.fontSizeTheme = "sizeSmall"
        label.text = self.mainArray![selectedCategory].serviceType[section].title
        label.tag = -1
        label.clipsToBounds = true
        view.addSubview(label)
        return view
    }
}

extension ServicesViewController : UITableViewDelegate {
    
    // MARK: UIScrollView delegate method implementation
    
     func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if refreshControl.isRefreshing {
            print("refreshControl.isRefreshing")
            optionsView.isCellAnimated = true
            getServiceData(showSpinner : false)
        }
    }
    
}

extension ServicesViewController : OptionsViewProtocol {
    
    func didSelectItemAtIndex(index: Int) {
       // print(optionsView.dataArray[index])
     //--ww   optionsView.isUserInteractionEnabled = false
        refreshControl.endRefreshing()
         selectedCategory = index
       //--ww  isCellAnimated = false
       //--ww self.tblViewServices.reloadData()
        tblViewReloadData()
     
//        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
//             self.optionsView.isUserInteractionEnabled = true
//        }
       
       
        
    }
}

// MARK: - Networking
extension ServicesViewController {
    
    func getServiceData(showSpinner : Bool) {
        
       let requestParam = self.manager.params()
        
        self.manager.apiCall(requestParam,completion: {
       
         self.refreshControl.endRefreshing()
         if self.manager.isSuccess {
            
              self.mainArray = self.manager.serviceData
            if self.mainArray != nil  {
              //--ww  self.setInitialValues()
              self.fillOptionsView()
                if showSpinner == false{
                    
                self.tblViewServices.reloadData()
                }else{
                    self.tblViewReloadData()
                    }
                }
            }
            else {
                
            }
        } , isSpinnerNeeded: showSpinner)
    }
}
extension ServicesViewController : ServicesTableViewCellProtocol {
    
    func didTapOnSlider() {
        
        bottomTopSetting()
    }
    
    func bottomTopSetting(){
        lblTotalAmount.text = "\(CartData.totalAmount) AED"
        if CartData.totalAmount > 0 {
             GlobalStatic.saveCartData()
            animateFooter(const: 0.0)
        }else{
            GlobalStatic.deleteCartData()
            animateFooter(const: -DesignUtility.getValueFromRatio(45))
        }
    }
    
    
    func animateFooter(const : CGFloat) {
        
        let rightBarButtons = self.navigationItem.rightBarButtonItems
        let lastBarButton = rightBarButtons?.last
        let totalCategoryCount = CartData.cartDict.count
       //--ww  CartData.totalQuantity
        if totalCategoryCount == 0 {
            lastBarButton?.removeBadge()
        }else{
        lastBarButton?.setBadge(text: "\(totalCategoryCount)")
        }
       
        if const == 0.0{
            viewFooter.isHidden = false
        }
        
        UIView.animate(withDuration: 0.3, animations: {
            self.footerBottomConstraint.constant = const
            self.view.layoutIfNeeded()
        }, completion: { (v) in
            
            if const  < 0.0{
                self.viewFooter.isHidden = true
            }
        })
    }
}

extension ServicesViewController : OrderSummaryProtocol {
    
    func deleteCart() {
        setInitialValues()
        tblViewServices.reloadData()
    }
}
