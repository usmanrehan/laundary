//
//  WalkThroughViewController.swift
//  LaundryApp
//
//  Created by CMS Testing on 2/27/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit
import MOLH

class WalkThroughViewController: UIViewController, StoryBoardHandler {
    static var myStoryBoard: (forIphone: String, forIpad: String?) = (Storyboards.walkthrough.rawValue,nil)
    
    
    
    @IBOutlet weak var colView: UICollectionView!
    @IBOutlet weak var lblTitle: BaseUILabel!
    @IBOutlet weak var lblDescription: BaseUILabel!
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var btnNext: CustomButton!
    
    
    @IBOutlet weak var btnSkip: BaseUIButton!
    
    let reuseIdentifier = "WalkthroughCollectionViewCell"
    var dataArray = [WalkthroughModel]()
    var currentPage = 0
    var isAnimating = false
   
    @IBOutlet weak var pageControll: BaseUIPageControl!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configColView()
        setupModel()
       setUpForAnimation()
    }

    func setUpForAnimation()  {
        imgLogo.isHidden = true
        btnNext.isHidden = true
    }
    
    func configColView(){
        colView.delegate = self
        colView.dataSource = self
        
        let nib = UINib(nibName: reuseIdentifier, bundle: nil)
        colView?.register(nib, forCellWithReuseIdentifier: reuseIdentifier)
        colView.semanticContentAttribute = .forceLeftToRight
    }
    func setupModel() {
        dataArray = [
            WalkthroughModel(imgName: "page1" , title : GlobalStatic.getLocalizedString("title1") , description : GlobalStatic.getLocalizedString("detail1")),
            
            WalkthroughModel(imgName: "page1" , title : GlobalStatic.getLocalizedString("title2") , description : GlobalStatic.getLocalizedString("detail2")),
            
            WalkthroughModel(imgName: "page1" , title : GlobalStatic.getLocalizedString("title3") , description :GlobalStatic.getLocalizedString("detail3"))
                    ]
        btnNext.setTitle(GlobalStatic.getLocalizedString("next"), for: .normal)
        btnSkip.setTitle(GlobalStatic.getLocalizedString("skip"), for: .normal)
        setDataForCurrentPage(index : 0)
        colView.reloadData()
    }
    
   
    
    override func viewDidAppear(_ animated: Bool) {
        let imgPos = imgLogo.frame
        imgLogo.frame = CGRect(x: imgPos.origin.x , y:-100 , width:imgPos.size.width , height :imgPos.size.height )
        
        imgLogo.isHidden = false
        UIView.animate(withDuration: 0.6, delay: 0.3, usingSpringWithDamping: 0.7, initialSpringVelocity: 1, options: .curveEaseInOut, animations: {
            self.imgLogo.frame = imgPos
        }) { _ in
           
        }
       // perform(#selector(yourFunction), with: nil, afterDelay: 1)
        btnNext.flipFromRight()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func skipWalkthrough(_ sender: Any) {
        goToLogin()
    }
    

    func goToLogin()  {
        show(viewcontrollerInstance: LoginViewController.loadVC())
    }
    
    @IBAction func nextBtnTapped(_ sender: Any) {
         btnNextTapped(indexNo: currentPage)
     }
}
extension WalkThroughViewController : UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = colView?.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as!
        WalkthroughCollectionViewCell
        
        cell.configCell(data: dataArray[indexPath.row])
        
//        cell.btnNext.didTouchUpInside = { [weak self](sender) in
//
//            self?.btnNextTapped(indexNo: indexPath.row)
//        }
        
//        cell.btnNextAction = { [weak self] in
//        
//        self?.btnNextTapped(indexNo: indexPath.row)
//    }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
      
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
    }
    
    
    func btnNextTapped(indexNo : Int){
        
        if isAnimating {
            return
        }
        isAnimating = true
        if indexNo < 2 {
            //get Collection View Instance
       
            
            //get cell size
            let cellSize = CGSize(width:self.view.frame.width,height: self.view.frame.height);
            
            //get current content Offset of the Collection view
            let contentOffset = colView.contentOffset;
            
            //scroll to next cell contentOffset.y
            colView.scrollRectToVisible(CGRect(x:contentOffset.x + cellSize.width, y: 0, width:cellSize.width,height: cellSize.height), animated: true)
//            DispatchQueue.main.async {
//                self.setDataForCurrentPage(index : indexNo)
//            }

        }else{
          goToLogin()
        }
        
    }
}
extension WalkThroughViewController : UICollectionViewDelegate {
    

    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let x = scrollView.contentOffset.x
        let w = scrollView.bounds.size.width
         currentPage = Int(ceil(x/w))
        setDataForCurrentPage(index: currentPage)
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        let x = scrollView.contentOffset.x
        let w = scrollView.bounds.size.width
        currentPage = Int(ceil(x/w))
        setDataForCurrentPage(index: currentPage)
        isAnimating = false
    }
    
    func setDataForCurrentPage(index : Int){
        
        pageControll.currentPage = index
        lblTitle.text = dataArray[index].title
        lblDescription.text = dataArray[index].description
    }
    
}
extension WalkThroughViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width:self.view.frame.width, height:self.view.frame.height)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
    
}


