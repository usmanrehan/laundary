//
//  MyProfileViewController.swift
//  LaundryApp
//
//  Created by Waqas Ali on 3/2/18.
//  Copyright © 2018 CMS Testing. All rights reserved.
//

import UIKit
import MICountryPicker

class MyProfileViewController: BaseViewController, StoryBoardHandler {

    static var myStoryBoard: (forIphone: String, forIpad: String?) = (Storyboards.home.rawValue , nil)
    
    @IBOutlet weak var tblMyProfile: UITableView!
    
    var dataArray = [(title : String, desc : String , imgName: String)]()
    var tblType : TblType = .profile
    var countryCode = ""
    var isAnimated = false
    
    var managerAddress = GetAddressManager()
    
 
    fileprivate var myProfileFooterView : MyProfileFooterView!
    
    
     let profileHeaderCellReuseIdentifier = "MyProfileImageTableViewCell"
     let profileCellReuseIdentifier = "MyProfileTableViewCell"
     let editCellReuseIdentifier = "EditProfileTableViewCell"
     let editMobileCellReuseIdentifier = "MyProfilePhoneTableViewCell"
    
     var manager = UpdateProfileManager()
    var choosenImage : UIImage? = nil
    
    let picker = UIImagePickerController()
    
    override func viewDidLoad() {
        print("before viewDidLoad")
        super.viewDidLoad()
         print("after viewDidLoad")
       customLoader.hide()
        // Do any additional setup after loading the view.
        setUpHeader()
        setupTable()
        setupTbleFooter()
        setupModel()
      //as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
    }
    
    override func viewDidAppear(_ animated: Bool) {
         print("after viewDidAppear")
        //--ww picker.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
         print("after viewWillAppear")
    }
    
    func setUpHeader() {
          print("after setUpHeader")
        
        self.title = GlobalStatic.getLocalizedString("profile")
        self.addBarButtonItemWithImage(UIImage.init(named: "edit")!,CustomNavBarEnum.CustomBarButtonItemPosition.BarButtonItemPositionRight, self, #selector(editButtonPressed))
    }
    
    override func goBack() {
        if tblType == .edit {
//            let rightBarButtons = self.navigationItem.rightBarButtonItems
//            let lastBarButton = rightBarButtons?.last
//            lastBarButton?.isEnabled = true
          //  lastBarButton?.image = UIImage.init(named: "edit")
            
             self.addBarButtonItemWithImage(UIImage.init(named: "edit")!,CustomNavBarEnum.CustomBarButtonItemPosition.BarButtonItemPositionRight, self, #selector(editButtonPressed))
            self.tblType = .profile
            self.tblMyProfile.tableFooterView = nil
            self.tblViewReloadData()
          //--ww  self.tblMyProfile.reloadData()
            
        }else{
         self.navigationController?.popViewController(animated: true)
        }
    }
    
    @objc func editButtonPressed(){
        
        if tblMyProfile.isUserInteractionEnabled{
        tblType = .edit
       tblMyProfile.tableFooterView = myProfileFooterView
      //--ww tblMyProfile.reloadData()
//            let rightBarButtons = self.navigationItem.rightBarButtonItems
//            let lastBarButton = rightBarButtons?.last
//            lastBarButton?.isEnabled = false
            self.navigationItem.rightBarButtonItems = nil
           // lastBarButton?.image = nil
        self.tblViewReloadData()
        }
    }
    
    func setupTable(){
     print("after setupTable")
        tblMyProfile.delegate = self
        tblMyProfile.dataSource = self
        
      tblMyProfile.register(UINib.init(nibName: profileHeaderCellReuseIdentifier, bundle: nil), forCellReuseIdentifier: profileHeaderCellReuseIdentifier)
        
        tblMyProfile.register(UINib.init(nibName: profileCellReuseIdentifier, bundle: nil), forCellReuseIdentifier: profileCellReuseIdentifier)
        
        tblMyProfile.register(UINib.init(nibName: editCellReuseIdentifier, bundle: nil), forCellReuseIdentifier: editCellReuseIdentifier)
        
        
         tblMyProfile.register(UINib.init(nibName: editMobileCellReuseIdentifier, bundle: nil), forCellReuseIdentifier: editMobileCellReuseIdentifier)
       tblMyProfile.estimatedRowHeight = DesignUtility.getValueFromRatio(90)
    }
    

    
    
    func setupTbleFooter(){
      print("after setupTbleFooter")
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName:"MyProfileFooterView" , bundle: bundle)
        myProfileFooterView = nib.instantiate(withOwner: self, options: nil)[0] as! MyProfileFooterView
        myProfileFooterView.frame = CGRect(x: tblMyProfile.frame.origin.x,
                                        y: tblMyProfile.frame.origin.y,
                                        width : tblMyProfile.frame.size.width,
                                        height : DesignUtility.getValueFromRatio(90))
        
        myProfileFooterView.btnUpdate.setTitle(GlobalStatic.getLocalizedString("update"), for: .normal)
        myProfileFooterView.updateAction = { [weak self] in
            
         let arr = self?.getTextFeildValuesFromTableView()
            let status = self?.validateForm(array: arr!)
            if status?.0 == false {
                Alert.showMsg(msg: (status?.1)!)
            }else {
                
                self?.updateProfile(data : arr!)
             }
        }
    }
    
    
    func getTextFeildValuesFromTableView() ->[String]? {
        var values = [String]()
        
        let total = dataArray.count
        for i in 1..<total {
            let indexPath = IndexPath(row: i, section: 0)
            if i == 4 {
                guard let cell = tblMyProfile.cellForRow(at: indexPath) as? MyProfilePhoneTableViewCell else{
                    return nil
                }
               
                    values.append(cell.txtCode.text ?? "")
                    values.append(cell.txtNumber.text ?? "")
                    }else{
                
                guard let cell = tblMyProfile.cellForRow(at: indexPath) as? EditProfileTableViewCell else{
                    return nil
                }
               
                values.append(cell.txtData.text ?? "")
            }
        }
     
        return values
    }
    
    
    func validateForm(array : [String]) -> (Bool , String)  {
        
       
        if (array[0].isEmptyStr()){
            return (false, GlobalStatic.getLocalizedString("name_needed"))
        }else if !(array[0].isAlphabetOnly){
            return (false, GlobalStatic.getLocalizedString("valid_name_needed"))
        }else if (array[0].count) < Constants.minimumLengthName {
            return (false,  GlobalStatic.getLocalizedString("name_min"))
        }else  if (array[3].isEmptyStr()) {
            return (false, GlobalStatic.getLocalizedString("country_code_needed"))
        }else if (array[4].isEmptyStr()) {
            return (false, GlobalStatic.getLocalizedString("mobile_needed"))
        }else if !(array[4].isNumeric()){
            return (false, GlobalStatic.getLocalizedString("valid_mobile_needed"))
        }else  if (array[4].hasPrefix("0")) {
            return (false, GlobalStatic.getLocalizedString("remove_0"))
        }else if (array[4].count) < Constants.minimumLengthNumber || (array[4].count) > Constants.maximumLengthNumber {
            return (false, "\(GlobalStatic.getLocalizedString("number_min"))  \(Constants.minimumLengthNumber) \(GlobalStatic.getLocalizedString("number_max")) \(Constants.maximumLengthNumber)  \(GlobalStatic.getLocalizedString("digits_long"))")
        }
        
        return (true, "")
    }
   
    
    func setupModel() {
         print("after setupModel")

         let add = (CurrentUser.data?.address?.isEmptyStr())! ? "N/A" :  CurrentUser.data?.address!
   dataArray = [
            (title : "Full Name", desc : "Charlie Hunnam", imgName : "name"),
            (title :  GlobalStatic.getLocalizedString("full_name"), desc : CurrentUser.data?.name ?? "Charlie Hunnam" , imgName : "name"),
            (title :  GlobalStatic.getLocalizedString("email_address"), desc : CurrentUser.data?.email ?? "abc@email.com", imgName : "email"),
            (title :  GlobalStatic.getLocalizedString("address"), desc : add!, imgName : "address"),
          
            (title :  GlobalStatic.getLocalizedString("mobile_no"), desc : CurrentUser.data?.phone ?? "+1 234 5678", imgName : "mobile")
        ]
        
      //  , Muhammad Bin Rashid Dubai , Muhammad Bin Rashid Dubai , Muhammad Bin Rashid Dubai
        
         tblViewReloadData()
    }
    
    deinit {
        print("MyProfile Deallocated")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tblViewReloadData(){
        isAnimated = false
        tblMyProfile.reloadData()
        tblMyProfile.isUserInteractionEnabled = false
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.tblMyProfile.isUserInteractionEnabled = true
            self.isAnimated = true
            
        }
    }
   

}
extension MyProfileViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return  dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if indexPath.row == 0 {
          
            let cell  = tableView.dequeueReusableCell(withIdentifier: profileHeaderCellReuseIdentifier) as! MyProfileImageTableViewCell
            cell.configCell(imgUrl: CurrentUser.data?.image ?? "", type: tblType , imgUpload: choosenImage)
        
            cell.selectionStyle = .none
            cell.uploadImageAction = { [weak self] in
                self?.showOptions()
            }
        
            let delay = Float(indexPath.row) * 0.10
            perform(#selector(self.slideView), with: cell, afterDelay:TimeInterval(delay))
            return cell
        }else if tblType == .profile {
           let cell  = tableView.dequeueReusableCell(withIdentifier: profileCellReuseIdentifier) as! MyProfileTableViewCell
        
            cell.configCell(data: dataArray[indexPath.row], row: indexPath.row)
            cell.selectionStyle = .none
            let delay = Float(indexPath.row) * 0.10
            perform(#selector(self.slideView), with: cell, afterDelay:TimeInterval(delay))
        return cell
        }else{
            
            if indexPath.row == 4 {
                
                let cell  = tableView.dequeueReusableCell(withIdentifier: editMobileCellReuseIdentifier) as! MyProfilePhoneTableViewCell
                
                cell.configCell(data: dataArray[indexPath.row], type: tblType, code: countryCode  )
                cell.selectionStyle = .none
                cell.codeAction = { [weak self] in
                        self?.showCountryPicker()
                    }
                let delay = Float(indexPath.row) * 0.10
                perform(#selector(self.slideView), with: cell, afterDelay:TimeInterval(delay))
                return cell
                
            }else {
            
            let cell  = tableView.dequeueReusableCell(withIdentifier: editCellReuseIdentifier) as! EditProfileTableViewCell
            
            cell.configCell(data: dataArray[indexPath.row], type: tblType)
            
            cell.selectionStyle = .none
                let delay = Float(indexPath.row) * 0.10
                perform(#selector(self.slideView), with: cell, afterDelay:TimeInterval(delay))
            return cell
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if isAnimated == false{
        cell.isHidden = true
        }
   }
    
    @objc func flipImageCell(){
        let cell = tblMyProfile.cellForRow(at: IndexPath.init(row: 0, section: 0)) as! MyProfileImageTableViewCell
        
        cell.flipFromRight()
        
    }
    
    @objc func slideView (cell : UITableViewCell){
        
        if isAnimated == false{
        let viewFrame = cell.frame
        cell.frame = CGRect(x:-cell.frame.size.width, y:cell.frame.origin.y , width :cell.frame.size.width , height : cell.frame.size.height )
        cell.isHidden = false
       UIView.animate(withDuration: 1, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 1, options: .curveEaseInOut, animations: {
            cell.frame = viewFrame
        }) { _ in
            
        }
        }
    }
    
    
    @objc func slideViewLastCell (cell : UITableViewCell){
        
        if isAnimated == false{
            let viewFrame = cell.frame
            cell.frame = CGRect(x:-cell.frame.size.width, y:cell.frame.origin.y , width :cell.frame.size.width , height : cell.frame.size.height )
            cell.isHidden = false
            UIView.animate(withDuration: 1, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 1, options: .curveEaseInOut, animations: {
                cell.frame = viewFrame
            }) { _ in
                self.isAnimated = true
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
           return DesignUtility.getValueFromRatio(130)
        }
        if tblType == .edit {
        return DesignUtility.getValueFromRatio(75)
        }
        return UITableViewAutomaticDimension
    }
    
    
    func showCountryPicker() {
        let countryPicker = MICountryPicker()
        countryPicker.showCallingCodes = true
        
        countryPicker.didSelectCountryWithCallingCodeClosure = { name, code, dialCode in
            print(dialCode)
            self.countryCode = dialCode
            
            self.tblMyProfile.reloadRows(at: [IndexPath.init(row: 4, section: 0)], with: .automatic)
            self.navigationController?.popViewController(animated: true)
        }
       // navigationController?.pushViewController(picker, animated: true)
        
        self.navigationController?.navigationBar.tintColor = UIColor.darkGray
        show(viewcontrollerInstance: countryPicker)
    }
    
}

extension MyProfileViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tblType == .edit {
        if indexPath.row == 3 {
            getAddressData()
         }
        }
      }
    }

enum TblType {
    case profile , edit
    
}

// MARK: - Networking
extension MyProfileViewController {
    
    func updateProfile(data : [String]) {
        
        let parameters = [
            "user_id" : String(describing: CurrentUser.data!.id) ,
            "name" : data[0],
            "address" :data[2],
            "phone" : data[3] + "-" + data[4],
          ] as [String : AnyObject]
        
        
        var resizedImg = UIImage()
        
        if choosenImage != nil  {
        resizedImg = choosenImage!.kf.resize(to: CGSize(width:220 , height: 220), for : .aspectFit)
        }else{
            
        }
        
        let requestParam = self.manager.params(parameters: parameters , imageUpload: resizedImg)
        
        self.manager.api(requestParam, completion: {
            
            if self.manager.isSuccess {
               Alert.showWithCompletion(msg: "Profile updated successfull", completionAction: {
                    
                    
                    self.dataArray = [
                        (title : "Full Name", desc : "Charlie Hunnam", imgName : "name"),
                        (title : "Full Name", desc : data[0] , imgName : "name"),
                        (title : "Email Address", desc : CurrentUser.data?.email ?? "abc@email.com", imgName : "email"),
                        (title : "Address", desc : data[2] , imgName : "address"),
                        (title : "Mobile Number", desc : data[3] + "-" + data[4] , imgName : "mobile")
                    ]
                    
                    
                    self.tblType = .profile
                    self.tblMyProfile.tableFooterView = nil
                    self.tblViewReloadData()
                self.addBarButtonItemWithImage(UIImage.init(named: "edit")!,CustomNavBarEnum.CustomBarButtonItemPosition.BarButtonItemPositionRight, self, #selector(self.self.editButtonPressed))
                   //--ww self.tblMyProfile.reloadData()
                    
                })
            }
            else {
                
                print("failed")
            }
        })
    }
}
//-- Mark
extension MyProfileViewController : UIImagePickerControllerDelegate,
UINavigationControllerDelegate {
    
    //Image selection option to pick image from camera or from photo library
    func showOptions(){
        
        let actionSheetController = UIAlertController(title:GlobalStatic.getLocalizedString("please_select"), message: GlobalStatic.getLocalizedString("choose_img"), preferredStyle: .actionSheet)
        let cancelActionButton = UIAlertAction(title: GlobalStatic.getLocalizedString("ok"),  style: .cancel) { action -> Void in
        }
        actionSheetController.addAction(cancelActionButton)
        let saveActionButton = UIAlertAction(title: GlobalStatic.getLocalizedString("camera"), style: .default) { action -> Void in
            self.openCamera()
        }
        actionSheetController.addAction(saveActionButton)
        let deleteActionButton = UIAlertAction(title: GlobalStatic.getLocalizedString("photolibrary"), style: .default) { action -> Void in
            //self.openLibrary(showDocumentForm: showDocumentForm)
            self.openGallery()
        }
        actionSheetController.addAction(deleteActionButton)
        self.present(actionSheetController, animated: true) {
        }
        //self.present(actionSheetController, animated: true, completi
    }
    
    
    //Image picker souce type check if it is camera
    func openCamera(){
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
             let picker = UIImagePickerController()
            picker.delegate = self
           picker.sourceType = UIImagePickerControllerSourceType.camera;
            picker.cameraDevice = .front
            picker.allowsEditing = true
            self.present(picker, animated: true, completion: {
            })
        }
    }
    
    
    func openGallery(){
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            let picker = UIImagePickerController()
            picker.delegate = self
            picker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
            picker.allowsEditing = true
            self.present(picker, animated: true, completion: {
            })
        }
    }
    
    //MARK: - Delegates
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        choosenImage = info[UIImagePickerControllerEditedImage] as? UIImage //2
        // myImageView.contentMode = .scaleAspectFit //3
        //myImageView.image = chosenImage //4
        
        tblMyProfile.reloadRows(at: [IndexPath.init(row: 0, section: 0)], with: .automatic)
        dismiss(animated:true, completion: nil)
    }
    

    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
}


extension MyProfileViewController {
    
    func getAddressData() {
        let requestParam = self.managerAddress.params(offset: 1, limit: 10)
        
        self.managerAddress.api(requestParam, completion: {
            
            if self.managerAddress.isSuccess {
                
                if self.managerAddress.addressData.count > 0 {
                    let vc = MyAddressViewController.loadVC()
                    vc.addressArray = self.managerAddress.addressData
                    vc.addType = .pickup
                    vc.totalPages = self.managerAddress.totalPages
                    vc.delegateAddress = self
                    self.show(viewcontrollerInstance:vc )
                }else{
                    let vc = AddAddressViewController.loadVC()
                     vc.delegateAddAddress = self
                    self.show(viewcontrollerInstance: vc)
                }
                
            }
            else {
                
            }
        })
    }
}

extension MyProfileViewController : MyAddressProtocol {
    func gotAddress(add: GAData, type: AddressType) {
        
        print("gotAddress" , add , type)
        let indexPath = IndexPath.init(row: 3, section: 0)
       // if let cell = tblMyProfile.cellForRow(at: indexPath) as? EditProfileTableViewCell {
            //cell.txtData.text = GlobalStatic.getFormatedAddress(add: add.location!)
     
        dataArray[3].desc = GlobalStatic.getFormatedAddress(add: add.location!)
            tblMyProfile.reloadRows(at: [indexPath], with: .automatic)
        //}
        
    }
}


extension MyProfileViewController : AddAddressProtocol {
    func addressSaved(add: GAData, type: AddressType) {
        CurrentUser.selectedAddressID = add.id!
        UserDefaults.standard.set(CurrentUser.selectedAddressID, forKey: UserDefaultKey.selectedAddress)
        let indexPath = IndexPath.init(row: 3, section: 0)
        dataArray[3].desc = GlobalStatic.getFormatedAddress(add: add.location!)
        tblMyProfile.reloadRows(at: [indexPath], with: .automatic)
       
    }
}
